#include "server.h"

int verbose = 0, listenfd;
char *MOTD;
static active_users pool;
static accounts *acc_Head;
accounts *cur;
int fp;

void sigint_handler(int sig) {
  int i;
  for (i = 0; i < FD_SETSIZE; ++i) {
    if (pool.client[i].fd > 0) {
      char *buf = "BYE \r\n\r\n";
      int num = write(pool.client[i].fd, buf, strlen(buf));
      if (verbose) {
        fprintf(stderr, "\x1B[1;34mSent: BYE Byte Count: %d\n\x1B[0m", num);
      }
      close(pool.client[i].fd);
      ERROR(2);
    }
  }

  while (cur != NULL) {
    accounts *temp;
    temp = acc_Head;
    while (temp != cur->prev) {
      temp = temp->next;
    }
    free(cur);
    cur = temp;
  }
  close(listenfd);
  free(MOTD);
  close(fp);
  exit(0);
}

void sigpipe_handler(int sig) {}

// to do sqlite3
int main(int argc, char **argv) {
  signal(SIGINT, sigint_handler);
  signal(SIGPIPE, sigpipe_handler);
  int args = commandLineArgs(argc, argv);
  if (args == -1) {
    helpArgs();
    return 1;
  } else if (args == 0) {
    helpArgs();
    return 0;
  } else if (args == 1)
    verbose = 1;
  if (argc < 3) {
    helpArgs();
    return 1;
  }
  int portNum = 0;
  MOTD = (char *)calloc(MAXLINE, 1);
  for (int i = 1; i < argc; ++i) {
    if (i + verbose == 1 + verbose) {
      char *str = argv[i + verbose];
      for (int j = 0; j < strlen(argv[i + verbose]); j++) {
        if ((*str >= '0') && (*str <= '9')) {
          portNum = (portNum * 10) + ((*str) - '0');
          str++;
        } else {
          fprintf(stderr, "\x1B[1;34mError: No such port %s\n",
                  argv[i + verbose]);
          helpArgs();
          free(MOTD);
          return 1;
        }
      }
    } else if (i + verbose == 2 + verbose)
      strcpy(MOTD, argv[i + verbose]);
  }
  char *input_path = calloc(MAXLINE, 1);
  if (3 + verbose < argc && (argc - 3 - verbose) == 1) {
    // input_path = argv[3 + verbose];
    sprintf(input_path, "%s", argv[3 + verbose]);
  }
  args = validate_args(input_path);
  // change to sqlite3
  if (args == 1) {
    struct stat sb;
    stat(input_path, &sb);
    FILE *fake = fopen(input_path, "w");
    fclose(fake);
    if ((fp = open(input_path, O_WRONLY)) <= 0) {
      fprintf(stderr, "\t\x1B[1;31mFailed to open the file %s\x1B[0m\n",
              input_path);
      return 1;
    }
    acc_Head = (accounts *)malloc(sizeof(accounts));
    cur = acc_Head;
    acc_Head->next = NULL;
    acc_Head->prev = NULL;
  } else if (args == 0) {
    struct stat sb;
    stat(input_path, &sb);

    if ((fp = open(input_path, O_RDONLY)) <= 0) {
      fprintf(stderr, "\t\x1B[1;31mFailed to open the file %s\x1B[0m\n",
              input_path);
      return 1;
    }
    FILE *fake = fdopen(fp, "r");
    unsigned char read_value;
    char *str;
    accounts *new = (accounts *)malloc(sizeof(accounts));
    str = new->c.name;
    int isName = 0, set = 0;
    while ((read_value = (char)fgetc(fake)) != 255) {
      if (set) {
        new = (accounts *)malloc(sizeof(accounts));
        set = 0;
        str = new->c.name;
      }
      if (read_value == '\n' && isName == 0) {
        isName = 1;
        str = new->c.hash;
      } else if (read_value == '\n' && isName == 1) {
        isName = 2;
        str = new->c.salt;
      } else if (read_value == '\n' && isName == 2) {
        isName = 0;
        if (acc_Head == NULL) {
          acc_Head = new;
          cur = acc_Head;
          set = 1;
        } else {
          new->prev = cur;
          cur->next = new;
          new->next = NULL;
          cur = new;
          set = 1;
        }
      } else {
        *str = read_value;
        str++;
      }
    }
    fclose(fake);
    if (acc_Head == NULL) {
      acc_Head = (accounts *)malloc(sizeof(accounts));
      cur = acc_Head;
      acc_Head->next = NULL;
      acc_Head->prev = NULL;
    }
    close(fp);
    if ((fp = open(input_path, O_WRONLY)) <= 0) {
      perror("Open");
      fprintf(stderr, "\t\x1B[1;31mFailed to open the file %s\x1B[0m\n",
              input_path);
      return 1;
    }
    lseek(fp, 0, SEEK_END);
  } else {
    free(input_path);
    input_path = calloc(MAXLINE, 1);
    sprintf(input_path, "accounts.txt");
    struct stat sb;
    stat(input_path, &sb);
    FILE *fake = fopen(input_path, "w");
    fclose(fake);
    if ((fp = open(input_path, O_WRONLY)) <= 0) {
      fprintf(stderr, "\t\x1B[1;31mFailed to open the file %s\x1B[0m\n",
              input_path);
      return 1;
    }
    acc_Head = (accounts *)malloc(sizeof(accounts));
    cur = acc_Head;
    acc_Head->next = NULL;
    acc_Head->prev = NULL;
  }
  free(input_path);
  int connfd, optval = 1;
  printf("\x1B[0mCurrently listening on port %d.\n", portNum);
  struct sockaddr_in serveraddr;
  if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    fprintf(stderr, "\t\x1B[1;31mError: Failed to create socket.\x1B[0m\n");
    return 1;
  }
  if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval,
                 sizeof(int)) < 0) {
    ERROR(3);
    return 1;
  }
  memset((char *)&serveraddr, 0, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short)portNum);
  if (bind(listenfd, (SA *)&serveraddr, sizeof(serveraddr)) < 0) {
    ERROR(4);
    return 1;
  }
  if (listen(listenfd, 1024) < 0) {
    ERROR(5);
    return 1;
  }
  initilizeUsers(listenfd, &pool);
  struct sockaddr_storage clientaddr;
  char hostname[MAXLINE], port[MAXLINE];
  socklen_t clientlen;
  while (1) {
    pool.ready_set = pool.read_set;
    errno = 0;
    pool.nready = select(pool.maxfd + 1, &pool.ready_set, NULL, NULL, NULL);
    if (ERROR(0) > 0) {
      internalError(&pool);
    }
    /*This processes server terminal commands.*/
    if (FD_ISSET(STDIN_FILENO, &pool.ready_set)) {
      int isExit = command(&pool);
      if (isExit) {
        free(MOTD);
        close(listenfd);
        close(fp);
        return 0;
      }
    }
    /*This is the Accept Thread for clients*/
    if (FD_ISSET(listenfd, &pool.ready_set)) {
      clientlen = sizeof(struct sockaddr_storage);
      errno = 0;
      connfd = accept(listenfd, (SA *)&clientaddr, &clientlen);
      if (ERROR(1) > 0) {
        internalError(&pool);
      }
      errno = getnameinfo((SA *)&clientaddr, clientlen, hostname, MAXLINE, port,
                          MAXLINE, 0);
      if (errno != 0) {
        internalError(&pool);
      }
      errno = 0;
      /*Not sure if hostname contains ip address find out when you try to
       * connect.*/
      addClient(connfd, &pool, hostname);
    }
    /*This is the login thread and comm thread*/
    checkClients(&pool);
    sleep(1);
  }
  close(listenfd);
  close(fp);
  return 0;
}

/*Displays the help menu for args*/
void helpArgs() {
  printf("\x1B[0mUsage:\n./server [-h|-v] PORT_NUMBER MOTD\n");
  printf("\t-h\t\tDisplays this help menu & returns EXIT_SUCCESS.\n");
  printf("\t-v\t\tVerbose print all incoming and outgoing "
         "protocol\n\t\t\tverbs & content.\n");
  printf("\tPORT_NUMBER\tPort number to listen on.\n");
  printf("\tMOTD\t\tMessage to display to the client when they connect.\n");
}

/*Validates file 1 for No such file, 2 for NULL path, 0 if file exists*/
int validate_args(const char *input_path) {
  if (input_path != NULL && input_path[0] != '\0') {
    struct stat sb;
    memset(&sb, 0, sizeof(sb) + 1);
    if (stat(input_path, &sb) == -1) {
      if (errno == ENOENT)
        return 1;
    }
    stat(input_path, &sb);
  } else {
    return 2;
  }
  return 0;
}

/*Returns -1 on failure 0 if -h, 1 if -v, 2 if anything else*/
int commandLineArgs(int argc, char **argv) {
  if (argv[1] == NULL)
    return -1;
  else if (strcmp(argv[1], "-h") == 0)
    return 0;
  else if (strcmp(argv[1], "-v") == 0)
    return 1;
  else
    return 2;
}

/*Initilizes the pool of clients*/
void initilizeUsers(int listenfd, active_users *p) {
  for (int i = 0; i < FD_SETSIZE; ++i) {
    p->client[i].fd = -2;
    p->client[i].sec = -1;
    p->client[i].tid = -1;
  }
  p->maxfd = listenfd;
  FD_ZERO(&p->read_set);
  FD_ZERO(&p->login_set);
  FD_ZERO(&p->ready_set);
  FD_ZERO(&p->notlogin_set);
  FD_SET(listenfd, &p->read_set);
  FD_SET(STDIN_FILENO, &p->read_set);
}

/*Adds a client to the pool*/
void addClient(int connfd, active_users *p, char *hostname) {
  p->nready--;
  int i = 0;
  for (i = 0; i < FD_SETSIZE; ++i) {
    if (p->client[i].fd < 0) {
      p->client[i].fd = connfd;
      strcpy(p->client[i].ip, hostname);
      p->client[i].rio.fd = connfd;
      p->client[i].rio.bufptr = p->client[i].rio.buf;
      FD_SET(connfd, &p->read_set);
      FD_SET(connfd, &p->notlogin_set);
      if (connfd > p->maxfd)
        p->maxfd = connfd;
      break;
    }
  }
  if (i == FD_SETSIZE) {
    fprintf(stderr, "\t\x1B[1;31mError: Full.\x1B[0m\n");
  }
}

/*Login and comm thread creation*/
void checkClients(active_users *p) {
  int i, connfd;
  for (i = 0; (i <= FD_SETSIZE) && (p->nready > 0); ++i) {
    connfd = p->client[i].fd;
    /* Login thread */
    if (connfd > 0 && FD_ISSET(connfd, &p->ready_set) &&
        !FD_ISSET(connfd, &p->login_set) &&
        FD_ISSET(connfd, &p->notlogin_set)) {
      p->nready--;
      thread *t = malloc(sizeof(thread));
      t->c = &p->client[i];
      t->p = p;
      pthread_t tid;
      if (!FD_ISSET(connfd, &p->login_set)) {
        sleep(1);
        pthread_create(&tid, NULL, loginThread, t);
      }
      /* Comm thread */
    } else if (connfd > 0 && FD_ISSET(connfd, &p->login_set) &&
               FD_ISSET(connfd, &p->ready_set) &&
               !FD_ISSET(connfd, &p->notlogin_set)) {
      p->nready--;
      thread *t = malloc(sizeof(thread));
      t->c = &p->client[i];
      t->p = p;
      pthread_t tid;
      pthread_create(&tid, NULL, commThread, t);
    }
  }
}

/*
* Prints out what errno error caused the failure and what method caused it
* whatError = 0 when select is called
* whatError = 1 when accept is called
* whatError = 2 when close is called
* whatError = 3 when setsockopt is called
* whatError = 4 when bind is called
* whatError = 5 when listen is called
*/
int ERROR(int whatError) {
  if (errno == 0)
    return 0;
  else if (errno == EBADF)
    fprintf(stderr, "\t\x1B[1;31mError: Bad file descriptor.\n");
  else if (errno == EINTR)
    fprintf(stderr, "\t\x1B[1;31mError: A signal was caught.\n");
  else if (errno == EINVAL && whatError == 0)
    fprintf(stderr, "\t\x1B[1;31mError: The file descriptor is negative.\n");
  else if (errno == ENOMEM)
    fprintf(stderr, "\t\x1B[1;31mError: Unable to allocate enough memory.\n");
  else if (errno == EAGAIN)
    fprintf(stderr, "\t\x1B[1;31mError: Socket is nonnlocking and no "
                    "connections are present.\n");
  else if (errno == ECONNABORTED)
    fprintf(stderr, "\t\x1B[1;31mError: Connection has been aborted.\n");
  else if (errno == EFAULT)
    fprintf(stderr, "\t\x1B[1;31mError: The address argument is not in a "
                    "writeable part of the user address space.\n");
  else if (errno == EINVAL && whatError == 1)
    fprintf(stderr, "\t\x1B[1;31mError: Socket is not listening for "
                    "connections or the length is invalid.\n");
  else if (errno == EMFILE)
    fprintf(stderr, "\t\x1B[1;31mError: The per process limit of open file "
                    "descriptors has been reached.\n");
  else if (errno == ENFILE)
    fprintf(stderr, "\t\x1B[1;31mError: The system limit of open files has "
                    "been reached.\n");
  else if (errno == ENOTSOCK)
    fprintf(
        stderr,
        "\t\x1B[1;31mError: The descriptor references a file, not a socket.\n");
  else if (errno == EOPNOTSUPP)
    fprintf(stderr, "\t\x1B[1;31mError: The referenced socket is not of type "
                    "SOCK_STREAM.\n");
  else if (errno == EPROTO)
    fprintf(stderr, "\t\x1B[1;31mError: Protocol.\n");
  else if (errno == EIO)
    fprintf(stderr, "\t\x1B[1;31mError: I/O.\n");
  else if (errno == EINVAL && whatError == 3)
    fprintf(stderr, "\t\x1B[1;31mError: Optlen is invalid.\n");
  else if (errno == ENOPROTOOPT)
    fprintf(
        stderr,
        "\t\x1B[1;31mError: The option is unknown at the level indicated.\n");
  else if (errno == EADDRINUSE)
    fprintf(stderr,
            "\t\x1B[1;31mError: The specified address is already in use.\n");
  else if (errno == EADDRNOTAVAIL)
    fprintf(stderr,
            "\t\x1B[1;31mError: The specified address is not available.\n");
  else if (errno == EAFNOSUPPORT)
    fprintf(stderr, "\t\x1B[1;31mError: The specified address is not a valid "
                    "address for the address family of the specified "
                    "socket.\n");
  else if (errno == EINVAL && whatError == 4)
    fprintf(stderr, "\t\x1B[1;31mError: The socket is already bound to an "
                    "address or the socket has been shutdown.\n");
  else if (errno == EINVAL && whatError == 5)
    fprintf(
        stderr,
        "\t\x1B[1;31mError: The socket is already connected or shutdown.\n");
  else if (errno == EDESTADDRREQ)
    fprintf(stderr,
            "\t\x1B[1;31mError: The socket is not bound to a local address.\n");
  else if (errno == EACCES)
    fprintf(stderr, "\t\x1B[1;31mError: The calling proccess does not have the "
                    "appropriate privileges.\n");
  else if (errno == ENOBUFS)
    fprintf(stderr, "\t\x1B[1;31mError: Insufficient resources are available "
                    "in the system to complete the call.\x1B[0m\n");
  if (whatError == 0 && errno != 0) {
    fprintf(stderr, "\t\x1B[1;31mError: Failed to select.\x1B[0m\n");
    return 1;
  } else if (whatError == 1 && errno != 0) {
    fprintf(stderr, "\t\x1B[1;31mError: Failed to accept.\x1B[0m\n");
    return 1;
  } else if (whatError == 2 && errno != 0) {
    fprintf(stderr, "\t\x1B[1;31mError: Failed to close.\x1B[0m\n");
    return 1;
  } else if (whatError == 3 && errno != 0) {
    fprintf(stderr, "\t\x1B[1;31mError: Failed to set socket.\x1B[0m\n");
    return 1;
  } else if (whatError == 4 && errno != 0) {
    fprintf(stderr, "\t\x1B[1;31mError: Failed to bind.\x1B[0m\n");
    return 1;
  } else if (whatError == 5 && errno != 0) {
    fprintf(stderr, "\t\x1B[1;31mError: Failed to listen.\x1B[0m\n");
    return 1;
  }
  return 0;
}

/*Interprets server terminal commands*/
int command(active_users *p) {
  char buf[MAXLINE];
  if (!fgets(buf, MAXLINE, stdin)) {
  }
  if (strcmp(buf, "/users\n") == 0) {
    printf("\x1B[0mCurrent Users:\n");
    fprintf(stderr, "______________________________________________________"
                    "__________________________\n");
    for (int i = 0; i < FD_SETSIZE; ++i) {
      if (p->client[i].fd > 0) {
        fprintf(stderr, "NAME = %s\n", p->client[i].name);
        fprintf(stderr, "SALT = %s\n", p->client[i].salt);
        fprintf(stderr, "HASH = %s\n", p->client[i].hash);
        fprintf(stderr, "LOGIN TIME = %d\n", p->client[i].sec);
        fprintf(stderr, "IP = %s\n", p->client[i].ip);
        fprintf(stderr, "______________________________________________________"
                        "__________________________\n");
      }
    }
    return 0;
  } else if (strcmp(buf, "/help\n") == 0) {
    helpServer();
    return 0;
  } else if (strstr(buf, "/shutdown\n") != NULL) {
    shutDown(p);
    return 1;
  } else if (strstr(buf, "/accts\n") != NULL) {
    accounts *temp = acc_Head;
    printf("\x1B[0mCurrent Accounts:\n");
    fprintf(stderr, "______________________________________________________"
                    "__________________________\n");
    while (temp != NULL && temp->c.name[0] != '\0') {
      fprintf(stderr, "NAME = %s\n", temp->c.name);
      fprintf(stderr, "SALT = %s\n", temp->c.salt);
      fprintf(stderr, "HASH = %s\n", temp->c.hash);
      fprintf(stderr, "________________________________________________________"
                      "______________\n");
      temp = temp->next;
    }
    return 0;
  } else {
    return 0;
  }
}

/*Help menu for server*/
void helpServer() {
  fprintf(stderr, "\x1B[0m\t/help\t\tDisplays this help menu.\n");
  fprintf(stderr,
          "\t/shutdown\tDisconnects all users and shuts down the server.\n");
  fprintf(stderr, "\t/users\t\tDisplays a list of the users and their "
                  "information that\n\t\t\tare currently logged in.\n");
  fprintf(stderr, "\t/accts\t\tDisplays a list of all users accounts.\n");
}

/*Shutdown command*/
void shutDown(active_users *p) {
  printf("Shutting down.\n");
  int i;
  for (i = 0; i < FD_SETSIZE; ++i) {
    if (p->client[i].fd > 0) {
      char *buf = "BYE \r\n\r\n";
      int num = write(p->client[i].rio.fd, buf, strlen(buf));
      if (verbose) {
        fprintf(stderr, "\x1B[1;34mSent: BYE Byte Count: %d\n\x1B[0m", num);
      }
      close(p->client[i].fd);
      ERROR(2);
    }
  }
}

/* Actual thread for logging in*/
void *loginThread(void *args) {
  thread *t = (thread *)args;
  client *c = t->c;
  active_users *p = t->p;
  c->tid = pthread_self();
  pthread_detach(pthread_self());
  int error = 0;
  int login = 0;
  int numBytes;
  if (c->fd > 0) {
    numBytes = read(c->fd, c->rio.buf, MAXLINE);
    if (numBytes != 0) {
      if (strstr(c->rio.buf, "WOLFIE \r\n\r\n") != NULL) {
        if (verbose) {
          fprintf(stderr, "\x1B[1;34mRecieved: WOLFIE Byte Count: %d\x1B[0m\n",
                  numBytes);
        }
        char *buf = "EIFLOW \r\n\r\n";
        if (c->fd > 0) {
          int num = write(c->fd, buf, strlen(buf));
          if (verbose) {
            fprintf(stderr, "\x1B[1;34mSent: EIFLOW Byte Count: %d\x1B[0m\n",
                    num);
          }
        }
      } else if (strstr(c->rio.buf, "BYE \r\n\r\n") != NULL) {
        if (verbose) {
          fprintf(stderr, "\x1B[1;34mRecieved: BYE Byte Count: %d\x1B[0m\n",
                  numBytes);
        }
        if (c->fd > 0) {
          close(c->fd);
          if (ERROR(2) > 0) {
            internalError(&pool);
          }
        }
        FD_CLR(c->fd, &p->read_set);
        FD_CLR(c->fd, &p->login_set);
        FD_CLR(c->fd, &p->notlogin_set);
        memset(c->name, 0, MAXLINE);
        memset(c->pass, 0, MAXLINE);
        memset(c->ip, 0, MAXLINE);
        memset(c->hash, 0, MAXLINE);
        memset(c->salt, 0, MAXLINE);
        c->fd = -2;
      } else if (strstr(c->rio.buf, "IAMNEW ") != NULL) {
        char *str;
        str = strstr(c->rio.buf, " ");
        str++;
        char *str2 = c->name;
        while (*str != '\r') {
          *str2 = *str;
          str2++;
          str++;
        }
        *str2 = '\0';
        if (verbose) {
          fprintf(stderr,
                  "\x1B[1;34mRecieved: IAMNEW %s Byte Count: %d\x1B[0m\n",
                  c->name, numBytes);
        }
        accounts *temp = acc_Head;
        while (temp != NULL) {
          if (strcmp(temp->c.name, c->name) == 0 &&
              strlen(c->name) == strlen(temp->c.name)) {
            char *buf = "ERR 00 USER NAME TAKEN \r\n\r\n";
            if (c->fd > 0) {
              int num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                fprintf(stderr, "\x1B[1;34mSent: ERR 00 USER NAME TAKEN Byte "
                                "Count: %d\x1B[0m\n",
                        num);
              }
            }
            memset(c->name, 0, MAXLINE);
            memset(c->ip, 0, MAXLINE);
            memset(c->hash, 0, MAXLINE);
            memset(c->salt, 0, MAXLINE);
            buf = "BYE \r\n\r\n";
            if (c->fd > 0) {
              sleep(1);
              int num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                fprintf(stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                        num);
              }
              error = 1;
            }
            if (c->fd > 0) {
              close(c->fd);
              if (ERROR(2) > 0) {
                internalError(&pool);
              }
              FD_CLR(c->fd, &p->read_set);
              FD_CLR(c->fd, &p->login_set);
              FD_CLR(c->fd, &p->notlogin_set);
              c->fd = -2;
            }
            break;
          }
          temp = temp->next;
        }
        if (c->fd > 0 && error != 1) {
          char *buf = (char *)calloc(MAXLINE, 1);
          sprintf(buf, "HINEW %s \r\n\r\n", c->name);
          if (c->fd > 0) {
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              fprintf(stderr,
                      "\x1B[1;34mSent: HINEW %s Byte Count: %d\x1B[0m\n",
                      c->name, num);
            }
          }
          free(buf);
        }
        fflush(stderr);
        c->rio.bufptr = c->rio.buf;
        memset(c->rio.buf, 0, MAXLINE);
        free(t);
        return NULL;
      } else if (strstr(c->rio.buf, "NEWPASS ") !=
                 NULL) // storing passwords as strings need salt
      {
        char *str;
        str = strstr(c->rio.buf, " ");
        str++;
        char *str2 = c->pass;
        while (*str != '\r') {
          *str2 = *str;
          str2++;
          str++;
        }
        *str2 = '\0';
        if (verbose) {
          fprintf(stderr,
                  "\x1B[1;34mRecieved: NEWPASS %s Byte Count: %d\x1B[0m\n",
                  c->pass, numBytes);
        }
        int bad = checkPassword(c->pass);
        if (bad) {
          char *buf = "ERR 02 BAD PASSWORD \r\n\r\n";
          if (c->fd > 0) {
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              fprintf(
                  stderr,
                  "\x1B[1;34mSent: ERR 02 BAD PASSWORD Byte Count: %d\x1B[0m\n",
                  num);
            }
          }
          memset(c->name, 0, MAXLINE);
          memset(c->pass, 0, MAXLINE);
          memset(c->hash, 0, MAXLINE);
          memset(c->salt, 0, MAXLINE);
          memset(c->ip, 0, MAXLINE);
          buf = "BYE \r\n\r\n";
          if (c->fd > 0) {
            sleep(1);
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              fprintf(stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                      num);
            }
            error = 1;
          }
          if (c->fd > 0) {
            close(c->fd);
            if (ERROR(2) > 0) {
              internalError(&pool);
            }
            FD_CLR(c->fd, &p->read_set);
            FD_CLR(c->fd, &p->login_set);
            FD_CLR(c->fd, &p->notlogin_set);
            c->fd = -2;
          }
        } else {
          time_t rawtime;
          time(&rawtime);
          c->login_t = localtime(&rawtime);
          char *buf = (char *)calloc(MAXLINE, 1);
          sprintf(buf, "SSAPWEN \r\n\r\n");
          if (c->fd > 0) {
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              fprintf(stderr, "\x1B[1;34mSent: SSAPWEN Byte Count: %d\x1B[0m\n",
                      num);
            }
          }
          unsigned char *salt = (unsigned char *)calloc(RAND_BYTE_LENGTH, 1);
          RAND_bytes(salt, RAND_BYTE_LENGTH / 2);
          char *hexSalt = (char *)calloc(RAND_BYTE_LENGTH + 1, 1);
          char *hexSaltPtr = hexSalt;
          for (int i = 0; i < RAND_BYTE_LENGTH / 2; i++) {
            hexSaltPtr += sprintf(hexSaltPtr, "%02x", salt[i]);
          }
          hexSaltPtr = hexSalt;
          char *str3 = c->salt;
          while (*hexSaltPtr != '\0') {
            *str3 = *hexSaltPtr;
            str3++;
            hexSaltPtr++;
          }
          *str3 = '\0';
          unsigned char hash[SHA256_DIGEST_LENGTH];
          SHA256_CTX sha256;
          SHA256_Init(&sha256);
          char *pass =
              (char *)calloc(RAND_BYTE_LENGTH + strlen(c->pass) + 1, 1);
          sprintf(pass, "%s%s", c->pass, hexSalt);

          SHA256_Update(&sha256, pass, RAND_BYTE_LENGTH + strlen(c->pass) + 1);
          SHA256_Final(hash, &sha256);
          char *hexHash = (char *)calloc(SHA256_DIGEST_LENGTH * 2 + 1, 1);
          char *hexHashPtr = hexHash;
          for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
            hexHashPtr += sprintf(hexHashPtr, "%02x", hash[i]);
          }
          str3 = c->hash;
          hexHashPtr = hexHash;
          while (*hexHashPtr != '\0') {
            *str3 = *hexHashPtr;
            str3++;
            hexHashPtr++;
          }
          *str3 = '\0';
          free(buf);
          free(pass);
          free(hexHash);
          free(salt);
          free(hexSalt);
          memset(c->pass, 0, MAXLINE);
          buf = (char *)calloc(MAXLINE, 1);
          c->sec = c->login_t->tm_mday * 24 * 60 * 60;
          c->sec += c->login_t->tm_hour * 60 * 60;
          c->sec += c->login_t->tm_min * 60;
          c->sec += c->login_t->tm_sec;
          sprintf(buf, "HI %s \r\n\r\n", c->name);
          if (c->fd > 0) {
            sleep(1);
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              fprintf(stderr, "\x1B[1;34mSent: HI %s Byte Count: %d\x1B[0m\n",
                      c->name, num);
            }
          }
          free(buf);
          strcpy(cur->c.name, c->name); // adds to end of list
          strcpy(cur->c.salt, c->salt);
          strcpy(cur->c.hash, c->hash);
          accounts *temp = cur;
          cur = cur->next;
          cur = (accounts *)malloc(sizeof(accounts));
          cur->prev = temp;
          temp->next = cur;
          cur->next = NULL;
          cur->prev = NULL;
          buf = (char *)calloc(MAXLINE, 1);
          sprintf(buf, "%s\n", c->name);
          write(fp, buf, strlen(buf));
          free(buf);
          buf = (char *)calloc(MAXLINE, 1);
          sprintf(buf, "%s\n", c->hash);
          write(fp, buf, strlen(buf));
          free(buf);
          buf = (char *)calloc(MAXLINE, 1);
          sprintf(buf, "%s\n", c->salt);
          write(fp, buf, strlen(buf));
          free(buf);
          buf = (char *)calloc(MAXLINE, 1);
          sprintf(buf, "MOTD %s \r\n\r\n", MOTD);
          if (c->fd > 0) {
            sleep(1);
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              fprintf(stderr, "\x1B[1;34mSent: MOTD %s Byte Count: %d\x1B[0m\n",
                      MOTD, num);
            }
            fflush(stderr);
            free(buf);
            FD_SET(c->fd, &p->login_set);
            FD_CLR(c->fd, &p->notlogin_set);
            c->rio.bufptr = c->rio.buf;
            memset(c->rio.buf, 0, MAXLINE);
            free(t);
            return NULL;
          }
        }
      } else if (strstr(c->rio.buf, "IAM") != NULL) // IAM part 3
      {
        char *str;
        str = strstr(c->rio.buf, " ");
        str++;
        char *str2 = c->name;
        while (*str != '\r') {
          *str2 = *str;
          str2++;
          str++;
        }
        *str2 = '\0';
        if (verbose) {
          fprintf(stderr, "\x1B[1;34mRecieved: IAM %s Byte Count: %d\x1B[0m\n",
                  c->name, numBytes);
        }
        int i = 0;
        for (i = 0; i < FD_SETSIZE; ++i) {
          if (p->client[i].fd > 0) {

            if (strcmp(p->client[i].name, c->name) == 0 &&
                strlen(c->name) == strlen(p->client[i].name) &&
                c->fd != p->client[i].fd) {
              char *buf = "ERR 00 USER NAME TAKEN \r\n\r\n";
              if (c->fd > 0) {
                int num = write(c->fd, buf, strlen(buf));
                if (verbose) {
                  fprintf(stderr, "\x1B[1;34mSent: ERR 00 USER NAME TAKEN Byte "
                                  "Count: %d\x1B[0m\n",
                          num);
                }
              }
              memset(c->name, 0, MAXLINE);
              memset(c->ip, 0, MAXLINE);
              memset(c->hash, 0, MAXLINE);
              memset(c->salt, 0, MAXLINE);
              buf = "BYE \r\n\r\n";
              if (c->fd > 0) {
                sleep(1);
                int num = write(c->fd, buf, strlen(buf));
                if (verbose) {
                  fprintf(stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                          num);
                }
                error = 1;
              }
              if (c->fd > 0) {
                close(c->fd);
                if (ERROR(2) > 0) {
                  internalError(&pool);
                }
                FD_CLR(c->fd, &p->read_set);
                FD_CLR(c->fd, &p->login_set);
                FD_CLR(c->fd, &p->notlogin_set);
                c->fd = -2;
              }
              break;
            }
          }
        }
        if (!error) {
          accounts *temp = acc_Head;
          while (temp != NULL) {
            if (strcmp(temp->c.name, c->name) == 0 &&
                strlen(c->name) == strlen(temp->c.name)) {
              char *buf = (char *)calloc(MAXLINE, 1);
              sprintf(buf, "AUTH %s \r\n\r\n", c->name);
              if (c->fd > 0) {
                int num = write(c->fd, buf, strlen(buf));
                if (verbose) {
                  fprintf(stderr,
                          "\x1B[1;34mSent: AUTH %s Byte Count: %d\x1B[0m\n",
                          c->name, num);
                }
                error = 1;
              }
              break;
            }
            temp = temp->next;
          }
          if (!error) {
            char *buf = "ERR 01 USER NOT AVAIBLE \r\n\r\n";
            if (c->fd > 0) {
              int num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                fprintf(stderr, "\x1B[1;34mSent: ERR 01 USER NOT AVAIBLE Byte "
                                "Count: %d\x1B[0m\n",
                        num);
              }
            }
            memset(c->name, 0, MAXLINE);
            memset(c->ip, 0, MAXLINE);
            memset(c->hash, 0, MAXLINE);
            memset(c->salt, 0, MAXLINE);
            buf = "BYE \r\n\r\n";
            if (c->fd > 0) {
              sleep(1);
              int num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                fprintf(stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                        num);
              }
              error = 0;
            }
            if (c->fd > 0) {
              close(c->fd);
              if (ERROR(2) > 0) {
                internalError(&pool);
              }
              FD_CLR(c->fd, &p->read_set);
              FD_CLR(c->fd, &p->login_set);
              FD_CLR(c->fd, &p->notlogin_set);
              c->fd = -2;
            }
          }
        }
      } else if (strstr(c->rio.buf, "PASS") != NULL) {
        char *str;
        str = strstr(c->rio.buf, " ");
        str++;
        char *str2 = c->pass;
        while (*str != '\r') {
          *str2 = *str;
          str2++;
          str++;
        }
        *str2 = '\0';
        if (verbose) {
          fprintf(stderr, "\x1B[1;34mRecieved: PASS %s Byte Count: %d\x1B[0m\n",
                  c->pass, numBytes);
        }
        accounts *temp = acc_Head;
        while (temp != NULL) {
          if (strcmp(temp->c.name, c->name) == 0 &&
              strlen(c->name) == strlen(temp->c.name)) {
            unsigned char hash[SHA256_DIGEST_LENGTH];
            SHA256_CTX sha256;
            SHA256_Init(&sha256);
            char *pass =
                (char *)calloc(RAND_BYTE_LENGTH + strlen(c->pass) + 1, 1);
            sprintf(pass, "%s%s", c->pass, temp->c.salt);
            SHA256_Update(&sha256, pass,
                          RAND_BYTE_LENGTH + strlen(c->pass) + 1);
            SHA256_Final(hash, &sha256);
            char *hexHash = (char *)calloc(SHA256_DIGEST_LENGTH * 2 + 1, 1);
            char *hexHashPtr = hexHash;
            for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
              hexHashPtr += sprintf(hexHashPtr, "%02x", hash[i]);
            }
            char *str3 = c->hash;
            hexHashPtr = hexHash;
            while (*hexHashPtr != '\0') {
              *str3 = *hexHashPtr;
              str3++;
              hexHashPtr++;
            }
            *str3 = '\0';
            free(pass);
            free(hexHash);
            if (!strcmp(temp->c.hash, c->hash) == 0 ||
                strlen(c->hash) != strlen(temp->c.hash)) {
              char *buf = "ERR 02 BAD PASSWORD \r\n\r\n";
              if (c->fd > 0) {
                int num = write(c->fd, buf, strlen(buf));
                if (verbose) {
                  fprintf(stderr, "\x1B[1;34mSent: ERR 02 BAD PASSWORD Byte "
                                  "Count: %d\x1B[0m\n",
                          num);
                }
              }
              memset(c->name, 0, MAXLINE);
              memset(c->ip, 0, MAXLINE);
              memset(c->pass, 0, MAXLINE);
              memset(c->hash, 0, MAXLINE);
              memset(c->salt, 0, MAXLINE);
              buf = "BYE \r\n\r\n";
              if (c->fd > 0) {
                sleep(1);
                int num = write(c->fd, buf, strlen(buf));
                if (verbose) {
                  fprintf(stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                          num);
                }
                error = 1;
              }
              if (c->fd > 0) {
                close(c->fd);
                if (ERROR(2) > 0) {
                  internalError(&pool);
                }
                FD_CLR(c->fd, &p->read_set);
                FD_CLR(c->fd, &p->login_set);
                FD_CLR(c->fd, &p->notlogin_set);
                c->fd = -2;
              }
              break;
            }
          }
          temp = temp->next;
        }
        if (!error) {
          time_t rawtime;
          time(&rawtime);
          c->login_t = localtime(&rawtime);
          char *buf = (char *)calloc(MAXLINE, 1);
          sprintf(buf, "SSAP \r\n\r\n");
          if (c->fd > 0) {
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              fprintf(stderr, "\x1B[1;34mSent: SSAP Byte Count: %d\x1B[0m\n",
                      num);
            }
          }
          free(buf);
          buf = (char *)calloc(MAXLINE, 1);
          c->sec = c->login_t->tm_mday * 24 * 60 * 60;
          c->sec += c->login_t->tm_hour * 60 * 60;
          c->sec += c->login_t->tm_min * 60;
          c->sec += c->login_t->tm_sec;
          sprintf(buf, "HI %s \r\n\r\n", c->name);
          if (c->fd > 0) {
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              fprintf(stderr, "\x1B[1;34mSent: HI %s Byte Count: %d\x1B[0m\n",
                      c->name, num);
            }
            free(buf);
            buf = (char *)calloc(MAXLINE, 1);
            sprintf(buf, "MOTD %s \r\n\r\n", MOTD);
            if (c->fd > 0) {
              sleep(1);
              int num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                fprintf(stderr,
                        "\x1B[1;34mSent: MOTD %s Byte Count: %d\x1B[0m\n", MOTD,
                        num);
              }
              fflush(stderr);
              free(buf);
              FD_SET(c->fd, &p->login_set);
              FD_CLR(c->fd, &p->notlogin_set);
              c->rio.bufptr = c->rio.buf;
              memset(c->rio.buf, 0, MAXLINE);
              free(t);
              return NULL;
            }
          }
        }
      }
      c->rio.bufptr = c->rio.buf;
      memset(c->rio.buf, 0, MAXLINE);
    } else {
      login = 3;
    }
    if (login == 2) {
      FD_SET(c->fd, &p->login_set);
    } else if (login == 1) {
      if (!FD_ISSET(c->fd, &p->login_set)) {
        char *buf = "BYE \r\n\r\n";
        if (c->fd > 0) {
          int num = write(c->fd, buf, strlen(buf));
          if (verbose) {
            fprintf(stderr, "\x1B[1;34mSent: BYE Byte Count: %d\n\x1B[0m", num);
          }
          close(c->fd);
          if (ERROR(2) > 0) {
            internalError(&pool);
          }
        }
      }
      FD_CLR(c->fd, &p->read_set);
      FD_CLR(c->fd, &p->login_set);
      FD_CLR(c->fd, &p->notlogin_set);
      memset(c->name, 0, MAXLINE);
      memset(c->pass, 0, MAXLINE);
      memset(c->ip, 0, MAXLINE);
      memset(c->hash, 0, MAXLINE);
      memset(c->salt, 0, MAXLINE);
      c->fd = -2;
    } else if (login == 3) {
      if (c->fd > 0) {
        close(c->fd);
        if (ERROR(2) > 0) {
          internalError(&pool);
        }
      }
      FD_CLR(c->fd, &p->read_set);
      FD_CLR(c->fd, &p->login_set);
      FD_CLR(c->fd, &p->notlogin_set);
      memset(c->name, 0, MAXLINE);
      memset(c->pass, 0, MAXLINE);
      memset(c->ip, 0, MAXLINE);
      memset(c->hash, 0, MAXLINE);
      memset(c->salt, 0, MAXLINE);
      c->fd = -2;
    }
  }
  free(t);
  return NULL;
}

/* Actual thread for communication*/
void *commThread(void *args) {
  thread *t = (thread *)args;
  client *c = t->c;
  active_users *p = t->p;
  pthread_detach(pthread_self());
  int comm = 0;
  int error = 0;
  if (c->fd > 0) {
    int numBytes = read(c->fd, c->rio.buf, MAXLINE);
    if (numBytes != 0) {
      if (strcmp(c->rio.buf, "LISTU \r\n\r\n") == 0) {
        if (verbose) {
          fprintf(stderr, "\x1B[1;34mRecieved: LISTU Byte Count: %d\x1B[0m\n",
                  numBytes);
        }
        char *buf;
        int i = 0;
        int active = 0;
        for (i = 0; i < FD_SETSIZE; ++i) {
          if (p->client[i].fd > 0) {
            active++;
          }
        }
        buf = (char *)calloc(MAXLINE * active + 5 * active + 8, 1);
        sprintf(buf, "UTSIL");
        for (i = 0; i < FD_SETSIZE; ++i) {
          if (p->client[i].fd > 0) {
            sprintf(buf, "%s %s \r\n", buf, p->client[i].name);
          }
        }
        sprintf(buf, "%s\r\n", buf);

        if (c->fd > 0) {
          int num = write(c->fd, buf, strlen(buf));
          free(buf);
          buf = (char *)calloc(MAXLINE * active + 5 * active + 8, 1);
          if (verbose) {
            fprintf(stderr, "\x1B[1;34mSent: UTSIL");
            for (i = 0; i < FD_SETSIZE; ++i) {
              if (p->client[i].fd > 0) {
                sprintf(buf, "%s %s ", buf, p->client[i].name);
              }
            }
            fprintf(stderr, "%sByte Count: %d\n\x1B[0m", buf, num);
          }
          free(buf);
        }
      } else if (strcmp(c->rio.buf, "BYE \r\n\r\n") == 0) {
        if (verbose) {
          fprintf(stderr, "\x1B[1;34mRecieved: BYE Byte Count: %d\x1B[0m\n",
                  numBytes);
        }
        if (error == 0) {
          char *buf = "BYE \r\n\r\n";
          if (c->fd > 0) {
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              fprintf(stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                      num);
            }
          }
        }
        FD_CLR(c->fd, &p->login_set);
        FD_CLR(c->fd, &p->read_set);
        if (c->fd > 0) {
          close(c->fd);
          if (ERROR(2) > 0) {
            internalError(&pool);
          }
          c->fd = -2;
          uoff(c, p);
          memset(c->name, 0, MAXLINE);
          memset(c->pass, 0, MAXLINE);
          memset(c->ip, 0, MAXLINE);
          memset(c->hash, 0, MAXLINE);
          memset(c->salt, 0, MAXLINE);
        }
      } else if (strcmp(c->rio.buf, "TIME \r\n\r\n") == 0) {
        if (verbose) {
          fprintf(stderr, "\x1B[1;34mRecieved: TIME Byte Count: %d\x1B[0m\n",
                  numBytes);
        }
        char *buf = (char *)calloc(MAXLINE, 1);
        time_t rawtime;
        struct tm *timeinfo;
        time(&rawtime);
        timeinfo = localtime(&rawtime);
        int sec = timeinfo->tm_mday * 24 * 60 * 60;
        sec += timeinfo->tm_hour * 60 * 60;
        sec += timeinfo->tm_min * 60;
        sec += timeinfo->tm_sec;
        sprintf(buf, "EMIT %d \r\n\r\n", sec - c->sec);
        if (c->fd > 0) {
          int num = write(c->fd, buf, strlen(buf));
          if (verbose) {
            fprintf(stderr, "\x1B[1;34mSent: EMIT %d Byte Count: %d\x1B[0m\n",
                    sec - c->sec, num);
          }
        }
        free(buf);
      } else if (strstr(c->rio.buf, "MSG") != NULL) {
        char *str = strstr(c->rio.buf, " ");
        str++;
        client *c1 = (client *)malloc(sizeof(client));
        client *c2 = (client *)malloc(sizeof(client));
        char *str2 = c1->name;
        while (*str != ' ') {
          *str2 = *str;
          str2++;
          str++;
        }
        *str2 = '\0';
        str++;
        str2 = c2->name;
        while (*str != ' ') {
          *str2 = *str;
          str2++;
          str++;
        }
        *str2 = '\0';
        char *msg = (char *)calloc(MAXLINE, 1);
        str++;
        str2 = msg;
        while (*str != '\r') {
          *str2 = *str;
          str2++;
          str++;
        }
        str2--;
        *str2 = '\0';
        char *buf = (char *)calloc(MAXLINE, 1);
        if (verbose) {
          fprintf(stderr,
                  "\x1B[1;34mRecieved: MSG %s %s %s Byte Count: %d\x1B[0m\n",
                  c1->name, c2->name, msg, numBytes);
        }
        int i, loggedin = -1;
        for (i = 0; i < FD_SETSIZE; ++i) {
          if (p->client[i].fd > 0) {
            fprintf(stderr, "%s\n", c1->name);
            fprintf(stderr, "%s\n", c2->name);
            fprintf(stderr, "%s\n", p->client[i].name);
            if (strcmp(p->client[i].name, c1->name) == 0 &&
                strlen(c1->name) == strlen(p->client[i].name)) {
              if (c->fd > 0) {
                int num = write(c->fd, c->rio.buf, strlen(c->rio.buf));
                int num2 =
                    write(p->client[i].fd, c->rio.buf, strlen(c->rio.buf));
                if (verbose) {
                  fprintf(
                      stderr,
                      "\x1B[1;34mSent: MSG %s %s %s Byte Count: %d\x1B[0m\n",
                      c1->name, c2->name, msg, num);
                  fprintf(
                      stderr,
                      "\x1B[1;34mSent: MSG %s %s %s Byte Count: %d\x1B[0m\n",
                      c1->name, c2->name, msg, num2);
                }
                loggedin = 0;
                break;
              }
            }
          }
        }
        if (loggedin == -1) {
          char *buf = "ERR 01 USER NOT AVAIBLE \r\n\r\n";
          if (c->fd > 0) {
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              fprintf(stderr, "\x1B[1;34mSent: ERR 01 USER NOT AVAIBLE Byte "
                              "Count: %d\x1B[0m\n",
                      num);
            }
          }
        }
        free(c1);
        free(c2);
        free(msg);
        free(buf);
      }
      c->rio.bufptr = c->rio.buf;
      memset(c->rio.buf, 0, MAXLINE);
    } else {
      comm = 1;
    }
    if (comm == 1) {
      FD_CLR(c->fd, &p->login_set);
      FD_CLR(c->fd, &p->read_set);
      c->fd = -2;
      uoff(c, p);
      memset(c->name, 0, MAXLINE);
      memset(c->pass, 0, MAXLINE);
      memset(c->ip, 0, MAXLINE);
      memset(c->hash, 0, MAXLINE);
      memset(c->salt, 0, MAXLINE);
    }
  }
  free(t);
  return NULL;
}

/* Broadcasts UOFF*/
void uoff(client *c, active_users *p) {
  int i;
  for (i = 0; i < FD_SETSIZE; ++i) {
    if (p->client[i].fd > 0) {
      char *buf = (char *)calloc(MAXLINE, 1);
      sprintf(buf, "UOFF %s \r\n\r\n", c->name);
      int num = write(p->client[i].fd, buf, strlen(buf));
      if (verbose) {
        fprintf(stderr, "\x1B[1;34mSent: UOFF %s Byte Count: %d\x1B[0m\n",
                c->name, num);
      }
      free(buf);
    }
  }
}

/* Checks new passwords */
int checkPassword(char password[]) {
  int i = 0;
  bool hasLength = false;
  bool hasUpper = false;
  bool hasSymbol = false;
  bool hasNumber = false;
  if (strlen(password) >= 5) {
    hasLength = true;
  }
  for (i = 0; i < strlen(password); i++) {
    if (password[i] >= 65 && password[i] <= 90) {
      hasUpper = true;
    }
    if (password[i] >= 33 && password[i] <= 47) {
      hasSymbol = true;
    }
    if (password[i] >= 48 && password[i] <= 57) {
      hasNumber = true;
    }
  }
  if ((hasLength && hasUpper) && (hasSymbol && hasNumber))
    return 0;
  else
    return 1;
}

/* Broadcasts ERR 100 INTERNAL SERVER ERROR*/
void internalError(active_users *p) {
  int i;
  for (i = 0; i < FD_SETSIZE; ++i) {
    if (p->client[i].fd > 0) {
      char *buf = (char *)calloc(MAXLINE, 1);
      sprintf(buf, "ERR 100 INTERNAL SERVER ERROR \r\n\r\n");
      int num = write(p->client[i].fd, buf, strlen(buf));
      if (verbose) {
        fprintf(stderr, "\x1B[1;34mSent: ERR 100 INTERNAL SERVER ERROR Byte "
                        "Count: %d\x1B[0m\n",
                num);
      }
      free(buf);
    }
  }
}
