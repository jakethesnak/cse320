#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/rand.h>
#include <openssl/sha.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#define _GNU_SOURCE

#define MAXLINE 4096
#define RAND_BYTE_LENGTH 256
//#define _GNU_SOURCE
typedef struct sockaddr SA;

typedef struct {
  int fd;
  char *bufptr;
  char buf[MAXLINE];
} rio_t;

typedef struct {
  struct tm* login_t;
  int sec;
  int fd;
  pthread_t tid;
  char name[MAXLINE];
  char ip[MAXLINE];
  char pass[MAXLINE];
  char salt[RAND_BYTE_LENGTH * 2];
  char hash[SHA256_DIGEST_LENGTH * 2];
  rio_t rio;
} client;

typedef struct {
  int maxfd;
  fd_set read_set;
  fd_set ready_set;
  fd_set login_set;
  fd_set notlogin_set;
  int nready;
  client client[FD_SETSIZE];
} active_users;

typedef struct {
  client *c;
  active_users *p;
} thread;

typedef struct {
  client *c1;
  client *c2;
  active_users *p;
} chat;

struct __attribute__((__packed__)) accounts {
    client c;
    struct accounts *next;
    struct accounts *prev;
};
typedef struct accounts accounts;

void addClient(int connfd, active_users *p, char *hostname);
static int callback(void *data, int argc, char **argv, char **colName);
void *chatThread(void *args);
void checkClients(active_users *p);
int checkPassword(char password[]);
int command(active_users *p);
int commandLineArgs(int argc, char **argv);
void *commThread(void *args);
int ERROR(int whatError);
void helpArgs();
void helpServer();
void initilizeUsers(int listenfd, active_users *p);
void internalError(active_users *p);
void *loginThread(void *args);
int open_listenfd(int port);
void shutDown(active_users *p);
void uoff(client *c, active_users *p);
int validate_args(const char *input_path);