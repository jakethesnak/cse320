#include "client.h"

bool readyToGo = false;
int socketFD = -50;
int connectNum = -50;
bool connectionSuccess = false;
fd_set read_set, ready_set;
bool helpMenuFlag = false;
bool verboseFlag = false;
bool initLogin = false;
bool createClient = false;
bool auditFilePath = false;
bool random2 = false;
char* username = NULL;
char* password = NULL;
int chatFDSet[1000] = { 0 };
int chatFDSetCounter = 0;
int auditFD = -1;
long int portNum = -1;
struct sockaddr_in sa;
bool handleInSigHandler = false;
bool handleInSelect = false;
char MOTD[1024];

char serverResponse[MAXLINE];
int largestDesc = -1;

typedef struct chatNode {
  char usernameOfClient[1024];
  char usernameOfRecipient[1024];
  int chatFileDescrip;
  struct chatNode* next;
  pid_t pid;
} chatNode;

chatNode* head = NULL;
// the pair of socketfd's
int sv[2] = { 0 };
int main(int argc, char* argv[]) {
  signal(SIGKILL, signal_catch2);
  // lets presume for now we only get IP and PORt.
  if (argc <= 2) {
    helpMenuFlag = true;
  }
  int opt = 0;
  while ((opt = getopt(argc, argv, "hcva")) != -1) {
    switch (opt) {
      case 'h':
        /* The help menu was selected */
        helpMenuFlag = true;
        break;

      case 'v': // level of verbosity
        verboseFlag = true;
        break;

      case 'c':
        createClient = true;
        break;

      case 'a':
        auditFilePath = true;
        break;

      case '?':
        /* Let this case fall down to default;
                 * handled during bad option.*/ /*
*/
      default:
        /* A bad option was provided. */
        helpMenuFlag = true;
        exit(EXIT_FAILURE);
        break;
    }
  } // get opt loop end

  if (helpMenuFlag) {
    // fprintf(stderr, "VERBOSE: %d\n", verboseFlag);
    helpMenu();
    exit(EXIT_SUCCESS);
  }

  // create audit file
  char auditFileName[1024];
  if (auditFilePath) {
    strcpy(auditFileName, argv[argc - 4]);
  } else {
    strcpy(auditFileName, "audit.log");
  }

  auditFD = open(auditFileName, (O_CREAT | O_APPEND | O_WRONLY),
                 (S_IRWXU | S_IRWXG | S_IRWXO));
  fprintf(stderr, "AUDIT FD: %d\n", auditFD);

  sa.sin_family = AF_INET;
  int itWorked = inet_pton(AF_INET, argv[argc - 2], &(sa.sin_addr));

  //  printf("%s \n", argv[1]);
  if (itWorked == 1) {
    //  printf("%d \n", sa.sin_addr.s_addr);
    char* p;
    portNum = strtol(argv[argc - 1], &p, 10);
    sa.sin_port = htons(portNum);
    // printf("Port Number is %X \n", sa.sin_port);
    readyToGo = true;
  } else {
    printf("INCORRECT IP %d \n", sa.sin_addr.s_addr);
  }

  if (readyToGo) {
    socketFD = socket(AF_INET, SOCK_STREAM, 0);
    connectNum = connect(socketFD, (struct sockaddr*)&sa, sizeof(sa));
    // fprintf(stderr, "CONNECTNUM %d \n", connectNum);
    // setup input/output streams
    if (connectNum == 0) {
      connectionSuccess = true;
    }
  }

  if (connectionSuccess) {

    //  fprintf(stderr, "INSIDE CONNECTION SUCC\n");
    // init login
    char* buff = "WOLFIE \r\n\r\n";

    memset(serverResponse, '\0', MAXLINE);
    write(socketFD, buff, strlen(buff));
    if (verboseFlag) {
      fprintf(stderr, "\x1B[1;34m Sent: %s which is %lu bytes\n", buff,
              strlen(buff));
    }
    //  fprintf(stderr, "%d \n", bytesSent);
    read(socketFD, serverResponse, MAXLINE);
    if (verboseFlag) {
      fprintf(stderr, "\x1B[1;34m Recieved: %s \n", serverResponse);
    }

    if (strcmp(serverResponse, "EIFLOW \r\n\r\n") == 0) {
      // fprintf(stderr, "EIFLOW RECIEVED!\n");
      username = argv[argc - 3];

      if (!createClient) {
        // existing client
        char username2[MAXLINE];
        strcpy(username2, "IAM ");
        strcpy(username2 + 4, username);
        strcpy(username2 + 4 + strlen(username), "\r\n\r\n");
        write(socketFD, username2, strlen(username2));
        if (verboseFlag) {
          fprintf(stderr, "\x1B[1;34m Sent: %s which is %lu bytes\n", username2,
                  strlen(username2));
        }
        //  fprintf(stderr, "SENT USERNAME%s\n", username2);
        memset(serverResponse, '\0', MAXLINE);
        read(socketFD, serverResponse, MAXLINE);
        if (verboseFlag) {
          fprintf(stderr, "\x1B[1;34m Recieved: %s \n", serverResponse);
        }
        //  fprintf(stderr, "SERVER RESPONSE %s\n", serverResponse);
        if (strstr(serverResponse, "AUTH") != NULL) {
          //  fprintf(stderr, "INSIDE ASKING FOR PASSWORD\n");
          char* password = getpass("Enter Password: ");
          char password2[MAXLINE];
          strcpy(password2, "PASS ");
          strcpy(password2 + 5, password);
          strcpy(password2 + 5 + strlen(password), " \r\n\r\n");
          //  sleep(1);
          int bytesWrit = write(socketFD, password2, strlen(password2));
          if (verboseFlag) {
            fprintf(stderr, "\x1B[1;34m Sent: %s which is %d bytes\n",
                    password2, bytesWrit);
          }
          //  fprintf(stderr, "WROTE PASSWORD %s\n", password2);
          memset(serverResponse, '\0', MAXLINE);
          read(socketFD, serverResponse, MAXLINE);

          if (verboseFlag) {
            fprintf(stderr, "\x1B[1;34m Recieved: %s \n", serverResponse);
          }
          //  fprintf(stderr, "Server Response: %s\n", serverResponse);
          if (strstr(serverResponse, "SSAP") != NULL) {
            if (strstr(serverResponse, "HI") != NULL) {
              // fprintf(stderr, "I AM HERE :)\n");
              memset(serverResponse, '\0', MAXLINE);
              read(socketFD, serverResponse, MAXLINE);
              if (verboseFlag) {
                fprintf(stderr, "\x1B[1;34m Recieved: %s \n", serverResponse);
              }
              if (strstr(serverResponse, "MOTD") != NULL) {
                // fprintf(stderr, "I AM HERE2 :)\n");
                initLogin = true;
                strcpy(MOTD, serverResponse);
              }
            }
            memset(serverResponse, '\0', MAXLINE);
            read(socketFD, serverResponse, MAXLINE);
            if (verboseFlag) {
              fprintf(stderr, "\x1B[1;34m Recieved: %s \n", serverResponse);
            }
            if (strstr(serverResponse, "HI") != NULL) {
              if (verboseFlag) {
                fprintf(stderr, "%s\n", serverResponse);
              }
              if (strstr(serverResponse, "MOTD") != NULL) {
                initLogin = true;
                strcpy(MOTD, serverResponse);
              } else {
                memset(serverResponse, '\0', MAXLINE);
                read(socketFD, serverResponse, MAXLINE);
                if (verboseFlag) {
                  fprintf(stderr, "\x1B[1;34m Recieved: %s \n", serverResponse);
                }
                if (strstr(serverResponse, "MOTD") != NULL) {
                  if (verboseFlag) {
                    fprintf(stderr, "%s\n", serverResponse);
                  }
                  initLogin = true;
                }
              }
            }
          } else if (strstr(serverResponse, "BAD PASSWORD") != NULL) {
            fprintf(stderr, "\x1B[1;31m %s\n", serverResponse);
            writeToAudit(serverResponse, true, false, false, false);
          }
        } else if (strstr(serverResponse, "USER NOT") != NULL) {
          fprintf(stderr, "\x1B[1;31m %s\n", serverResponse);
          writeToAudit(serverResponse, true, false, false, false);

        } else if (strstr(serverResponse, "USER NAME TAKEN") != NULL) {
          fprintf(stderr, "\x1B[1;31m %s\n", serverResponse);
          writeToAudit(serverResponse, true, false, false, false);
        }

      } else {
        // new client
        // fprintf(stderr, "INSIDE ELSE\n");
        char username2[MAXLINE];
        strcpy(username2, "IAMNEW ");
        strcpy(username2 + 7, username);
        strcpy(username2 + 7 + strlen(username), "\r\n\r\n");
        write(socketFD, username2, strlen(username2));
        if (verboseFlag) {
          fprintf(stderr, "\x1B[1;34m Sent: %s which is %lu bytes\n", username2,
                  strlen(username2));
        }
        //  fprintf(stderr, "USERNAME WRITTEN: %s and bytes sent %lu\n",
        //  username2,
        //  strlen(username2));
        memset(serverResponse, '\0', MAXLINE);
        // fprintf(stderr, "AFTER USERNAME MEMSET\n");
        read(socketFD, serverResponse, MAXLINE);
        if (verboseFlag) {
          fprintf(stderr, "\x1B[1;34m Recieved: %s \n", serverResponse);
        }
        // fprintf(stderr, "Server Response: %s \n", serverResponse);
        if (strstr(serverResponse, "HINEW") != NULL) {
          // fprintf(stderr, "INSIDE HINEW\n");
          char* password = getpass("Enter Password: ");
          char password2[MAXLINE];
          strcpy(password2, "NEWPASS ");
          strcpy(password2 + 8, password);
          strcpy(password2 + 8 + strlen(password), "\r\n\r\n");
          write(socketFD, password2, strlen(password2));
          if (verboseFlag) {
            fprintf(stderr, "\x1B[1;34m Sent: %s which is %lu bytes\n",
                    password2, strlen(password2));
          }
          // fprintf(stderr, "PASSWORD WRITTEN: %s\n", password2);
          memset(serverResponse, '\0', MAXLINE);
          read(socketFD, serverResponse, MAXLINE);
          if (verboseFlag) {
            fprintf(stderr, "\x1B[1;34m Recieved: %s \n", serverResponse);
          }
          if (strstr(serverResponse, "SSAPWEN") != NULL) {
            // fprintf(stderr, "INSIDE  SSAPWEN\n");
            memset(serverResponse, '\0', MAXLINE);
            read(socketFD, serverResponse, MAXLINE);
            if (verboseFlag) {
              fprintf(stderr, "\x1B[1;34m Recieved: %s \n", serverResponse);
            }
            if (strstr(serverResponse, "HI") != NULL) {
              if (strstr(serverResponse, "MOTD ") != NULL) {
                initLogin = true;
                strcpy(MOTD, serverResponse);
                //  fprintf(stderr, "INSIDE HI\n");
                if (verboseFlag) {
                  fprintf(stderr, "%s\n", serverResponse);
                }
                memset(serverResponse, '\0', MAXLINE);
              } else {
                // fprintf(stderr, "INSIDE HI\n");
                fprintf(stderr, "%s\n", serverResponse);
                memset(serverResponse, '\0', MAXLINE);
                read(socketFD, serverResponse, MAXLINE);
                if (verboseFlag) {
                  fprintf(stderr, "\x1B[1;34m Recieved: %s \n", serverResponse);
                }
                if (strstr(serverResponse, "MOTD") != NULL) {
                  //  fprintf(stderr, "INSIDE MOTD\n");
                  fprintf(stderr, "%s\n", serverResponse);
                  strcpy(MOTD, serverResponse);
                  memset(serverResponse, '\0', MAXLINE);
                  initLogin = true;
                }
              }
            }
          } else if (strstr(serverResponse, "BAD PASSWORD") != NULL) {
            fprintf(stderr, "\x1B[1;31m %s\n", serverResponse);
            writeToAudit(serverResponse, true, false, false, false);
          }
        } else if (strstr(serverResponse, "USER NAME TAKEN") != NULL) {
          fprintf(stderr, " \x1B[1;31m %s\n", serverResponse);
          writeToAudit(serverResponse, true, false, false, false);
        }
      }
      if (initLogin) {
        initLoginMethod();
      }
    }
    return 0;
  }
}

int helpMenu() {

  printf("./client [-hcv] NAME SERVER_IP SERVER_PORT\n");
  printf("-h\tDisplays Help Menu and return EXIT_SUCCESS\n");
  printf("-c\tRequest to server to create a new user");
  printf("-v\tVerbose print all incoming and outgoing protocol verbs and "
         "contents\n");
  printf("NAME\tThis is the uersname to display when chatting\n");
  printf("SERVER_IP\tThe ipAddress of the server to connect to\n");
  printf("SERVER_PORT\tThe port to connect to\n");

  return EXIT_SUCCESS;
}

int handleSTDIN(char buf[]) {

  // read command line from stdin
  // fprintf(stderr, "INSIDE STDIN\n");
  if (fgets(buf, MAXLINE, stdin) != NULL) {

    if (strstr(buf, "/logout\n") != NULL) {
      random2 = true;
      char* buf1 = "BYE \r\n\r\n";
      write(socketFD, buf1, strlen(buf));
      if (verboseFlag) {
        fprintf(stderr, "\x1B[1;34m Sent: %s which is %lu bytes\n", buf1,
                strlen(buf1));
      }
      int byeBytes = read(socketFD, serverResponse, MAXLINE);
      if (verboseFlag) {
        fprintf(stderr, "\x1B[1;34m Response: %s\n", serverResponse);
      }
      if (byeBytes <= 0) {
        fprintf(stderr, "Broken Pipe\n");
        return 0;
      }
      writeToAudit("/logout", false, false, false, true);
      if (byeBytes >= 3) {
        // fprintf(stderr, "%s\n", serverResponse);
      }

      exit(EXIT_SUCCESS);
    } else if (strstr(buf, "/time\n") != NULL) {
      random2 = true;
      //  fprintf(stderr, "You Typed In /Time1\n");
      char* buf1 = "TIME \r\n\r\n";
      write(socketFD, buf1, strlen(buf1));
      if (verboseFlag) {
        fprintf(stderr, "\x1B[1;34m Sent: %s which is %lu bytes\n", buf1,
                strlen(buf1));
      }
      // fprintf(stderr, "BytesWritten: %d ", bytesWritten);
      memset(serverResponse, '\0', MAXLINE);
      int timeBytes = read(socketFD, serverResponse, MAXLINE);
      if (verboseFlag) {
        fprintf(stderr, "time\x1B[1;34m Response: %s\n", serverResponse);
      }
      if (timeBytes <= 0) {
        fprintf(stderr, "Broken Pipe\n");
        return 0;
      }
      // fprintf(stderr, "ServerResp:%s\n", serverResponse);
      fflush(stderr);
      writeToAudit("/time", false, false, false, true);
      long int seconds = atoi(serverResponse + 5);
      int days = seconds / (60 * 60 * 24);
      seconds -= days * (60 * 60 * 24);
      int hours = seconds / (60 * 60);
      seconds -= hours * (60 * 60);
      int minutes = seconds / 60;
      if (days > 0) {
        fprintf(stderr, "Days:%d\n", days);
      }
      if (hours > 0) {
        fprintf(stderr, "Hours:%d\n", hours);
      }
      if (minutes > 0) {
        fprintf(stderr, "Minutes:%d\n", minutes);
      }
      if (seconds > 0) {
        fprintf(stderr, "Seconds:%lu\n", seconds);
      }
    } else if (strcmp(buf, "/listu\n") == 0) {
      random2 = true;
      fprintf(stderr, "You typed listu");
      char* buf2 = "LISTU \r\n\r\n";
      write(socketFD, "LISTU \r\n\r\n", strlen(buf2));
      writeToAudit("LISTU", false, false, false, true);
      if (verboseFlag) {
        fprintf(stderr, "\x1B[1;34m Sent: %s which is %lu bytes\n", buf2,
                strlen(buf2));
      }
      int bytesAv = 0;
      ioctl(socketFD, FIONREAD, &bytesAv);
      if (bytesAv <= 0) {
        fprintf(stderr, "Broken Pipe\n");
        return 0;
      }
      char serverResponse2[bytesAv];
      memset(serverResponse2, '\0', bytesAv);
      read(socketFD, serverResponse2, bytesAv);
      if (verboseFlag) {
        fprintf(stderr, "\x1B[1;34mResponse: %s\n", serverResponse);
      }
      char* token = strtok(serverResponse2, " ");
      int counter = 0;
      while (token != NULL) {
        if (counter >= 1) {
          fprintf(stderr, "\x1B[0m%s \n", token);
        }
        counter++;
        token = strtok(NULL, " ");
      }

    } else if (strcmp(buf, "/help") == 0) {
      fprintf(stderr, "Help Typed");
      writeToAudit("/help", false, false, false, true);
      helpMenu();
    } else if (strstr(buf, "/chat") != NULL) {
      fprintf(stderr, "YoU Typed /chat\n");
      char* chatArgs[200];
      int j = 0;
      int numOfWords = 0;
      for (j = 0; j < 200; j++) {
        chatArgs[j] = (char*)malloc(1024);
      }
      j = 0;
      char* token = strtok(buf, " ");
      while (token != NULL) {
        strcpy(chatArgs[j], token);
        j++;
        numOfWords++;
        token = strtok(NULL, " ");
      }
      fprintf(stderr, "%d\n", numOfWords);
      char* actualMsg = (char*)malloc(1024);
      int size = 0;
      for (j = 2; j < numOfWords; j++) {
        // fprintf(stderr, "Copying: %d which is %s \n", j, chatArgs[j]);
        strcpy((actualMsg + size), chatArgs[j]);
        size += strlen(chatArgs[j]);
        strcpy((actualMsg + size), " ");
        size += 1;
      }
      // fprintf(stderr, "Actual Msg: %s\n", actualMsg);
      writeToAudit("/chat", false, false, false, true);
      char chatRecipient[1024];
      strcpy(chatRecipient, chatArgs[1]);
      // fprintf(stderr, "Chat Recipient %s \n", chatRecipient);

      char* sendMsgToServer = (char*)malloc(
          strlen(actualMsg) + strlen(username) + strlen(chatRecipient) + 11);
      strcpy(sendMsgToServer, "MSG ");
      strcpy(sendMsgToServer + 4, chatRecipient);
      strcpy(sendMsgToServer + 4 + strlen(chatRecipient), " ");
      strcpy(sendMsgToServer + 5 + strlen(chatRecipient), username);
      strcpy(sendMsgToServer + 5 + strlen(chatRecipient) + strlen(username),
             " ");
      strcpy(sendMsgToServer + 5 + strlen(chatRecipient) + strlen(username) + 1,
             actualMsg);
      actualMsg[strlen(actualMsg) - 2] = '\0';
      strcpy(sendMsgToServer + 5 + strlen(chatRecipient) + strlen(username) +
                 1 + strlen(actualMsg),
             " \r\n\r\n");
      fprintf(stderr, "Send Msg To Server %s \n", sendMsgToServer);
      memset(serverResponse, '\0', 1024);
      write(socketFD, sendMsgToServer, (strlen(actualMsg) + strlen(username) +
                                        strlen(chatRecipient) + 11));
      if (verboseFlag) {
        fprintf(stderr, "\x1B[1;34m Sent: %s which is %lu bytes\n",
                sendMsgToServer, strlen(sendMsgToServer));
      }
      for (j = 0; j < 200; j++) {
        free(chatArgs[j]);
      }
    }
  }

  return 0;
}

int handleServer() {
  fprintf(stderr, "Server Sent: %s \n", serverResponse);
  if (random2) {
    random2 = false;
    return 0;
  }
  char newBuf[MAXLINE];
  memset(newBuf, '\0', MAXLINE);
  int i = 0;
  read(socketFD, newBuf, MAXLINE);
  if (verboseFlag) {
    fprintf(stderr, "\x1B[1;34m Recieved: %s \n", newBuf);
  }
  if (strstr(newBuf, "MSG") != NULL) {

    // MSG format is: MSG TO FROM ACTUALMSG \r\n\r\n
    char* chatArgs[200];
    int j = 0;
    int numOfWords = 0;
    for (j = 0; j < 200; j++) {
      chatArgs[j] = (char*)malloc(1024);
    }
    j = 0;
    char* token = strtok(newBuf, " ");
    while (token != NULL) {
      strcpy(chatArgs[j], token);
      j++;
      numOfWords++;
      token = strtok(NULL, " ");
    }

    char* actualMsg = (char*)malloc(1024);
    int size = 0;
    for (j = 3; j < numOfWords; j++) {
      // fprintf(stderr, "Copying: %d which is %s \n", j, chatArgs[j]);
      strcpy((actualMsg + size), chatArgs[j]);
      size += strlen(chatArgs[j]);
      strcpy((actualMsg + size), " ");
      size += 1;
    }

    char MSGTO[1024];
    strcpy(MSGTO, chatArgs[1]);
    char MSGFROM[1024];
    strcpy(MSGFROM, chatArgs[2]);
    chatNode* curr = head;
    bool windowExists = false;
    while (curr != NULL) {
      fprintf(stderr, "CHECKING IF CHAT ALREADY EXISTS: \n");
      listTraverse2();
      if ((strstr(curr->usernameOfClient, MSGTO) != NULL) ||
          (strstr(curr->usernameOfRecipient, MSGFROM) != NULL)) {
        windowExists = true;
        write(curr->chatFileDescrip, actualMsg, strlen(actualMsg));

        // MSG FORM: MSG TO FROM ____MSG____
        writeToAudit2("MSG", MSGTO, MSGFROM, actualMsg);
        break;
      } else if ((strstr(curr->usernameOfRecipient, MSGTO) != NULL) ||
                 (strstr(curr->usernameOfClient, MSGFROM) != NULL)) {
        windowExists = true;
        //  writeToAudit(char *msg, bool login, bool chatMSG, bool logout, bool
        //  command);
        write(curr->chatFileDescrip, actualMsg, strlen(actualMsg));
        writeToAudit2("MSG", MSGTO, MSGFROM, actualMsg);
        break;
      } else {
        curr = curr->next;
      }
    }
    fprintf(stderr, "WINDOW EXISTS? : %d\n", windowExists);
    if (!windowExists) {
      if (socketpair(AF_UNIX, SOCK_STREAM, 0, sv)) {
        perror("socket pair");
        exit(1);
      }

      struct sigaction psa;
      psa.sa_handler = signal_catch;
      sigaction(SIGCHLD, &psa, NULL);
      pid_t child = fork();
      if (child == 0) {
        // child

        char* argv2[15];
        argv2[0] = "/usr/bin/xterm";
        argv2[1] = "-fg";
        argv2[2] = "PapayaWhip";
        argv2[3] = "-bg";
        argv2[4] = "rgb:00/00/80";
        argv2[5] = "-geometry";

        // 0 should be replaced with the offset in terms of pixel
        int total = 0;
        char offSetBuf[15];
        memset(offSetBuf, '\0', 15);
        sprintf(offSetBuf, "45x35+%d", total);
        total += 100;
        argv2[6] = offSetBuf;
        argv2[7] = "-title";
        // THIS SHOULD BE USERNAME
        char usernameBuf[50];
        memset(usernameBuf, '\0', 50);
        if (strstr(username, MSGTO) != NULL)
          sprintf(usernameBuf, "%s", MSGFROM);
        else
          sprintf(usernameBuf, "%s", MSGTO);
        argv2[8] = usernameBuf;
        argv2[9] = "-e";
        argv2[10] = "./chat";
        // pass in childs socket number instead of 3.
        char buf[MAXLINE];
        memset(buf, '\0', MAXLINE);
        // sv[1] is now what the chat will use to communicate with me.
        sprintf(buf, "%d", sv[1]);
        argv2[11] = buf;
        argv2[12] = actualMsg;
        argv2[13] = username;
        argv2[14] = NULL;
        // sv[0] is what I (the client) will use to communicate with
        // the chat.
        close(sv[0]);
        execvp("/usr/bin/xterm", argv2);
      } else {

        // parent
        close(sv[1]);
        fprintf(stderr, "Inside Parent\n");
        char chatRecipient[1024];
        memset(chatRecipient, '\0', 1024);
        if (strstr(MSGTO, username) == NULL) {
          strcpy(chatRecipient, MSGTO);
        } else {
          strcpy(chatRecipient, MSGFROM);
        }
        int returnVal =
            addToChatNodeList(sv[0], username, chatRecipient, child);
        if (returnVal < 0) {
          FD_ZERO(&read_set);
          FD_SET(STDIN_FILENO, &read_set);
          FD_SET(socketFD, &read_set);
          int i = 0;
          for (i = 0; i < chatFDSetCounter; i++) {
            FD_SET(chatFDSet[i], &read_set);
          }

        } else {
          FD_ZERO(&read_set);
          FD_SET(STDIN_FILENO, &read_set);
          FD_SET(socketFD, &read_set);
          // create array of clientFD and loop through and set them.
          addChatFD(sv[0]);

          int i = 0;
          for (i = 0; i < chatFDSetCounter; i++) {
            FD_SET(chatFDSet[i], &read_set);
          }
          write(sv[0], actualMsg, strlen(actualMsg));
          //  FD_SET(sv[0], &read_set);
          if (sv[0] > largestDesc)
            largestDesc = sv[0];
        }
      }
    }
  }

  if (strstr(newBuf, "BYE") != NULL) {
    if (verboseFlag) {
      fprintf(stderr, "\x1B[1;31m%s\n", newBuf);
    }
    exit(EXIT_SUCCESS);
  }
  if (strstr(newBuf, "UOFF") != NULL) {
    fprintf(stderr, "\x1B[1;31m%s\n", newBuf);
    return 0;
  }
  if (strstr(newBuf, "ERR 01")) {
    fprintf(stderr, "\x1B[1;31m User Not Available\n");
    return 0;
  } else {
    //  fprintf(stderr, " %s \n", newBuf);
    // fprintf(stderr, "Incomign Bytes: %d \n", incomingBytes);
    char* protocolLoc = strstr(newBuf, "\r\n\r\n");
    if (protocolLoc == NULL) {
      fprintf(stderr, "Protocol Missing \r\n\r\n");
    } else {
      char* p = newBuf;

      while (p != protocolLoc) {
        fprintf(stderr, "%c", *(newBuf + i));
        p++;
        i++;
      }
    }
  }
  return 0;
}

int handleChats(int fd) {
  if (handleInSigHandler) {
    return 0;
  } else {
    handleInSelect = true;
  }

  int bytesAv = 0;
  ioctl(fd, FIONREAD, &bytesAv);
  char* chatBuf2 = (char*)malloc(bytesAv);
  memset(chatBuf2, '\0', bytesAv);
  //  fprintf(stderr, "BYTESAV: %d\n", bytesAv);
  int chatBytesRead2 = read(fd, chatBuf2, bytesAv);
  char* chatBuf = (char*)calloc(bytesAv, 1);
  int i = 0;
  int j = 0;
  for (i = 0; i < bytesAv; i++) {
    chatBuf[j] = chatBuf2[i];
    j++;
  }

  if (verboseFlag) {
    fprintf(stderr, "\x1B[1;34m Receieved From Chat: %s and %lu BytesAV:%d \n",
            chatBuf, strlen(chatBuf), bytesAv);
  }

  if (chatBytesRead2 <= 0) {
    fprintf(stderr, "Broken Pipe INSIDE HANDLE CHATS!\n");
    chatNode* current = head;
    chatNode* prev = current;
    while (current != NULL) {
      if (current->chatFileDescrip == fd) {
        break;
      }
      prev = current;
      current = current->next;
    }
    if (current == NULL) {
      // end of list
      fprintf(stderr, "Current is NULL ....\n");
      //  addToChatNhandleChatsodeList(int fileDescript, char *usernameOfClient,
      //  char
      //  *usernameOfRecipient, pid_t child)

    } else if (current == prev) {
      // we are at the head;
      removeChatFD(current->chatFileDescrip);
      head = head->next;

    } else {
      //  fprintf(stderr, "INSIDE ELSE CONDITION\n");
      removeChatFD(current->chatFileDescrip);
      prev->next = current->next;
    }
    initLoginMethod();
    return 0;
  }

  char recipient[1024];
  memset(recipient, '\0', 1024);
  // send msg to recipient
  chatNode* current = head;
  while (current != NULL) {
    if (current->chatFileDescrip == fd) {
      strcpy(recipient, current->usernameOfRecipient);
      // fprintf(stderr, "WE FOUND FILE DESCRIPTOR MATCH\n");
      break;
    }
    current = current->next;
  }

  char* fullmsg =
      (char*)malloc(chatBytesRead2 + strlen(username) + strlen(recipient) + 11);
  memset(fullmsg, '\0',
         chatBytesRead2 + strlen(username) + strlen(recipient) + 11);
  strcpy(fullmsg, "MSG ");
  strcpy(fullmsg + 4, recipient);
  strcpy(fullmsg + 4 + strlen(recipient), " ");
  strcpy(fullmsg + strlen(recipient) + 5, username);
  strcpy(fullmsg + strlen(recipient) + 5 + strlen(username), " ");
  strcpy(fullmsg + strlen(recipient) + strlen(username) + 6, chatBuf);
  strcpy(fullmsg + strlen(recipient) + strlen(username) + 6 + strlen(chatBuf),
         " \r\n\r\n");
  write(socketFD, fullmsg, strlen(fullmsg));
  if (verboseFlag) {
    fprintf(stderr,
            "\x1B[1;34m Sent From Chat handler: %s which is %lu bytes\n",
            fullmsg, strlen(fullmsg));
  }
  free(fullmsg);
  free(chatBuf);
  return 0;
}

int addToChatNodeList(int fileDescript, char usernameOfClient[],
                      char usernameOfRecipient[], pid_t child) {

  // fprintf(stderr, "inside chatnode list add method\n");
  // fprintf(stderr, " FD: %d   client username: %s     recipient username:
  // %s\n",
  //  fileDescript, usernameOfClient, usernameOfRecipient);

  // head is null?
  if (head == NULL) {
    head = (chatNode*)malloc(sizeof(chatNode));
    head->chatFileDescrip = fileDescript;
    strcpy(head->usernameOfClient, usernameOfClient);
    strcpy(head->usernameOfRecipient, usernameOfRecipient);
    head->pid = child;
    head->next = NULL;
  } else {
    chatNode* current = head;
    while (current->next != NULL) {
      current = current->next;
    }
    current->next = (chatNode*)malloc(sizeof(chatNode));
    current->next->chatFileDescrip = fileDescript;
    strcpy(current->next->usernameOfClient, usernameOfClient);
    strcpy(current->next->usernameOfRecipient, usernameOfRecipient);
    current->next->pid = child;
    current->next->next = NULL;
  }

  return 0;
}

int listTraverse(char checkName[]) {
  chatNode* current = head;
  while (current != NULL) {
    fprintf(stderr, "Iteration: FD: %d Username: %s Recipient: %s\n",
            current->chatFileDescrip, current->usernameOfClient,
            current->usernameOfRecipient);
    if (strstr(current->usernameOfRecipient, checkName) != NULL) {
      return -1;
    } else
      current = current->next;
  }
  return 0;
}

void signal_catch(int sigNum) {
  if (!handleInSelect) {
    handleInSigHandler = true;
  } else {
    handleInSigHandler = false;
    return;
  }
  //  fprintf(stderr, "INSIDE SIGNAL HANDLER\n");

  pid_t pid;
  int status;
  while ((pid = waitpid(-1, &status, WNOHANG)) > 0)
    ;

  chatNode* current = head;
  chatNode* prev = current;
  while (current != NULL) {
    if (current->pid == pid) {
      break;
    }
    prev = current;
    current = current->next;
  }
  if (current == NULL) {
    // end of list
    fprintf(stderr, "Current is NULL ....\n");
    handleInSigHandler = false;
    //  addToChatNodeList(int fileDescript, char *usernameOfClient, char
    //  *usernameOfRecipient, pid_t child)
  } else if (current == prev) {
    // we are at the head;
    removeChatFD(current->chatFileDescrip);
    head = head->next;
    handleInSigHandler = false;
    return;
  } else {
    //  fprintf(stderr, "INSIDE ELSE CONDITION\n");
    removeChatFD(current->chatFileDescrip);
    prev->next = current->next;
    handleInSigHandler = false;
    return;
  }

  fprintf(stderr, "\x1B[0m \n");
}

void signal_catch2(int sigNum) {
  // send bye and close socketFD
}

int listTraverse2() {
  fprintf(stderr, "\n\n\n");
  chatNode* current = head;
  while (current != NULL) {
    fprintf(stderr, "Iteration: FD: %d Username: %s Recipient: %s\n",
            current->chatFileDescrip, current->usernameOfClient,
            current->usernameOfRecipient);

    current = current->next;
  }
  return 0;
}
int removeChatFD(int fd) {
  int i = 0;
  for (i = 0; i < chatFDSetCounter; i++) {
    if (chatFDSet[i] == fd) {
      close(fd);
      chatFDSet[i] = -1;
      chatFDSetCounter--;
      break;
    }
  }
  setSelectFDs();
  return 0;
}

int addChatFD(int fd) {
  chatFDSet[chatFDSetCounter] = fd;
  chatFDSetCounter++;
  setSelectFDs();
  return 0;
}

int setSelectFDs() {
  FD_ZERO(&read_set);
  FD_SET(STDIN_FILENO, &read_set);
  FD_SET(socketFD, &read_set);

  int i = 0;
  for (i = 0; i < chatFDSetCounter; i++) {
    FD_SET(chatFDSet[i], &read_set);
    if (chatFDSet[i] > largestDesc)
      largestDesc = chatFDSet[i];
  }

  return 0;
}

int writeToAudit(char* msg, bool login, bool chatMSG, bool logout,
                 bool command) {
  int flockLockReturn = flock(auditFD, LOCK_EX);
  fprintf(stderr, "Did it lock? %d\n", flockLockReturn);
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  char* auditMsg = (char*)malloc(150 + strlen(msg));
  if (initLogin && login) {
    snprintf(auditMsg, MAXLINE,
             "%d-%d-%d %d:%d:%d, %s LOGIN, %d:%lu, success, %s",
             tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour,
             tm.tm_min, tm.tm_sec, username, sa.sin_addr.s_addr, portNum, MOTD);
    fprintf(stderr, "AUDIT MSG: %s\n", auditMsg);
    write(auditFD, auditMsg, strlen(auditMsg));
  } else if (login) {
    snprintf(auditMsg, MAXLINE, "%d-%d-%d %d:%d:%d, %s LOGIN, %d:%lu, fail, %s",
             tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour,
             tm.tm_min, tm.tm_sec, username, sa.sin_addr.s_addr, portNum,
             serverResponse);
    fprintf(stderr, "AUDIT MSG: %s\n", auditMsg);
    write(auditFD, auditMsg, strlen(auditMsg));
  } else if (chatMSG) {
    snprintf(
        auditMsg, MAXLINE, "%d-%d-%d %d:%d:%d, %s MSG, %d:%lu, success, %s",
        tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
        tm.tm_sec, username, sa.sin_addr.s_addr, portNum, serverResponse);
    fprintf(stderr, "AUDIT MSG: %s\n", auditMsg);
    write(auditFD, auditMsg, strlen(auditMsg));
  } else if (logout) {
    snprintf(
        auditMsg, MAXLINE, "%d-%d-%d %d:%d:%d, %s LOGOUT, %d:%lu, success, %s",
        tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
        tm.tm_sec, username, sa.sin_addr.s_addr, portNum, serverResponse);
    fprintf(stderr, "AUDIT MSG: %s\n", auditMsg);
    write(auditFD, auditMsg, strlen(auditMsg));
  } else if (command) {
    snprintf(auditMsg, MAXLINE,
             "%d-%d-%d %d:%d:%d, %s: %s CMD, %d:%lu, success, %s\n",
             tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour,
             tm.tm_min, tm.tm_sec, username, msg, sa.sin_addr.s_addr, portNum,
             serverResponse);
    fprintf(stderr, "AUDIT MSG: %s\n", auditMsg);
    write(auditFD, auditMsg, strlen(auditMsg));
  }
  int didFlockUnlock = flock(auditFD, LOCK_UN);
  fprintf(stderr, "Did flock unlock? %d\n", didFlockUnlock);
  return 0;
}

int writeToAudit2(char* msg, char* to, char* from, char* chatMSG) {
  chatMSG[strlen(chatMSG) - 7] = '\0';
  int flockLockReturn = flock(auditFD, LOCK_EX);
  fprintf(stderr, "Did it lock2? %d\n", flockLockReturn);
  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  char* auditMsg = (char*)malloc(150 + strlen(chatMSG));
  snprintf(auditMsg, MAXLINE, "%d-%d-%d %d:%d:%d, %s MSG: %s to: %s from: %s\n",
           tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
           tm.tm_sec, username, chatMSG, to, from);
  fprintf(stderr, "AUDIT MSG: %s\n", auditMsg);
  write(auditFD, auditMsg, strlen(auditMsg));
  flock(auditFD, LOCK_UN);
  return 0;
}

int initLoginMethod() {
  writeToAudit("Login", true, false, false, false);
  // fprintf(stderr, "INSIDE INITLOGIN\n");
  char buf[MAXLINE];
  FD_ZERO(&read_set);
  FD_SET(STDIN_FILENO, &read_set);
  FD_SET(socketFD, &read_set);
  // struct timeval* time = (struct timeval*)malloc(sizeof(struct
  // timeval));
  // time->tv_sec = 10;
  // time->tv_usec = 100;
  largestDesc = socketFD;
  while (true) {
    // fprintf(stderr, "INSIDE SELECT WHILE LOOP\n");
    ready_set = read_set;
    select(largestDesc + 1, &ready_set, NULL, NULL, NULL);
    if (FD_ISSET(STDIN_FILENO, &ready_set)) {
      handleSTDIN(buf);
    }
    if (FD_ISSET(socketFD, &ready_set)) {
      handleServer();
    } else {
      int i = 0;
      for (i = 0; i < chatFDSetCounter; i++) {
        if (FD_ISSET(chatFDSet[i], &ready_set)) {
          handleChats(chatFDSet[i]);
        }
      }
    }
  }
}
