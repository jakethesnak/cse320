#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <errno.h>
//#include <ncurses.h>
#include <fcntl.h>
#include <signal.h>
#include <termios.h>

int binaryCheck(char **argv, char **envp);
char *runPWD();
int runEcho(char **argv, char **envp);
int runBinaries(char **argv, char **envp);
int runCD(char **envp);
int runSet(char **envp);
int runHelp();
int runFg();
int runBg();
int runJobs();
char *replaceVariableWithValue(char **envp);
int addToHistory(char *command);
int traverseHistory(bool upKeyPressed);
int sampleRedirect(char **argv, char **envp);
int sampleRedirect2(char **argv, char **envp);
int samplePipe(char **argv, char **envp);
void fixArguments();
void fixArguments2();
void runHistory();
void clearHistory();
