#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "functionHeaders.h"

// Assume no input line will be longer than 1024 bytes
#define MAX_INPUT 1024

// simple shell global variables
char *allTokensGlobal[MAX_INPUT];
int allTokensCounterGlobal = 0;
char *allPathTokensGlobal[MAX_INPUT];
int allPathTokensCounterGlobal = 0, debug_global = -1;
char *pwd, *oldpwd, *cwd2, *set1, *set2;
bool variableUse = false;
char *printValue;
int methodReturnVal = 0;
int isBinaryCommand = -1;
char **history;
int historyCounter = 0;
int historyTraverse;
bool redirectionReq = false;
int redirectionCounter = 0;
int totalRedirectionArgs = 0;
bool redirectIn = false;
bool redirectOut = false;
bool pipeReq = false;
char pipeArgs[1024][1024];
int pipeArgsCounter = 0;
int totalRedirections = 0;
int rv;
bool upPressed = false;
bool downPressed = false;
int lengthOfCommand = 0;
char *prompt = "320sh> ";
int sizeOfLastCommand = 0;
int totalCommandsSaved = 0;

volatile int *bgPids;
int bgPidsCounter = 0;
volatile pid_t pid = 0;
struct job_control {
  int num;
  pid_t pid;
  int isRunning;
  char *name[MAX_INPUT];
};
typedef struct job_control job;
int *jobsCounter;

job backgroundJobs[MAX_INPUT];
job foregroundJob;
typedef void (*handler_t)(int);
handler_t signal(int signum, handler_t handler);

int exitStatus = 0;
int isBinary = 0;
siginfo_t *infop;

void sigchld_handler(int signum) {

  for (int i = 0; i < 1024; ++i) {
    if (jobsCounter[i] == 1) {
      infop->si_pid = 0;
      int status = waitid(P_ALL, 0, infop, WEXITED | WNOHANG);
      if (status == 0 && infop->si_pid != 0) {
        if (infop->si_status != 24) {
          bgPids[bgPidsCounter] = infop->si_pid;
          bgPidsCounter++;
          exitStatus = infop->si_status;
        }
        jobsCounter[i] = 0;
      }
    }
  }
  pid = 1;
}
int TIME, MIN;

int main(int argc, char **argv, char **envp) {
  infop = (siginfo_t *)calloc(sizeof(infop), 1);
  history = (char **)malloc(20480);
  signal(SIGCHLD, sigchld_handler);
  int i = 0;
  for (i = 0; i < 20; i++) {
    history[i] = (char *)malloc(2024 + 1);
  }
  pwd = (char *)malloc(1024);
  oldpwd = (char *)malloc(1024);
  memset(oldpwd, 0, MAX_INPUT);
  set1 = (char *)malloc(1024);
  set2 = (char *)malloc(1024);
  cwd2 = (char *)malloc(1024);
  if (argv[1] != NULL) {
    if (strcmp(argv[1], "-d") == 0)
      debug_global = 0;

    else
      debug_global = -1;
  }
  int finished = 0;

  struct termios t;
  tcgetattr(0, &t);
  t.c_lflag &= ~(ECHO | ICANON);
  tcsetattr(0, TCSANOW, &t);
  TIME = t.c_cc[VTIME];
  MIN = t.c_cc[VMIN];
  jobsCounter = (int *)calloc(1024, 4);
  bgPids = (int *)calloc(1024, 1);

  while (!finished) {
    t.c_cc[VTIME] = TIME;
    t.c_cc[VMIN] = MIN;
    tcsetattr(0, TCSANOW, &t);
    char *cursor;
    char last_char;
    int count = 0, max_length = 0, left = 0;
    char *cmd = (char *)calloc(1024, 1);
    // Print the prompt
    rv = write(1, "[", 1);
    rv = write(1, runPWD(), strlen(runPWD()));
    rv = write(1, "] ", 2);
    rv = write(1, prompt, strlen(prompt));
    if (!rv) {
      finished = 1;
      break;
    }

    int isQuote = 0;
    int overallQuote = 0;
    int lineLocation = 0;
    int lineLength = 0;
    // read and parse the input
    for (rv = 1, count = 0, cursor = cmd, last_char = 1;
         rv && (max_length < (MAX_INPUT)); cursor++) {
      int ch, k = 0;
      ch = getchar();
      last_char = ch;
      if (last_char == 34 && !overallQuote) {
        overallQuote = 1;
      } else if (last_char == 34 && overallQuote) {
        overallQuote = 0;
      }
      if (last_char == 34 && isQuote == 0) {
        isQuote = 1;
      } else if (isQuote == 1 && last_char == 34) {
        isQuote = 0;
      }
      if (last_char == '\n' && !overallQuote) {
        if (count != max_length) {
          cursor = cmd;
          for (int i = 0; i < max_length; ++i) {
            cursor++;
          }
        }
        if (max_length == 0) {
          *cursor = ' ';
          cursor++;
        }
        *cursor = '\n';
        cursor++;
        write(1, &last_char, 1);
        break;
      }
      if (isQuote && last_char == 10) {
        if (count != max_length) {
          cursor = cmd;
          for (int i = 0; i < max_length; ++i) {
            cursor++;
          }
        }
        *cursor = '\n';
        write(1, &last_char, 1);
        write(1, "> ", 2);
        lineLocation = 0;
        left = 0;
        max_length++;
        count = max_length;
        lineLength = 0;
      }
      if (ch == 27) {
        k = 1;
        ch = getchar();
      }

      if (ch == 91 && k == 1) {
        k = 2;
        ch = getchar();
      }
      if (ch == 65 && k == 2) {
        free(cmd);
        cmd = (char *)calloc(1024, 1);
        cursor = cmd;
        int isexit = 0;
        if (totalCommandsSaved == 0) {
          int historyTotalFD =
              open("historyFileTotal.txt", (O_RDONLY | O_CREAT),
                   (S_IRUSR | S_IWUSR | S_IXUSR));
          char totalCommandsStr[5];
          int returnVal = read(historyTotalFD, totalCommandsStr, sizeof(int));
          close(historyTotalFD);
          int i = 0;
          int total = 0;
          int mult = 1;
          for (i = 0; i < (returnVal - 1); i++) {
            mult *= 10;
          }

          for (i = 0; i < (returnVal); i++) {
            total += (totalCommandsStr[i] - 48) * mult;
            mult /= 10;
          }
          totalCommandsSaved = total;
        }
        int historyFileDescriptor;
        char *cmd2 = (char *)calloc(1024, 1);
        historyFileDescriptor = open("historyFile.txt", O_RDONLY);
        if (totalCommandsSaved >= (historyTraverse + 1))
          historyTraverse++;
        else {
          printf("\033[%dD", sizeOfLastCommand);
          // save cursor position
          printf("\033[s");
          // clear to end of line
          printf("\033[K");
          // go back to saved position.
          printf("\033[u");
          // char end[] = "End Of Shell History Resetting Traversal";
          sizeOfLastCommand = 0;
          historyTraverse = 0;
          isexit = 1;
        }

        // up presssed
        if (!isexit) {
          if (sizeOfLastCommand != 0) {
            // move back __ lines
            printf("\033[%dD", sizeOfLastCommand);
            // save cursor position
            printf("\033[s");
            // clear to end of line
            printf("\033[K");
            // go back to saved position.
            printf("\033[u");
          }
          lseek(historyFileDescriptor, (-1024 * historyTraverse), SEEK_END);
          read(historyFileDescriptor, cmd2, 1022);
          int i = 0;
          int j = 0;
          cursor = cmd;
          for (i = 0; i < MAX_INPUT; i++) {
            if (cmd2[i] == '~')
              break;
            else if (cmd2[i] != '`') {
              cmd[j] = cmd2[i];
              cursor++;
              j++;
            }
          }
          cmd[j] = '\0';
          cursor--;
          left = 0;
          sizeOfLastCommand = j;
          if (sizeOfLastCommand > 0) {
            printf("%s", cmd);
          }
          max_length = j;
          count = j;
          lineLocation = j;
          lineLength = j;
          free(cmd2);
          close(historyFileDescriptor);
        }
      }
      if (ch == 66 && k == 2) {
        free(cmd);
        cmd = (char *)calloc(1024, 1);
        cursor = cmd;
        int isexit = 0;
        if (totalCommandsSaved == 0) {
          int historyTotalFD =
              open("historyFileTotal.txt", (O_RDONLY | O_CREAT),
                   (S_IRUSR | S_IWUSR | S_IXUSR));
          char totalCommandsStr[5];
          int returnVal = read(historyTotalFD, totalCommandsStr, sizeof(int));
          close(historyTotalFD);
          int i = 0;
          int total = 0;
          int mult = 1;
          for (i = 0; i < (returnVal - 1); i++) {
            mult *= 10;
          }
          for (i = 0; i < (returnVal); i++) {
            total += (totalCommandsStr[i] - 48) * mult;
            mult /= 10;
          }
          totalCommandsSaved = total;
        }
        int historyFileDescriptor;
        char *cmd2 = (char *)calloc(1024, 1);
        historyFileDescriptor = open("historyFile.txt", O_RDONLY);
        if (0 < (historyTraverse - 1))
          historyTraverse--;
        else {
          printf("\033[%dD", sizeOfLastCommand);
          // save cursor position
          printf("\033[s");
          // clear to end of line
          printf("\033[K");
          // go back to saved position.
          printf("\033[u");
          sizeOfLastCommand = 0;
          historyTraverse = totalCommandsSaved;
          isexit = 1;
        }
        if (!isexit) {
          if (sizeOfLastCommand != 0) {
            // printf("HI :)");
            // move back __ lines
            printf("\033[%dD", sizeOfLastCommand);
            // save cursor position
            printf("\033[s");
            // clear to end of line
            printf("\033[K");
            // go back to saved position.
            printf("\033[u");
          }
          lseek(historyFileDescriptor, (-1024 * historyTraverse), SEEK_END);
          read(historyFileDescriptor, cmd2, 1024);
          int i = 0;
          int j = 0;
          cursor = cmd;
          for (i = 0; i < MAX_INPUT; i++) {
            if (cmd2[i] == '~')
              break;
            else if (cmd2[i] != '`') {
              cmd[j] = cmd2[i];
              cursor++;
              j++;
            }
          }
          cmd[j] = '\0';
          cursor--;
          left = 0;
          sizeOfLastCommand = j;
          if (sizeOfLastCommand > 0) {
            printf("%s", cmd);
          }
          max_length = j;
          count = j;
          lineLocation = j;
          lineLength = j;
          free(cmd2);
          close(historyFileDescriptor);
        }
      }
      if (ch == 1) {
        if (strlen(cmd) > 1 || lineLocation != 0) {
          // move cursor to start of the line
          int difference = strlen(cmd);
          difference--;
          printf("\033[%dD", (difference + 1));
          lineLocation = 0;
          left = max_length;
          count = 0;
          cursor = cmd;
          cursor--;
        }
      }
      if (ch == 11) {
        // delete all text from position of the cursor in cmd buffer
        int i = 0;
        for (i = lineLocation; i < lineLength; i++) {
          *(cmd + i) = '\0';
        }

        for (i = 0; i < lineLocation; i++) {
          write(1, "\b \b", 3);
        }
        write(1, cmd, strlen(cmd));
        cursor--;
      }
      if (ch == 67 && k == 2) {
        if (left != 0) {
          left--;
          lineLocation++;
          count++;
          printf("\x1B[1C");
          lseek(1, 1, SEEK_CUR);
          if (*cursor == '\"' && isQuote) {
            isQuote = 0;
          } else if (*cursor == '\"' && !isQuote) {
            isQuote = 1;
          }
        } else {
          cursor--;
        }
      }
      if (ch == 68 && k == 2) {
        if (lineLocation > 0) {
          lineLocation--;
          write(1, "\b", 1);
          cursor--;
          count--;
          left++;
          if (*cursor == '\"' && isQuote) {
            isQuote = 0;
          } else if (*cursor == '\"' && !isQuote) {
            isQuote = 1;
          }
        }
        cursor--;
      }
      if (last_char == 127) {
        if (lineLocation > 0) {

          char *cmd2 = (char *)calloc(1024, 1);
          char *cur = cmd;
          char *cur2 = cmd2;
          while (*cur != '\0') {
            *cur2 = *cur;
            cur++;
            cur2++;
          }
          cursor--;
          if (*cursor == 34 && !overallQuote) {
            overallQuote = 1;
          } else if (*cursor == 34 && overallQuote) {
            overallQuote = 0;
          }
          if (*cursor == 34 && !isQuote) {
            isQuote = 1;
          } else if (*cursor == 34 && isQuote) {
            isQuote = 0;
          }
          *cursor = '\t';
          cur = cmd;
          cur2 = cmd2;
          while (*cur != '\t') {
            cur2++;
            cur++;
          }
          cur2++;
          while (*cur != '\0') {
            *cur = *cur2;
            cur++;
            cur2++;
          }
          free(cmd2);
          cmd2 = (char *)calloc(1024, 1);
          cur = cmd;
          cur2 = cmd2;
          while (*cur != '\0') {
            if (*cur == 10) {
              cur2 = cmd2;
              cur++;
            } else {
              *cur2 = *cur;
              cur++;
              cur2++;
            }
          }
          *cur2 = '\0';
          for (int i = lineLocation; i > 1; i--) {
            write(1, "\b \b", 3);
          }
          write(1, cmd2, strlen(cmd2));
          for (int i = lineLength; i > -1; i--) {
            write(1, "\b \b", 3);
          }
          write(1, " ", 1);
          lineLocation--;
          lineLength--;
          write(1, cmd2, strlen(cmd2));
          for (int i = 0; i < left; ++i) {
            write(1, "\b", 1);
          }
          free(cmd2);
          count--;
          max_length--;
        }
        cursor--;
      } else if (k != 2 && k != 1 && last_char != '\n' && last_char != '\t' &&
                 last_char != -1 && last_char != 1 && last_char != 11) {
        char *cmd2 = (char *)calloc(1024, 1);
        char *cur = cmd;
        char *cur2 = cmd2;
        for (int i = 0; i < count; ++i) {
          *cur2 = *cur;
          cur++;
          cur2++;
        }
        *cur2 = last_char;
        cur2++;
        while (*cur != '\0') {
          *cur2 = *cur;
          cur++;
          cur2++;
        }
        *cur2 = '\0';
        strcpy(cmd, cmd2);
        cur = cmd;
        cur2 = cmd2;
        while (*cur != '\0') {
          if (*cur == 10) {
            cur2 = cmd2;
            cur++;
          } else {
            *cur2 = *cur;
            cur++;
            cur2++;
          }
        }
        *cur2 = '\0';
        max_length++;
        count++;
        for (int i = lineLength + 1 - left; i > 0; i--) {
          write(1, "\b \b", 3);
        }
        lseek(1, -1, SEEK_CUR);
        write(1, " \b ", 3);
        write(1, " \b\b ", 4);
        write(1, cmd2, strlen(cmd2));
        lineLocation++;
        lineLength++;
        for (int i = 0; i < left; ++i) {
          write(1, "\b", 1);
        }
        free(cmd2);
      } else if (last_char == '\t') {
        cursor--;
      }
    }
    *cursor = '\0';

    if (!rv) {
      finished = 1;
      break;
    }
    for (int i = 0; i < bgPidsCounter; ++i) {
      for (int j = 0; j < 1024; ++j) {
        if (jobsCounter[j] == 0) {
          if (bgPids[i] == backgroundJobs[j].pid) {
            if (infop->si_status == 0) {
              printf("[%d]+ Done %s\n", j + 1, backgroundJobs[j].name[0]);
              jobsCounter[j] = 0;
            } else {
              printf("[%d]- Exit %d %s\n", j + 1, infop->si_status,
                     backgroundJobs[j].name[0]);
              jobsCounter[j] = 0;
            }
            break;
          }
        }
      }
    }
    bgPidsCounter = 0;
    cursor = cmd;
    isQuote = 0;
    addToHistory(cmd);
    char *cmd2 = (char *)calloc(1024, 1);
    strcpy(cmd2, cmd);
    char *cur2 = cmd2;
    int quoteLength = 0;
    char *cmd3 = (char *)calloc(1024, 1);
    char *cur3 = cmd3;
    for (int i = 0; i < strlen(cmd2); i++) {
      if (isQuote && *cur2 == ' ') {
        *cur3 = '\t';
      } else if (*cur2 == 34 && isQuote) {
        if (strlen(cmd2) - 1 == i) {
          *(cur3) = '\n';
          *(cur3 + 1) = '\0';
          break;
        }
        *(cur3) = *(cur2 + 1);
        i++;
        isQuote = 0;
        cur2++;
        quoteLength = 0;
      } else if (*cur2 == 34 && !isQuote) {
        isQuote = 1;
        *cur3 = *(cur2 + 1);
        i++;
        cur2++;
      } else {
        *cur3 = *cur2;
      }
      if (isQuote) {
        quoteLength++;
      }
      cur3++;
      cur2++;
    }
    cur3++;
    *cur3 = '\0';
    strcpy(cmd, cmd3);
    char *token_main = strtok(cmd, " ");
    int tokenSize = 0;
    free(cmd2);
    free(cmd3);
    char *token_spare_main = token_main;
    errno = 0;
    while (token_main != NULL) {
      tokenSize = strlen(token_main);
      if (token_main[0] == '$')
        variableUse = true;
      allTokensGlobal[allTokensCounterGlobal] = token_main;
      if (strstr(token_main, ">") != NULL || strstr(token_main, "<") != NULL)
        redirectionReq = true;

      if (strstr(token_main, "|") != NULL)
        pipeReq = true;
      token_main[tokenSize] = '\0';
      allTokensCounterGlobal++;
      token_spare_main = token_main;
      token_main = strtok(NULL, " ");
    }
    allTokensGlobal[allTokensCounterGlobal] = NULL;

    if (allTokensCounterGlobal > 0) {
      token_spare_main[tokenSize - 1] = '\0';
      allTokensGlobal[allTokensCounterGlobal] = token_main;
    }
    allTokensGlobal[allTokensCounterGlobal] = NULL;
    if (debug_global == 0) {
      fprintf(stderr, "Running Command: %s\n", allTokensGlobal[0]);
    }
    // tokenize all paths
    char *path = getenv("PATH");
    char *token_path_main = strtok(path, ":");
    while (token_path_main != NULL) {
      tokenSize = strlen(token_path_main);
      token_path_main[tokenSize] = '\0';
      allPathTokensGlobal[allPathTokensCounterGlobal] = token_path_main;
      allPathTokensCounterGlobal++;
      token_path_main = strtok(NULL, ":");
    }
    if (allTokensGlobal[0] != NULL) {
      if (strcmp(allTokensGlobal[0], "echo") == 0)
        methodReturnVal = runEcho(argv, envp);
    }

    allPathTokensGlobal[allPathTokensCounterGlobal] = NULL;
    if (redirectionReq == 1) {
      sampleRedirect(argv, envp);
      redirectionReq = 0;
      redirectionCounter = 0;
      redirectionCounter++;
      while (redirectionCounter < totalRedirectionArgs) {
        sampleRedirect(argv, envp);
        redirectionReq = 0;
        redirectionCounter = 0;
        redirectionCounter++;
      }
    } else if (pipeReq == 1) {
      sampleRedirect(argv, envp);
      pipeReq = 0;
    } else {

      if (allTokensGlobal[0] != NULL) {
        isBinary = runBinaries(argv, envp);
        if (isBinary < 0) {
          isBinaryCommand = binaryCheck(argv, envp);
          if (isBinaryCommand < 0) {
            int compareVal = strcmp(allTokensGlobal[0], "cd");
            if (compareVal == 0) {
              methodReturnVal = runCD(envp);
              if (debug_global == 0) {
                fprintf(stderr, "CD Returned: %d \n", methodReturnVal);
              }
            } else {
              compareVal = strcmp(allTokensGlobal[0], "help");
              if (compareVal == 0) {
                methodReturnVal = runHelp();
                if (debug_global == 0)
                  fprintf(stderr, "Help Returned: %d \n", methodReturnVal);
              } else {
                compareVal = strcmp(allTokensGlobal[0], "set");
                if (compareVal == 0) {
                  if (allTokensCounterGlobal == 4) {
                    compareVal = strcmp(allTokensGlobal[2], "=");
                    if (compareVal == 0) {
                      methodReturnVal = runSet(envp);
                      if (debug_global == 0)
                        fprintf(stderr, "Set Returned %d \n", methodReturnVal);
                    } else {
                      printf("Usage: set [VAR_NAME] = [VALUE]\n");
                    }
                  } else {
                    printf("Usage: set [VAR_NAME] = [VALUE]\n");
                  }
                } else {
                  compareVal = strcmp(allTokensGlobal[0], "exit");
                  if (compareVal == 0) {
                    if (debug_global == 0)
                      fprintf(stderr, "Exit Returned: 0\n");
                    exit(0);
                  } else {
                    compareVal = strcmp(allTokensGlobal[0], "pwd");
                    if (compareVal == 0) {
                      printf("%s\n", runPWD());
                    } else {
                      compareVal = strcmp(allTokensGlobal[0], "echo");
                      if (compareVal == 0) {
                        if (debug_global == 0)
                          fprintf(stderr, "Echo Returned: %d \n",
                                  methodReturnVal);
                      } else {
                        compareVal = strcmp(allTokensGlobal[0], "fg");
                        if (compareVal == 0) {
                          methodReturnVal = runFg();
                          if (debug_global == 0)
                            fprintf(stderr, "Fg Returned: %d \n",
                                    methodReturnVal);
                        } else {
                          compareVal = strcmp(allTokensGlobal[0], "bg");
                          if (compareVal == 0) {
                            methodReturnVal = runBg();
                            if (debug_global == 0)
                              fprintf(stderr, "Bg Returned: %d \n",
                                      methodReturnVal);
                          } else {
                            compareVal = strcmp(allTokensGlobal[0], "jobs");
                            if (compareVal == 0) {
                              methodReturnVal = runJobs();
                              if (debug_global == 0)
                                fprintf(stderr, "Jobs Returned: %d \n",
                                        methodReturnVal);
                            } else {
                              if ((strcmp(allTokensGlobal[0], "history") == 0))
                                runHistory();
                              else if ((strcmp(allTokensGlobal[0],
                                               "clear-history") == 0))
                                clearHistory();
                              else {
                                int j = 0;
                                while (allTokensGlobal[j] != NULL) {
                                  printf("%s", allTokensGlobal[j]);
                                  j++;
                                }
                                printf(": command not found\n");
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          if (debug_global == 0)
            fprintf(stderr, "%s returned %d \n", allTokensGlobal[0],
                    isBinaryCommand);
        }
      }
    }
    int i = 0;
    for (i = 0; i < allTokensCounterGlobal; i++) {
      allTokensGlobal[i] = NULL;
    }
    allTokensCounterGlobal = 0;
    free(cmd);
  }
  free(oldpwd);
  free(pwd);
  free(cwd2);
  free(set1);
  free(set2);
  for (i = 0; i < 20; i++) {
    free(history[i]);
  }
  return 0;
}

int runFg() {
  if (allTokensGlobal[1] == NULL) {
    printf("Usage: fg: fg [JOBID | %cPID]\n", '%');
    return -1;
  }
  int percent = strlen(allTokensGlobal[1]);
  percent = -1;
  int num = 0;
  char *str = allTokensGlobal[1];
  for (int i = 0; i < strlen(allTokensGlobal[1]); i++) {
    if (*str != '%' && percent == -1) {
      percent = 1;
      i--;
    } else if (*str == '%' && percent == -1) {
      percent = 0;
      str++;
    } else if ((*str >= '0') && (*str <= '9')) {
      num = (num * 10) + ((*str) - '0');
      str++;
    } else {
      printf("%s no such job.\n", allTokensGlobal[1]);
      return -1;
    }
  }
  int jobid = 0;
  if (num == 0) {
    printf("%d no such job.\n", num);
    return -1;
  }
  if (percent) {
    for (int i = 0; i < MAX_INPUT - 1; ++i) {
      if (backgroundJobs[i].pid == num) {
        jobid = i + 1;
      }
    }
    if (jobid == 0) {
      printf("%d no such job.\n", num);
      return -1;
    }
  } else if (!percent) {
    jobid = num;
    if (MAX_INPUT >= jobid && jobsCounter[jobid - 1] != 1) {
      printf("%d no such job.\n", jobid);
      return -1;
    }
  }
  if (jobid <= MAX_INPUT) {

    foregroundJob.num = jobid;
    foregroundJob.isRunning = 1;
    foregroundJob.pid = backgroundJobs[foregroundJob.num - 1].pid;
  } else {
    printf("%d no such job.\n", jobid);
    return -1;
  }

  kill(foregroundJob.pid, SIGCONT);
  struct termios t;
  tcgetattr(0, &t);
  t.c_lflag &= ~(ECHO | ICANON);
  tcsetattr(0, TCSANOW, &t);
  t.c_cc[VTIME] = 1;
  t.c_cc[VMIN] = 0;
  tcsetattr(0, TCSANOW, &t);
  sleep(1);
  pid = 0;
  while (!pid) {
    char ch = 0;
    read(1, &ch, 1);
    char last_char = ch;
    if (last_char == 3) {
      kill(foregroundJob.pid, SIGKILL);
      sleep(1);
      pid = 1;
      printf("Job [%d] (%d) killed by signal %d\n", foregroundJob.num,
             foregroundJob.pid, SIGKILL);
      jobsCounter[foregroundJob.num - 1] = 0;
    } else if (last_char == 26) {
      backgroundJobs[foregroundJob.num - 1].num = foregroundJob.num;
      backgroundJobs[foregroundJob.num - 1].isRunning = 0;
      backgroundJobs[foregroundJob.num - 1].pid = foregroundJob.pid;
      kill(foregroundJob.pid, SIGTSTP);
      sleep(1);
      printf("Job [%d] (%d) stopped by signal %d\n", foregroundJob.num,
             foregroundJob.pid, SIGTSTP);
      pid = 1;
    }
  }
  t.c_cc[VTIME] = TIME;
  t.c_cc[VMIN] = MIN;
  errno = 0;
  return 0;
}

int runBg() {
  if (allTokensGlobal[1] == NULL) {
    printf("Usage: bg: bg [JOBID | %cPID]\n", '%');
    return -1;
  }
  int percent = strlen(allTokensGlobal[1]);
  percent = -1;
  int num = 0;
  char *str = allTokensGlobal[1];
  for (int i = 0; i < strlen(allTokensGlobal[1]); i++) {
    if (*str != '%' && percent == -1) {
      percent = 1;
      i--;
    } else if (*str == '%' && percent == -1) {
      percent = 0;
      str++;
    } else if ((*str >= '0') && (*str <= '9')) {
      num = (num * 10) + ((*str) - '0');
      str++;
    } else {
      printf("%s no such job.\n", allTokensGlobal[1]);
      return -1;
    }
  }
  int jobid = 0;
  if (num == 0) {
    printf("%d no such job.\n", num);
    return -1;
  }
  if (percent) {
    for (int i = 0; i < MAX_INPUT - 1; ++i) {
      if (backgroundJobs[i].pid == num) {
        jobid = i + 1;
      }
    }
    if (jobid == 0) {
      printf("%d no such job.\n", num);
      return -1;
    }
  } else if (!percent) {
    jobid = num;
    if (MAX_INPUT >= jobid && jobsCounter[jobid - 1] != 1) {
      printf("%d no such job.\n", jobid);
      return -1;
    }
  }
  if (jobid > MAX_INPUT) {
    printf("%d no such job.\n", jobid);
    return -1;
  }
  if (backgroundJobs[jobid - 1].isRunning == 1) {
    printf("job %d already in background\n", jobid);
    return -1;
  }

  kill(backgroundJobs[jobid - 1].pid, SIGCONT);
  sleep(1);
  backgroundJobs[jobid - 1].isRunning = 1;
  errno = 0;
  return 0;
}

int runJobs() {
  for (int i = 0; i < 1024; ++i) {
    if (jobsCounter[i] == 1) {
      if (backgroundJobs[i].isRunning) {
        printf("[%d] (%d) Running %s\n", i + 1, backgroundJobs[i].pid,
               backgroundJobs[i].name[0]);
      } else {
        printf("[%d] (%d) Stopped %s\n", i + 1, backgroundJobs[i].pid,
               backgroundJobs[i].name[0]);
      }
    }
  }
  errno = 0;
  return 0;
}

int runBinaries(char **argv, char **envp) {
  pid_t childPid = 0;
  if (allTokensGlobal[0][0] == '.') {
    if (allTokensGlobal[0][1] == '/') {
      int isBackground = 0;
      for (int i = 0; i < allTokensCounterGlobal; ++i) {
        if (allTokensGlobal[i][0] == '&' && allTokensGlobal[i][1] == '\0') {
          isBackground = 1;
          allTokensCounterGlobal--;
        }
        if (isBackground && allTokensGlobal[i + 1] != NULL) {
          strcpy(allTokensGlobal[i], allTokensGlobal[i + 1]);
        } else if (allTokensGlobal[allTokensCounterGlobal + 1] == NULL) {
          allTokensGlobal[allTokensCounterGlobal] = NULL;
        }
      }
      struct stat fileStat;
      int sizeOfPath = 1024;
      char dest[sizeOfPath];
      strcpy(dest, runPWD());
      strcat(dest, "/");
      char binary[1024];
      int binaryCounter = 0;
      int i = 2;
      while (allTokensGlobal[0][i] != '\0') {
        binary[binaryCounter] = allTokensGlobal[0][i];
        i++;
        binaryCounter++;
      }
      binary[binaryCounter] = '\0';
      strcat(dest, binary);
      int returnVal = stat(dest, &fileStat);
      if (returnVal >= 0) {
        pid = 0;
        childPid = fork();
        errno = 0;
        if (childPid == 0) {
          errno = 0;
          execvp(dest, allTokensGlobal);
          if (errno == 13) {
            printf("%s: Permission denied or is a directory\n",
                   allTokensGlobal[0]);
          }
          exit(0);
        } else {
          foregroundJob.pid = childPid;
          for (int i = 1; i < MAX_INPUT + 1; ++i) {
            if (jobsCounter[i - 1] == 0) {
              if (!isBackground) {
                foregroundJob.num = i;
                backgroundJobs[i - 1].isRunning = 1;
              } else {
                foregroundJob.num = i;
                backgroundJobs[i - 1].num = i;
                backgroundJobs[i - 1].isRunning = 1;
                backgroundJobs[i - 1].pid = foregroundJob.pid;
                printf("[%d] (%d)\n", i, foregroundJob.pid);
              }
              for (int j = 0; j < allTokensCounterGlobal; j++) {
                foregroundJob.name[j] = (char *)calloc(1024, 1);
                backgroundJobs[i - 1].name[j] = (char *)calloc(1024, 1);
                strcpy(foregroundJob.name[j], allTokensGlobal[j]);
                strcpy(backgroundJobs[i - 1].name[j], allTokensGlobal[j]);
              }
              jobsCounter[i - 1] = 1;
              break;
            }
          }
          foregroundJob.isRunning = 1;
          struct termios t;
          tcgetattr(0, &t);
          t.c_lflag &= ~(ECHO | ICANON);
          tcsetattr(0, TCSANOW, &t);
          t.c_cc[VTIME] = 1;
          t.c_cc[VMIN] = 0;
          tcsetattr(0, TCSANOW, &t);
          while (!pid && !isBackground) {
            char ch = 0;
            read(1, &ch, 1);
            char last_char = ch;
            if (last_char == 3) {
              kill(foregroundJob.pid, SIGKILL);
              sleep(1);
              pid = 1;
              printf("Job [%d] (%d) killed by signal %d\n", foregroundJob.num,
                     foregroundJob.pid, SIGKILL);
              jobsCounter[foregroundJob.num - 1] = 0;
              break;
            } else if (last_char == 26) {
              backgroundJobs[foregroundJob.num - 1].num = foregroundJob.num;
              backgroundJobs[foregroundJob.num - 1].isRunning = 0;
              backgroundJobs[foregroundJob.num - 1].pid = childPid;
              kill(foregroundJob.pid, SIGTSTP);
              sleep(1);
              printf("Job [%d] (%d) stopped by signal %d\n", foregroundJob.num,
                     foregroundJob.pid, SIGTSTP);
              pid = 1;
              break;
            }
          }
          t.c_cc[VTIME] = TIME;
          t.c_cc[VMIN] = MIN;
        }
        return 0;
      } else {
        printf("%s: No such file or directory\n", allTokensGlobal[0]);
        return -1;
      }
    }
  }
  return -1;
}

int binaryCheck(char **argv, char **envp) {

  pid_t childPid_binaryCheck = 0;
  int i_binaryCheck = 0;
  struct stat fileStat_binaryCheck;
  int isBackground = 0;
  for (int i = 0; i < allTokensCounterGlobal; ++i) {
    if (allTokensGlobal[i][0] == '&' && allTokensGlobal[i][1] == '\0') {
      isBackground = 1;
      allTokensCounterGlobal--;
    }
    if (isBackground && allTokensGlobal[i + 1] != NULL) {
      strcpy(allTokensGlobal[i], allTokensGlobal[i + 1]);
    } else if (allTokensGlobal[allTokensCounterGlobal + 1] == NULL) {
      allTokensGlobal[allTokensCounterGlobal] = NULL;
    }
  }
  for (i_binaryCheck = 0; i_binaryCheck < allPathTokensCounterGlobal;
       i_binaryCheck++) {

    int sizeOfPath_binaryCheck = strlen(allPathTokensGlobal[i_binaryCheck]);
    sizeOfPath_binaryCheck += 3;
    char dest_binaryCheck[sizeOfPath_binaryCheck + 1];
    strcpy(dest_binaryCheck, allPathTokensGlobal[i_binaryCheck]);
    strcat(dest_binaryCheck, "/");
    strcat(dest_binaryCheck, allTokensGlobal[0]);
    int compareVal = 0;
    compareVal = strcmp(allTokensGlobal[0], "echo");
    if (compareVal == 0) {
      return -1;
    }
    compareVal = strcmp(allTokensGlobal[0], "pwd");
    if (compareVal == 0) {
      return -1;
    }
    int returnVal_binaryCheck = stat(dest_binaryCheck, &fileStat_binaryCheck);

    if (returnVal_binaryCheck >= 0) {
      pid = 0;
      errno = 0;

      childPid_binaryCheck = fork();
      if (childPid_binaryCheck == 0) {
        execve(dest_binaryCheck, allTokensGlobal, envp);
        exit(24);
      } else {
        foregroundJob.pid = childPid_binaryCheck;
        for (int i = 1; i < MAX_INPUT + 1; ++i) {
          if (jobsCounter[i - 1] != 1) {
            if (!isBackground) {
              foregroundJob.num = i;
              backgroundJobs[i - 1].isRunning = 1;
            } else {
              backgroundJobs[i - 1].num = i;
              backgroundJobs[i - 1].isRunning = 1;
              backgroundJobs[i - 1].pid = childPid_binaryCheck;
            }
            for (int j = 0; j < allTokensCounterGlobal; j++) {
              foregroundJob.name[j] = (char *)calloc(1024, 1);
              backgroundJobs[i - 1].name[j] = (char *)calloc(1024, 1);
              strcpy(foregroundJob.name[j], allTokensGlobal[j]);
              strcpy(backgroundJobs[i - 1].name[j], allTokensGlobal[j]);
            }
            jobsCounter[i - 1] = 1;
          }
          break;
        }
        foregroundJob.isRunning = 1;
        struct termios t;
        tcgetattr(0, &t);
        t.c_lflag &= ~(ECHO | ICANON);
        tcsetattr(0, TCSANOW, &t);
        t.c_cc[VTIME] = 1;
        t.c_cc[VMIN] = 0;
        tcsetattr(0, TCSANOW, &t);
        while (!pid && !isBackground) {
          char ch = 0;
          read(1, &ch, 1);
          char last_char = ch;
          if (last_char == 3) {
            kill(foregroundJob.pid, SIGKILL);
            sleep(1);
            pid = 1;
            printf("Job [%d] (%d) killed by signal %d\n", foregroundJob.num,
                   foregroundJob.pid, SIGKILL);
            jobsCounter[foregroundJob.num - 1] = 0;
          } else if (last_char == 26) {
            backgroundJobs[foregroundJob.num - 1].num = foregroundJob.num;
            backgroundJobs[foregroundJob.num - 1].isRunning = 0;
            backgroundJobs[foregroundJob.num - 1].pid = foregroundJob.pid;
            kill(foregroundJob.pid, SIGTSTP);
            sleep(1);
            printf("Job [%d] (%d) stopped by signal %d\n", foregroundJob.num,
                   foregroundJob.pid, SIGTSTP);
            pid = 1;
          }
        }
        t.c_cc[VTIME] = TIME;
        t.c_cc[VMIN] = MIN;
      }
      break;
    }
    if (returnVal_binaryCheck < 0 &&
        i_binaryCheck == allPathTokensCounterGlobal - 1) {
      return -1;
    }
  }
  return 0;
}

int runHelp() {
  printf("Name\t320Sh\n");
  printf("Synopsis\t320Sh binaryName\n");
  printf("CopyRight:\t Made for CSE320 Spring 2016\n");
  printf("Description:\t 320Sh is a standard interpreter used to run commands "
         "via the command line or through a file\n");
  printf("Available BuiltIns: \n");
  printf("\tcd\tUsage: cd [Indirect Path | Direct Path]\n");
  printf("\tpwd\tUsage: pwd\n");
  printf("\thelp\tUsage: help\n");
  printf("\techo\tUsage: echo [$VAR_NAME | S]\n");
  printf("\tset\tUsage: set [VAR_NAME] = [VALUE]\n");
  printf("\tfg\tUsage: fg: fg [JOBID | %cPID]\n", '%');
  printf("\tbg\tUsage: bg: bg [JOBID | %cPID]\n", '%');
  printf("\tjobs\tUsgae: jobs\n");
  errno = 0;
  return 0;
}

char *runPWD() {
  char buff[1024];
  errno = 0;
  return getcwd(buff, 1024);
}

int runEcho(char **argv, char **envp) {
  int i = 1;
  for (i = 1; i < allTokensCounterGlobal; i++) {
    if (allTokensGlobal[i][0] == '$') {
      // check if they are looking for method return val
      if (strcmp(allTokensGlobal[i], "$?") == 0) {
        if (isBinaryCommand != 0 && isBinary != 0) {
          printf("%d", methodReturnVal);
        } else {
          printf("%d", exitStatus);
        }
      } else {
        char *printVal = replaceVariableWithValue(envp);
        printf("%s", printVal);
        free(printValue);
      }
    } else
      printf("%s", allTokensGlobal[i]);
  }
  printf("\n");
  return 0;
}

int runSet(char **envp) {
  strcpy(set1, allTokensGlobal[1]);
  strcpy(set2, allTokensGlobal[3]);
  int returnVal = setenv(set1, set2, 1);
  int i = 0;
  while (envp[i] != NULL) {
    if (strstr(envp[i], set1) != NULL) {
      if (returnVal == -1) {
        printf("Error");
        return -1;
      } else {
        return 0;
      }
    }
    i++;
  }
  if (envp[i] == NULL) {
    envp[i] = (char *)malloc(1024);
    strcpy(envp[i], set1);
    strcat(envp[i], "=");
    strcat(envp[i], set2);
    envp[++i] = NULL;
  }
  errno = 0;
  return 0;
}

int runCD(char **envp) {
  errno = 0;
  cwd2 = runPWD();
  if (allTokensGlobal[1] != NULL) {
    chdir(allTokensGlobal[1]);
  }
  if (errno == 0 && allTokensGlobal[1] != NULL) {
    int i = 0;
    char *q = (char *)malloc(1024);
    strcpy(q, "OLDPWD=");
    strcat(q, oldpwd);
    while (envp[i] != NULL) {
      int compareVal = strcmp(envp[i], q);
      if (compareVal == 0) {
        char q2[1024];
        strcpy(q2, "OLDPWD=");
        strcat(q2, cwd2);
        strcpy(oldpwd, cwd2);
        strcpy(envp[i], q2);
        envp[++i] = NULL;
        i--;
        break;
      }
      i++;
    }
    free(q);
    if (envp[i] == NULL) {
      envp[i] = (char *)malloc(1024);
      strcpy(envp[i], "OLDPWD=");
      strcat(envp[i], cwd2);
      strcpy(oldpwd, cwd2);
      envp[++i] = NULL;
    }
    pwd = runPWD();
    setenv("PWD", pwd, 1);
    pwd = getenv("PWD");
    return 0;
  } else if (allTokensGlobal[1] == NULL) {
    char *home = getenv("HOME");
    if (home == NULL) {
      printf("HOME not set\n");
      return 0;
    }
    errno = 0;
    chdir(home);
    if (errno == 0) {
      int i = 0;
      char *q = (char *)malloc(1024);
      strcpy(q, "OLDPWD=");
      if (oldpwd != NULL) {
        strcat(q, oldpwd);
      }

      while (envp[i] != NULL) {
        int compareVal = strcmp(envp[i], q);
        if (compareVal == 0) {
          char q2[1024];
          strcpy(q2, "OLDPWD=");
          strcat(q2, cwd2);
          strcpy(oldpwd, cwd2);
          strcpy(envp[i], q2);
          envp[++i] = NULL;
          i--;
          break;
        }
        i++;
      }
      free(q);

      if (envp[i] == NULL) {
        oldpwd = (char *)realloc(oldpwd, 1024);
        envp[i] = (char *)malloc(1024);
        strcpy(envp[i], "OLDPWD=");
        strcpy(oldpwd, cwd2);
        strcat(envp[i], cwd2);

        envp[++i] = NULL;
      }

      pwd = runPWD();

      setenv("PWD", pwd, 1);
      pwd = getenv("PWD");

    } else if (errno == EACCES) {
      printf("Permission denied for one of the components of the path.\n");
    } else if (errno == EIO) {
      printf("An I/0 error occured.\n");
    } else if (errno == ELOOP) {
      printf("Too many symbolic links encountered in resolving the path.\n");
    } else if (errno == ENAMETOOLONG) {
      printf("The path name is too long.\n");
    } else if (errno == ENOENT) {
      printf("The file does not exist.\n");
    } else if (errno == ENOMEM) {
      printf("Insufficient kernel memory was available.\n");
    } else if (errno == ENOTDIR) {
      printf("A component of the path is not a directory.\n");
    }
    return 0;
  } else {
    int compareVal = strcmp(allTokensGlobal[1], "-");
    if (compareVal == 0) {
      errno = 0;
      oldpwd = getenv("OLDPWD");
      if (oldpwd == NULL) {
        printf("OLDPWD not set\n");
        return 0;
      }
      chdir(oldpwd);
      if (errno == 0) {
        int i = 0;
        char *q = (char *)malloc(1024);
        strcpy(q, "OLDPWD=");
        strcat(q, oldpwd);
        while (envp[i] != NULL) {
          int compareVal = strcmp(envp[i], q);
          if (compareVal == 0) {
            char q2[1024];
            strcpy(q2, "OLDPWD=");
            strcat(q2, cwd2);
            strcpy(oldpwd, cwd2);
            strcpy(envp[i], q2);
            envp[++i] = NULL;
            i--;
            break;
          }
          i++;
        }
        free(q);
        if (envp[i] == NULL) {
          envp[i] = (char *)malloc(1024);
          strcpy(envp[i], "OLDPWD=");
          strcat(envp[i], cwd2);
          strcpy(oldpwd, cwd2);
          envp[++i] = NULL;
        }
        pwd = runPWD();
        setenv("PWD", pwd, 1);
        pwd = getenv("PWD");
      } else if (errno == EACCES) {
        printf("Permission denied for one of the components of the path.\n");
      } else if (errno == EIO) {
        printf("An I/0 error occured.\n");
      } else if (errno == ELOOP) {
        printf("Too many symbolic links encountered in resolving the path.\n");
      } else if (errno == ENAMETOOLONG) {
        printf("The path name is too long.\n");
      } else if (errno == ENOENT) {
        printf("The file does not exist.\n");
      } else if (errno == ENOMEM) {
        printf("Insufficient kernel memory was available.\n");
      } else if (errno == ENOTDIR) {
        printf("A component of the path is not a directory.\n");
      }
      return 0;
    }
  }
  if (errno == EACCES) {
    printf("Permission denied for one of the components of the path.\n");
  } else if (errno == ENOENT && allTokensGlobal[1] != NULL) {
    int sizeOfPath = 1024;
    char dest[sizeOfPath];
    strcpy(dest, runPWD());
    strcat(dest, "/");
    strcat(dest, allTokensGlobal[1]);
    errno = 0;
    chdir(dest);
    if (errno == 0) {
      int i = 0;
      char *q = (char *)malloc(1024);
      strcpy(q, "OLDPWD=");
      strcat(q, oldpwd);
      while (envp[i] != NULL) {
        int compareVal = strcmp(envp[i], q);
        if (compareVal == 0) {
          char q2[1024];
          strcpy(q2, "OLDPWD=");
          strcat(q2, cwd2);
          strcpy(oldpwd, cwd2);
          strcpy(envp[i], q2);
          envp[++i] = NULL;
          i--;
          break;
        }
        i++;
      }
      free(q);
      if (envp[i] == NULL) {
        envp[i] = (char *)malloc(1024);
        strcpy(envp[i], "OLDPWD=");
        strcat(envp[i], cwd2);
        strcpy(oldpwd, cwd2);
        envp[++i] = NULL;
      }
      pwd = runPWD();
      setenv("PWD", pwd, 1);
      pwd = getenv("PWD");
    } else if (errno == EACCES) {
      printf("Permission denied for one of the components of the path.\n");
    } else if (errno == EIO) {
      printf("An I/0 error occured.\n");
    } else if (errno == ELOOP) {
      printf("Too many symbolic links encountered in resolving the path.\n");
    } else if (errno == ENAMETOOLONG) {
      printf("The path name is too long.\n");
    } else if (errno == ENOENT) {
      printf("The file does not exist.\n");
    } else if (errno == ENOMEM) {
      printf("Insufficient kernel memory was available.\n");
    } else if (errno == ENOTDIR) {
      printf("A component of the path is not a directory.\n");
    }
  } else if (errno == EIO) {
    printf("An I/0 error occured.\n");
  } else if (errno == ELOOP) {
    printf("Too many symbolic links encountered in resolving the path.\n");
  } else if (errno == ENAMETOOLONG) {
    printf("The path name is too long.\n");
  } else if (errno == ENOENT) {
    printf("The file does not exist.\n");
  } else if (errno == ENOMEM) {
    printf("Insufficient kernel memory was available.\n");
  } else if (errno == ENOTDIR) {
    printf("A component of the path is not a directory.\n");
  }
  return 0;
}

char *replaceVariableWithValue(char **envp) {
  printValue = (char *)malloc(1024);
  int i = 0;
  char variableName[MAX_INPUT];
  char variableName2[MAX_INPUT];
  int variableNameSize = 0;
  int variableName2Size = 0;
  for (i = 0; i < allTokensCounterGlobal; i++) {
    if (allTokensGlobal[i][0] == '$')
      strcpy(variableName, allTokensGlobal[i]);
  }
  variableNameSize = strlen(variableName);
  for (i = 1; i < variableNameSize; i++) {
    variableName2[i - 1] = variableName[i];
  }
  variableName2[i - 1] = '\0';
  variableName2Size = strlen(variableName2);
  i = 0;
  while (envp[i] != NULL) {
    char *returnVal = strstr(envp[i], variableName2);
    if (returnVal != NULL) {
      char *startOfString = strstr(envp[i], variableName2);
      startOfString += variableName2Size;
      strcpy(printValue, startOfString);
      break;
    }
    i++;
  }
  return printValue;
}

int addToHistory(char *command) {
  if (totalCommandsSaved == 0) {
    int historyTotalFD = open("historyFileTotal.txt", (O_RDONLY | O_CREAT),
                              (S_IRUSR | S_IWUSR | S_IXUSR));
    char totalCommandsStr[5];
    int returnVal = read(historyTotalFD, totalCommandsStr, sizeof(int));
    close(historyTotalFD);
    int i = 0;
    int total = 0;
    int mult = 1;
    for (i = 0; i < (returnVal - 1); i++) {
      mult *= 10;
    }

    for (i = 0; i < (returnVal); i++) {
      total += (totalCommandsStr[i] - 48) * mult;
      mult /= 10;
    }
    totalCommandsSaved = total;
  }
  totalCommandsSaved++;
  char command2[MAX_INPUT];
  int i = 0;
  int j = 0;
  bool addSpaces = false;
  for (i = 0; i < MAX_INPUT - 1; i++) {
    if (!addSpaces) {
      if (*(command + i) == '\0' || *(command + i) == '\n') {
        command2[j] = '~';
        j++;
        addSpaces = true;
      }
      command2[j] = *(command + i);
      j++;

    } else {
      command2[j] = '`';
      j++;
    }
  }
  command2[MAX_INPUT - 1] = '\0';

  FILE *fp;
  fp = fopen("historyFile.txt", "a");
  fputs(command2, fp);
  fclose(fp);

  FILE *fp2 = fopen("historyFileTotal.txt", "w+");
  fprintf(fp2, "%d", totalCommandsSaved);
  fclose(fp2);

  return 0;
}

int traverseHistory(bool upKeyPressed) {
  if (totalCommandsSaved == 0) {
    int historyTotalFD = open("historyFileTotal.txt", (O_RDONLY | O_CREAT),
                              (S_IRUSR | S_IWUSR | S_IXUSR));
    char totalCommandsStr[5];
    int returnVal = read(historyTotalFD, totalCommandsStr, sizeof(int));
    close(historyTotalFD);
    int i = 0;
    int total = 0;
    int mult = 1;
    for (i = 0; i < (returnVal - 1); i++) {
      mult *= 10;
    }

    for (i = 0; i < (returnVal); i++) {
      total += (totalCommandsStr[i] - 48) * mult;
      mult /= 10;
    }
    totalCommandsSaved = total;
  }
  int historyFileDescriptor;
  char str[MAX_INPUT];
  historyFileDescriptor = open("historyFile.txt", O_RDONLY);

  if (upKeyPressed) {
    if (totalCommandsSaved >= (historyTraverse + 1))
      historyTraverse++;
    else {
      printf("\033[%dD", sizeOfLastCommand);
      // save cursor position
      printf("\033[s");
      // clear to end of line
      printf("\033[K");
      // go back to saved position.
      printf("\033[u");
      char end[] = "End Of Shell History Resetting Traversal";
      printf("%s", end);
      sizeOfLastCommand = (sizeof(end) - 1);
      historyTraverse = 0;
      return 0;
    }
    // up presssed
    if (sizeOfLastCommand != 0) {
      // printf("HI :)");
      // move back __ lines
      printf("\033[%dD", sizeOfLastCommand);
      // save cursor position
      printf("\033[s");
      // clear to end of line
      printf("\033[K");
      // go back to saved position.
      printf("\033[u");
    }
    lseek(historyFileDescriptor, (-1024 * historyTraverse), SEEK_END);
    read(historyFileDescriptor, str, 1024);
    int i = 0;
    char newStr[MAX_INPUT];
    int j = 0;
    for (i = 0; i < MAX_INPUT; i++) {
      if (str[i] == '!')
        break;
      else {
        // printf("COPYING INTO NEW STR: %c \n", str[i]);
        newStr[j] = str[i];
        j++;
      }
    }
    newStr[j] = '\0';
    sizeOfLastCommand = j;
    //  cmd = newStr;
    printf("%s", newStr);

  } else {
    // down key pressed
    if (0 < (historyTraverse - 1))
      historyTraverse--;
    else {
      printf("\033[%dD", sizeOfLastCommand);
      // save cursor position
      printf("\033[s");
      // clear to end of line
      printf("\033[K");
      // go back to saved position.
      printf("\033[u");
      char end[] = "End Of Shell History Resetting Traversal";
      printf("%s", end);
      sizeOfLastCommand = (sizeof(end) - 1);
      historyTraverse = totalCommandsSaved;
      return 0;
    }

    if (sizeOfLastCommand != 0) {
      // printf("HI :)");
      // move back __ lines
      printf("\033[%dD", sizeOfLastCommand);
      // save cursor position
      printf("\033[s");
      // clear to end of line
      printf("\033[K");
      // go back to saved position.
      printf("\033[u");
    }
    lseek(historyFileDescriptor, (-1024 * historyTraverse), SEEK_END);
    read(historyFileDescriptor, str, 1024);
    int i = 0;
    char newStr[MAX_INPUT];
    int j = 0;
    for (i = 0; i < MAX_INPUT; i++) {
      if (str[i] == '!')
        break;
      else {
        // printf("COPYING INTO NEW STR: %c \n", str[i]);
        newStr[j] = str[i];
        j++;
      }
    }
    newStr[j] = '\0';
    sizeOfLastCommand = j;
    printf("%s", newStr);
    //  cmd = newStr;
  }

  return 0;
}

int binaryCheck2(char **argv, char **envp) {
  pid_t childPid_binaryCheck = 0;
  int isBackground = 0;

  for (int i = 0; i < allTokensCounterGlobal; ++i) {
    if (allTokensGlobal[i][0] == '&' && allTokensGlobal[i][1] == '\0') {
      isBackground = 1;
      allTokensCounterGlobal--;
    }
    if (isBackground && allTokensGlobal[i + 1] != NULL) {
      strcpy(allTokensGlobal[i], allTokensGlobal[i + 1]);
    } else if (allTokensGlobal[allTokensCounterGlobal + 1] == NULL) {
      allTokensGlobal[allTokensCounterGlobal] = NULL;
    }
  }
  int i_binaryCheck = 0;
  struct stat fileStat_binaryCheck;
  for (i_binaryCheck = 0; i_binaryCheck < allPathTokensCounterGlobal;
       i_binaryCheck++) {
    int sizeOfPath_binaryCheck = strlen(allPathTokensGlobal[i_binaryCheck]);
    sizeOfPath_binaryCheck += 3;
    char *dest_binaryCheck = (char *)calloc(sizeOfPath_binaryCheck + 1, 1);
    strcpy(dest_binaryCheck, allPathTokensGlobal[i_binaryCheck]);
    strcat(dest_binaryCheck, "/");
    strcat(dest_binaryCheck, allTokensGlobal[0]);
    int returnVal_binaryCheck = stat(dest_binaryCheck, &fileStat_binaryCheck);
    int returnVal = 0;
    if (returnVal_binaryCheck < 0) {
      struct stat fileStat;
      int sizeOfPath = 1024;
      free(dest_binaryCheck);
      dest_binaryCheck = (char *)calloc(sizeOfPath, 1);
      strcpy(dest_binaryCheck, runPWD());
      strcat(dest_binaryCheck, "/");
      char binary[1024];
      int binaryCounter = 0;
      int i = 2;
      while (allTokensGlobal[0][i] != '\0') {
        binary[binaryCounter] = allTokensGlobal[0][i];
        i++;
        binaryCounter++;
      }
      binary[binaryCounter] = '\0';
      strcat(dest_binaryCheck, binary);
      returnVal = stat(dest_binaryCheck, &fileStat);
    }
    if (returnVal_binaryCheck >= 0 || returnVal >= 0) {
      pid = 0;
      errno = 0;
      childPid_binaryCheck = fork();
      if (childPid_binaryCheck == 0) {
        char *newTokens[MAX_INPUT];
        int i = 0;
        int j = 0;
        for (i = 0; i < allTokensCounterGlobal; i++) {
          if (strstr(allTokensGlobal[i], ">") != NULL) {
            break;
          } else if (strstr(allTokensGlobal[i], "<") != NULL) {
            if (returnVal > 0) {
              break;
            } else {
              continue;
            }
          } else {
            newTokens[j] = allTokensGlobal[i];
            j++;
          }
        }
        execve(dest_binaryCheck, newTokens, envp);
        exit(0);
      } else {
        foregroundJob.pid = childPid_binaryCheck;
        for (int i = 1; i < MAX_INPUT + 1; ++i) {
          if (jobsCounter[i - 1] != 1) {
            if (!isBackground) {
              foregroundJob.num = i;
              backgroundJobs[i - 1].isRunning = 1;
            } else {
              backgroundJobs[i - 1].num = i;
              backgroundJobs[i - 1].isRunning = 1;
              backgroundJobs[i - 1].pid = foregroundJob.pid;
            }
            for (int j = 0; j < allTokensCounterGlobal; j++) {
              foregroundJob.name[j] = (char *)calloc(1024, 1);
              backgroundJobs[i - 1].name[j] = (char *)calloc(1024, 1);
              strcpy(foregroundJob.name[j], allTokensGlobal[j]);
              strcpy(backgroundJobs[i - 1].name[j], allTokensGlobal[j]);
            }
            jobsCounter[i - 1] = 1;
          }
          break;
        }
        foregroundJob.isRunning = 1;
        struct termios t;
        tcgetattr(0, &t);
        t.c_lflag &= ~(ECHO | ICANON);
        tcsetattr(0, TCSANOW, &t);
        t.c_cc[VTIME] = 1;
        t.c_cc[VMIN] = 0;
        tcsetattr(0, TCSANOW, &t);
        while (!pid && !isBackground) {
          char ch = 0;
          read(1, &ch, 1);
          char last_char = ch;
          if (last_char == 3) {
            kill(foregroundJob.pid, SIGKILL);
            sleep(1);
            pid = 1;
            printf("Job [%d] (%d) killed by signal %d\n", foregroundJob.num,
                   foregroundJob.pid, SIGKILL);
            jobsCounter[foregroundJob.num - 1] = 0;
          } else if (last_char == 26) {
            backgroundJobs[foregroundJob.num - 1].num = foregroundJob.num;
            backgroundJobs[foregroundJob.num - 1].isRunning = 0;
            backgroundJobs[foregroundJob.num - 1].pid = foregroundJob.pid;
            kill(foregroundJob.pid, SIGTSTP);
            sleep(1);
            printf("Job [%d] (%d) stopped by signal %d\n", foregroundJob.num,
                   foregroundJob.pid, SIGTSTP);
            pid = 1;
          }
        }
        free(dest_binaryCheck);
        t.c_cc[VTIME] = TIME;
        t.c_cc[VMIN] = MIN;
      }
      break;
    }
    if (returnVal_binaryCheck < 0 &&
        i_binaryCheck == allPathTokensCounterGlobal - 1) {
      return -1;
    }
  }
  return 0;
}

int binaryCheck3(char **argv, char **envp, int execArg) {
  int i_binaryCheck = 0;
  struct stat fileStat_binaryCheck;
  bool commandRequiresRedirection = false;
  if ((strstr(pipeArgs[execArg], ">") != NULL) ||
      (strstr(pipeArgs[execArg], "<") != NULL)) {
    commandRequiresRedirection = true;
  }
  if (commandRequiresRedirection) {
    // there is redirection inside this command
    int totalNumOfRedirections = 0;
    int redirectIn = 0;
    int redirectOut = 0;
    int i = 0;
    int sizeOfArg = sizeof(pipeArgs[execArg]);
    // char arg[MAX_INPUT];
    for (i = 0; i < sizeOfArg; i++) {
      if (pipeArgs[execArg][i] == '>') {
        totalNumOfRedirections++;
        redirectOut++;
      }

      else if (pipeArgs[execArg][i] == '<') {
        totalNumOfRedirections++;
        redirectIn++;
      }
    }
    bool allRedirectIn = false;
    bool allRedirectOut = false;
    // if both are false we have combination of both > and < in redirection
    // arg
    if (totalNumOfRedirections == redirectIn)
      allRedirectIn = true;

    else if (totalNumOfRedirections == redirectOut)
      allRedirectOut = true;

    if (allRedirectOut) {
      // command > file > file ....
      char commandToExec[50];
      char fileToExec[50];
      int i = 0;
      int j = 0;
      while (pipeArgs[execArg][i] != '>') {
        commandToExec[j] = pipeArgs[execArg][i];
        i++;
        j++;
      }
      int numberOfRedirects = 0;
      i = 0;
      j = 0;
      while (true) {
        i++;
        if (pipeArgs[execArg][i] == '>') {
          numberOfRedirects++;
          i++;
          continue;
        }
        if (numberOfRedirects == redirectOut) {
          // we hit the last '>', the following thing is the final file
          if (pipeArgs[execArg][i] != '\0') {
            fileToExec[j] = pipeArgs[execArg][i];
            j++;
          } else {
            break;
          }
        }
        // i++;
      }
      if (fileToExec[j - 1] == ' ') {
        fileToExec[j - 1] = '\0';
      } else {
        fileToExec[j] = '\0';
      }
      char *commandArgsArr2[50];
      int commandArgsCounter2 = 0;
      char *justCommand = strtok(commandToExec, " ");
      commandArgsArr2[commandArgsCounter2++] = justCommand;
      while (justCommand != NULL) {
        justCommand = strtok(NULL, " ");
        commandArgsArr2[commandArgsCounter2++] = justCommand;
      }
      // so far we have command in commandToExec and lastFile.txt in
      // fileToExec
      for (i_binaryCheck = 0; i_binaryCheck < allPathTokensCounterGlobal;
           i_binaryCheck++) {
        int sizeOfPath_binaryCheck = strlen(allPathTokensGlobal[i_binaryCheck]);
        sizeOfPath_binaryCheck += 3;
        char dest_binaryCheck[sizeOfPath_binaryCheck];
        memset(dest_binaryCheck, 0, sizeof(dest_binaryCheck));
        strcpy(dest_binaryCheck, allPathTokensGlobal[i_binaryCheck]);
        strcat(dest_binaryCheck, "/");
        strcat(dest_binaryCheck, commandArgsArr2[0]);
        int returnVal_binaryCheck =
            stat(dest_binaryCheck, &fileStat_binaryCheck);
        if (returnVal_binaryCheck >= 0) {
          int fd1 = open(fileToExec, O_RDWR | O_CREAT, 0666);
          // close(STDOUT_FILENO);
          dup2(fd1, STDOUT_FILENO);
          close(fd1);
          execve(dest_binaryCheck, commandArgsArr2, envp);
          return 0;
        }
        if (returnVal_binaryCheck < 0 &&
            i_binaryCheck == allPathTokensCounterGlobal - 1) {
          return -1;
        }
      }
    } else if (allRedirectIn) {
      // command < file< file....
      char commandToExec[50];
      char fileToExec[50];
      int i = 0;
      int j = 0;
      while (pipeArgs[execArg][i] != '<') {
        commandToExec[j] = pipeArgs[execArg][i];
        i++;
        j++;
      }

      int numberOfRedirects = 0;
      i = 0;
      j = 0;
      while (true) {
        i++;
        if (pipeArgs[execArg][i] == '<') {
          numberOfRedirects++;
          i++;
          continue;
        }
        if (numberOfRedirects == redirectIn) {
          // we hit the last '>', the following thing is the final file
          if (pipeArgs[execArg][i] != '\0') {
            fileToExec[j] = pipeArgs[execArg][i];
            j++;
          } else {
            break;
          }
        }
      }
      if (fileToExec[j - 1] == ' ') {
        fileToExec[j - 1] = '\0';
      } else {
        fileToExec[j] = '\0';
      }
      char *commandArgsArr2[50];
      int commandArgsCounter2 = 0;
      char *justCommand = strtok(commandToExec, " ");
      commandArgsArr2[commandArgsCounter2++] = justCommand;
      while (justCommand != NULL) {
        justCommand = strtok(NULL, " ");
        commandArgsArr2[commandArgsCounter2++] = justCommand;
      }
      // so far we have command in commandToExec and lastFile.txt in
      // fileToExec
      for (i_binaryCheck = 0; i_binaryCheck < allPathTokensCounterGlobal;
           i_binaryCheck++) {
        int sizeOfPath_binaryCheck = strlen(allPathTokensGlobal[i_binaryCheck]);
        sizeOfPath_binaryCheck += 3;
        char dest_binaryCheck[sizeOfPath_binaryCheck + 5];
        memset(dest_binaryCheck, 0, sizeof(dest_binaryCheck));
        strcpy(dest_binaryCheck, allPathTokensGlobal[i_binaryCheck]);
        strcat(dest_binaryCheck, "/");
        strcat(dest_binaryCheck, commandArgsArr2[0]);
        int returnVal_binaryCheck =
            stat(dest_binaryCheck, &fileStat_binaryCheck);
        if (returnVal_binaryCheck >= 0) {
          int fd1 = open(fileToExec, O_RDONLY);
          if (fd1 < 0) {
            fprintf(stderr, "BAD FD1 \n");
          }
          dup2(fd1, STDIN_FILENO);
          close(fd1);
          execve(dest_binaryCheck, commandArgsArr2, envp);
          return 0;
        }
        if (returnVal_binaryCheck < 0 &&
            i_binaryCheck == allPathTokensCounterGlobal - 1) {
          return -1;
        }
      }
    } else {
      // command's contain different redirects (i.e program(ls) < file >
      // file)
    }

    return 0;
  } else {
    char *commandArgsArr[MAX_INPUT];
    int commandArgsCounter = 0;
    char *token = strtok(pipeArgs[execArg], " ");
    commandArgsArr[commandArgsCounter++] = token;

    while (token != NULL) {
      token = strtok(NULL, " ");
      commandArgsArr[commandArgsCounter++] = token;
    }

    for (i_binaryCheck = 0; i_binaryCheck < allPathTokensCounterGlobal;
         i_binaryCheck++) {
      //  printf("HERE\n\n\n");
      int sizeOfPath_binaryCheck = strlen(allPathTokensGlobal[i_binaryCheck]);
      sizeOfPath_binaryCheck += 3;
      char dest_binaryCheck[sizeOfPath_binaryCheck];
      strcpy(dest_binaryCheck, allPathTokensGlobal[i_binaryCheck]);
      strcat(dest_binaryCheck, "/");
      strcat(dest_binaryCheck, commandArgsArr[0]);
      int returnVal_binaryCheck = stat(dest_binaryCheck, &fileStat_binaryCheck);
      if (returnVal_binaryCheck >= 0) {
        execve(dest_binaryCheck, commandArgsArr, envp);
        return 0;
      }
      if (returnVal_binaryCheck < 0 &&
          i_binaryCheck == allPathTokensCounterGlobal - 1) {
        return -1;
      }
    }
    return 0;
  }
}

int sampleRedirect(char **argv, char **envp) {
  fixArguments();
  int i = 0;
  int redirectionArgs = 0;
  int pipeArgs = 0;
  int isBackground = 0;
  for (int i = 0; i < allTokensCounterGlobal; ++i) {
    if (allTokensGlobal[i][0] == '&' && allTokensGlobal[i][1] == '\0') {
      isBackground = 1;
      allTokensCounterGlobal--;
    }
    if (isBackground && allTokensGlobal[i + 1] != NULL) {
      strcpy(allTokensGlobal[i], allTokensGlobal[i + 1]);
    } else if (allTokensGlobal[allTokensCounterGlobal + 1] == NULL) {
      allTokensGlobal[allTokensCounterGlobal] = NULL;
    }
  }
  while (i < allTokensCounterGlobal) {
    if (strstr(allTokensGlobal[i], ">") != NULL) {
      redirectionArgs++;
    } else if (strstr(allTokensGlobal[i], "<") != NULL) {
      redirectionArgs++;
    } else if (strstr(allTokensGlobal[i], "|") != NULL) {
      pipeArgs++;
    }
    i++;
  }

  totalRedirectionArgs = redirectionArgs + pipeArgs;
  int j = 0;
  bool pipe = false;
  char *filePath = NULL;
  int takeInputFromFd = -1;
  for (j = 0; j < allTokensCounterGlobal; j++) {
    if (strstr(allTokensGlobal[j], ">") != NULL) {
      redirectOut = true;
      filePath = allTokensGlobal[j + 1];
      char *ptr = strstr(allTokensGlobal[j], ">");
      ptr -= 1;
      takeInputFromFd = *ptr - 48;
    } else if (strstr(allTokensGlobal[j], "<") != NULL) {
      redirectIn = true;
      char *ptr = strstr(allTokensGlobal[j], "<");
      ptr -= 1;
      takeInputFromFd = *ptr - 48;
    } else if (strstr(allTokensGlobal[j], "|") != NULL) {
      pipe = true;
    }
  }
  // now we know type of redirect happening
  if (takeInputFromFd < 0 || takeInputFromFd > 100)
    takeInputFromFd = STDOUT_FILENO;
  // if pipe then pipe here before you fork
  if (pipe) {
    pipeReq = true;
    samplePipe(argv, envp);
    return 0;
  }
  pid = 0;
  errno = 0;
  pid_t childpid = fork();
  if (childpid == 0) {
    if (redirectOut) {
      int fd1 = open(filePath, O_RDWR | O_CREAT, 0666);
      dup2(fd1, takeInputFromFd);
      close(fd1);
    } else if (redirectIn) {
      int fd1 = open(filePath, O_RDONLY);
      dup2(takeInputFromFd, fd1);
      close(fd1);
    }
    binaryCheck2(argv, envp);
    exit(0);
  } else {
    foregroundJob.pid = childpid;
    for (int i = 1; i < MAX_INPUT + 1; ++i) {
      if (jobsCounter[i - 1] != 1) {
        if (!isBackground) {
          foregroundJob.num = i;
          backgroundJobs[i - 1].isRunning = 1;
        } else {
          backgroundJobs[i - 1].num = i;
          backgroundJobs[i - 1].isRunning = 1;
          backgroundJobs[i - 1].pid = foregroundJob.pid;
        }
        for (int j = 0; j < allTokensCounterGlobal; j++) {
          foregroundJob.name[j] = (char *)calloc(1024, 1);
          backgroundJobs[i - 1].name[j] = (char *)calloc(1024, 1);
          strcpy(foregroundJob.name[j], allTokensGlobal[j]);
          strcpy(backgroundJobs[i - 1].name[j], allTokensGlobal[j]);
        }
        jobsCounter[i - 1] = 1;
      }
      break;
    }
    foregroundJob.isRunning = 1;
    struct termios t;
    tcgetattr(0, &t);
    t.c_lflag &= ~(ECHO | ICANON);
    tcsetattr(0, TCSANOW, &t);
    t.c_cc[VTIME] = 1;
    t.c_cc[VMIN] = 0;
    tcsetattr(0, TCSANOW, &t);
    while (!pid && !isBackground) {
      char ch = 0;
      read(1, &ch, 1);
      char last_char = ch;
      if (last_char == 3) {
        kill(foregroundJob.pid, SIGKILL);
        pid = 1;
        printf("Job [%d] (%d) killed by signal %d\n", foregroundJob.num,
               foregroundJob.pid, SIGKILL);
        jobsCounter[foregroundJob.num - 1] = 0;
      } else if (last_char == 26) {
        backgroundJobs[foregroundJob.num - 1].num = foregroundJob.num;
        backgroundJobs[foregroundJob.num - 1].isRunning = 0;
        backgroundJobs[foregroundJob.num - 1].pid = foregroundJob.pid;
        kill(foregroundJob.pid, SIGTSTP);
        printf("Job [%d] (%d) stopped by signal %d\n", foregroundJob.num,
               foregroundJob.pid, SIGTSTP);
        pid = 1;
      }
    }
    t.c_cc[VTIME] = TIME;
    t.c_cc[VMIN] = MIN;
    return 0;
  }
}

void fixArguments() {
  if ((!redirectOut && !redirectIn) || (totalRedirectionArgs < 2))
    return;

  int i = 0;
  int copyFrom = 0;
  int nextRedirect = 0;
  char *prevRedirectSym = NULL;

  for (i = 0; i < allTokensCounterGlobal; i++) {
    if (strstr(allTokensGlobal[i], ">") != NULL) {
      nextRedirect++;
      if (nextRedirect == 1) {
        prevRedirectSym = allTokensGlobal[i];
      }
    } else if (strstr(allTokensGlobal[i], "<") != NULL) {
      nextRedirect++;
      if (nextRedirect == 1) {
        prevRedirectSym = allTokensGlobal[i];
      }
    }

    if (nextRedirect == 2) {
      copyFrom = i;
      break;
    }
  }

  int j = 0;
  bool copiedOneRedirect = false;
  for (i = 0; i < allTokensCounterGlobal; i++) {
    if ((strcmp(allTokensGlobal[i], prevRedirectSym) != 0)) {
      allTokensGlobal[j] = allTokensGlobal[i];
      j++;
    } else {
      if (copiedOneRedirect == false) {
        i = copyFrom;
        allTokensGlobal[j] = allTokensGlobal[i];
        j++;
        copiedOneRedirect = true;
      } else {
        allTokensGlobal[j] = allTokensGlobal[i];
        j++;
      }
    }
  }
  allTokensCounterGlobal = j;
  i = 0;
}

int samplePipe(char **argv, char **envp) {
  fixArguments2();
  int new_fds[2] = { 0 };
  int old_fds[2] = { 0 };
  int i = 0;
  for (i = 0; i < pipeArgsCounter; i++) {
    if ((i + 1) < pipeArgsCounter) {
      pipe(new_fds);
    }

    pid_t pid = fork();
    if (pid == 0) {
      // child process
      // was there a command prior to this?
      if (i > 0) {
        // duplicate STDIN to 0
        dup2(old_fds[0], STDIN_FILENO);
        // CLOSE STDIN & STDOUT
        close(old_fds[0]);
        close(old_fds[1]);
      }
      // is there a command after this one?
      if ((i + 1) < pipeArgsCounter) {
        // close stdin
        close(new_fds[0]);
        // dup stdout
        dup2(new_fds[1], STDOUT_FILENO);
        // close stdout
        close(new_fds[1]);
      }
      binaryCheck3(argv, envp, i);

    } else {
      // parent
      // was there a prior command?
      if (i > 0) {
        if (old_fds[0] != STDIN_FILENO)
          close(old_fds[0]);
        if (old_fds[1] != STDOUT_FILENO)
          close(old_fds[1]);
      }
      // is there a command after this?
      if ((i + 1) < pipeArgsCounter) {
        old_fds[0] = new_fds[0];
        old_fds[1] = new_fds[1];
      }
      // wait
      waitpid(pid, &pid, 0);
    }
  }

  if (pipeArgsCounter > 2) {
    close(old_fds[0]);
    close(old_fds[1]);
  }
  return 0;
}

void fixArguments2() {
  int i = 0;
  int j = 0;
  char dest[MAX_INPUT];
  memset(dest, 0, MAX_INPUT);
  for (i = 0; i < allTokensCounterGlobal; i++) {
    if (strstr(allTokensGlobal[i], "|") == NULL) {
      // current token does not have pipe char
      strcat(dest, allTokensGlobal[i]);
      strcat(dest, " ");
    } else {
      strcpy(pipeArgs[j], dest);
      j++;
      memset(dest, 0, MAX_INPUT);
    }
  }
  strcpy(pipeArgs[j], dest);
  j++;
  pipeArgsCounter = j;
}

void runHistory() {
  if (totalCommandsSaved == 0) {
    int historyTotalFD = open("historyFileTotal.txt", (O_RDONLY | O_CREAT),
                              (S_IRUSR | S_IWUSR | S_IXUSR));
    char totalCommandsStr[5];
    int returnVal = read(historyTotalFD, totalCommandsStr, sizeof(int));
    close(historyTotalFD);
    int i = 0;
    int total = 0;
    int mult = 1;
    for (i = 0; i < (returnVal - 1); i++) {
      mult *= 10;
    }

    for (i = 0; i < (returnVal); i++) {
      total += (totalCommandsStr[i] - 48) * mult;
      mult /= 10;
    }
    totalCommandsSaved = total;
  }
  int historyFileDescriptor;
  char str[MAX_INPUT];
  historyFileDescriptor = open("historyFile.txt", O_RDONLY);

  int i = 0;
  int k = 0;
  for (i = 1; i < totalCommandsSaved; i++) {
    lseek(historyFileDescriptor, (-1024 * i), SEEK_END);
    read(historyFileDescriptor, str, 1024);
    char newStr[MAX_INPUT];
    int j = 0;
    for (k = 0; k < MAX_INPUT; k++) {
      if (str[k] == '!')
        break;
      else {
        newStr[j] = str[k];
        j++;
      }
    }
    newStr[j] = '\0';
    sizeOfLastCommand = j;
    printf("%s\n", newStr);
  }
}

void clearHistory() {
  remove("historyFile.txt");
  remove("historyFileTotal.txt");
  open("historyFileTotal.txt", (O_RDONLY | O_CREAT),
       (S_IRUSR | S_IWUSR | S_IXUSR));
  open("historyFile.txt", O_RDONLY | O_CREAT);
}
