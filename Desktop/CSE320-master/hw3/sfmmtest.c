#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <errno.h>
#include "sfmm.h"
#include "sfdebug.h"

// Define 20 megabytes as the max heap size
#define MAX_HEAP_SIZE (20 * (1 << 20))
#define VALUE1_VALUE 320
#define VALUE2_VALUE 0xDEADBEEFF00D

#define press_to_cont() do { \
    printf("Press Enter to Continue"); \
    while(getchar() != '\n'); \
    printf("\n"); \
} while(0)

#define null_check(ptr, size) do { \
    if ((ptr) == NULL) { \
        error("Failed to allocate %lu byte(s) for an integer using sf_malloc.\n", (size)); \
        error("Aborting...\n"); \
        assert(false); \
    } else { \
        success("sf_malloc returned a non-null address: %p\n", (ptr)); \
    } \
} while(0)

#define payload_check(ptr) do { \
    if ((unsigned long)(ptr) % 16 != 0) { \
        warn("Returned payload is not divisble by a quadword. %p %% 16 = %lu\n", (ptr), (unsigned long)(ptr) % 16); \
    } \
} while(0)

#define check_prim_contents(actual_value, expected_value, fmt_spec, name) do { \
    if (*(actual_value) != (expected_value)) { \
        error("Expected " name " to be " fmt_spec " but got " fmt_spec "\n", (expected_value), *(actual_value)); \
        error("Aborting...\n"); \
        assert(false); \
    } else { \
        success(name " retained the value of " fmt_spec " after assignment\n", (expected_value)); \
    } \
} while(0)

int main(int argc, char *argv[]) {
    // Initialize the custom allocator
    sf_mem_init(MAX_HEAP_SIZE);

    // Tell the user about the fields
    info("Initialized heap with %dmb of heap space.\n", MAX_HEAP_SIZE >> 20);
    // Test #1: Allocate an integer
    int *value1 = (int*) sf_malloc(8192);
    sf_snapshot(true);
    int *value2 = (int*) sf_malloc(96);
    sf_snapshot(true);int *value3 = (int*) sf_malloc(96);
    sf_snapshot(true);
    sf_snapshot(true);
    sf_free(value2);
    sf_snapshot(true);
    int *value7 = (int*) sf_malloc(30);
    sf_snapshot(true);
    sf_varprint(value7);
    //sf_free(value3);
    sf_snapshot(true);
    int *value11 = (int*) sf_malloc(13);
    //return EXIT_SUCCESS;
    sf_snapshot(true);
    int *value4 = (int*) sf_malloc(64);
    sf_free(value4);
    sf_malloc(36);
    int *value5 = (int*) sf_malloc(80);
    int *value6 = (int*) sf_malloc(128);
    sf_free(value6);
    sf_snapshot(true);
    int *value8 = (int*) sf_malloc(128);
    //sf_snapshot(true);
    int *value9 = (int*) sf_malloc(112);
    int *value10 = (int*) sf_malloc(128);
    //return EXIT_SUCCESS;
    sf_snapshot(true);
    sf_free(value8);
    sf_free(value2);
    sf_snapshot(true);
    sf_snapshot(true);
    sf_free(value3);
    sf_snapshot(true);
    sf_free(value5);
    sf_snapshot(true);
    sf_free(value1);
    sf_snapshot(true);
    sf_free(value7);
    sf_snapshot(true);
    sf_free(value9);
    sf_snapshot(true);
    sf_varprint(value6);
    sf_free(value10);
    sf_snapshot(true);

    int *value12 = (int*) sf_malloc(96);
    sf_snapshot(true);
    int *value13 = (int*) sf_malloc(96);
    sf_snapshot(true);int *value14 = (int*) sf_malloc(96);
    sf_snapshot(true);
    sf_snapshot(true);
    sf_free(value12);
    sf_snapshot(true);int *value15 = (int*) sf_malloc(30);
    sf_snapshot(true);
    sf_snapshot(true);
    int *value16 = (int*) sf_malloc(13);
    sf_snapshot(true);
    int *value17 = (int*) sf_malloc(64);
    sf_free(value11);
    sf_malloc(36);
    int *value18 = (int*) sf_malloc(80);
    int *value19 = (int*) sf_malloc(128);
    sf_free(value18);
    //return EXIT_SUCCESS;
    sf_snapshot(true);
    //sf_snapshot(true);
    int *value20 = (int*) sf_malloc(128);

    //sf_snapshot(true);
    int *value21 = (int*) sf_malloc(112);
    int *value23 = (int*) sf_malloc(128);
    sf_free(value13);
    sf_snapshot(true);
    sf_free(value15);
    sf_snapshot(true);
    sf_free(value14);
    sf_snapshot(true);
    sf_free(value16);
    sf_snapshot(true);
    //return EXIT_SUCCESS;
    sf_varprint(value17);
    sf_free(value17);
    sf_snapshot(true);
    sf_free(value12);
    sf_snapshot(true);
    void *memory = sf_malloc(8192);
    sf_free(memory);
    int* value111 = (int *)sf_malloc(16);
    int* value22 = (int *)sf_malloc(48);
    int* value33 = (int *)sf_malloc(48);
    sf_snapshot(true);
    value111[0] = 6;
    value111[1] = 7;
    value111 = (int *)sf_realloc(value111, 80);
    sf_snapshot(true);
    return EXIT_SUCCESS;
    sf_varprint(value1);
    sf_varprint(value2);
    sf_varprint(value3);
    sf_varprint(value4);
    sf_varprint(value5);
    sf_varprint(value6);
    sf_varprint(value7);
    sf_varprint(value8);
    sf_varprint(value9);
    sf_varprint(value10);
    sf_varprint(value11);
    sf_varprint(value12);
    sf_varprint(value13);
    sf_varprint(value14);
    sf_varprint(value15);
    sf_varprint(value16);
    sf_varprint(value17);
    sf_varprint(value18);
    sf_varprint(value19);
    sf_varprint(value20);
    sf_varprint(value21);
    sf_varprint(value23);
    sf_varprint(value22);
    sf_varprint(value33);
    
}
