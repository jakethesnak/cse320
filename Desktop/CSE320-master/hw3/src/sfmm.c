/**
 * All functions you make for the assignment must be implemented in this file.
 * Do not submit your assignment with a main function in this file.
 * If you submit with a main function in this file, you will get a zero.
 */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
 #include <string.h>
#include "sfmm.h"

/**
 * You should store the head of your free list in this variable.
 * Doing so will make it accessible via the extern statement in sfmm.h
 * which will allow you to pass the address to sf_snapshot in a different file.
 */
sf_free_header *freelist_head = NULL;
sf_free_header *cursor = NULL;

bool count = false;

#define PAGE_SIZE 4096
#define MAX_REQ_SIZE 1073741824
#define LIFO2 1
#define FIRST2 1
#ifdef ADDRESS
	#undef LIFO2
	#define LIFO2 0
#endif

#ifdef NEXT
	#undef FIRST2
	#define FIRST2 0
#endif 

#ifdef LIFO
	#ifdef ADDRESS
		#error
	#endif
#endif

#ifdef FIRST
	#ifdef NEXT
		#error
	#endif
#endif

void* sf_malloc(size_t size) {
	if (!count)
	{
		count = true;
		void *p = sf_sbrk(PAGE_SIZE);
		sf_footer *root = p;
		root->alloc = 1;
		freelist_head = p + 8;
		freelist_head->header.alloc = 0;
		freelist_head->header.block_size = PAGE_SIZE/16;
		freelist_head->header.requested_size = 0;
		freelist_head->next = NULL;
		freelist_head->prev = NULL;
		sf_footer *foot = p + PAGE_SIZE;
		foot->alloc = 0;
		foot->block_size = PAGE_SIZE/16;
		cursor = freelist_head;
		
	}
	void *return_ptr = NULL;
	if (size < MAX_REQ_SIZE && size > 0)
	{
		bool first_look = false;
		if (FIRST2)
		{
			cursor = freelist_head;
		} else{
			first_look = true;
		}
		while(true){
			if (cursor->header.block_size * 16 > size)
			{
				if (-size % PAGE_SIZE < 48)
				{
					errno = 0;
					void *ptr3 = sf_sbrk(PAGE_SIZE);
					if (errno == 12)
					{
						errno = ENOMEM;
						return NULL;
					}
					sf_footer *foot2 = ptr3;
					void *ptr4 = ptr3 - foot2->block_size * 16 + 8;
					sf_free_header *new_node = ptr4;
					new_node->header.block_size += PAGE_SIZE / 16;
					sf_footer *foot3 = ptr3 + PAGE_SIZE;
					foot3->alloc = 0;
					foot3->block_size = new_node->header.block_size;
					cursor = new_node;
					if (LIFO2)
					{
						if (cursor != freelist_head)
						{
						 	if (freelist_head->next == cursor)
						 	{
						 		freelist_head->prev = cursor;
						 		freelist_head->next = cursor->next;
						 		if (cursor->next != NULL)
					 			{
					 				cursor->next->prev = freelist_head;
						 		}
						 		cursor->next = freelist_head;
						 		new_node->prev = NULL;
						 	}else{
						 		freelist_head->prev = cursor;
						 		cursor->prev->next = cursor->next;
					 			if (cursor->next != NULL)
					 			{
					 				cursor->next->prev = cursor->prev;
						 		}
						 		cursor->next = freelist_head;
						 		new_node->prev = NULL;
						 	}
						} 
					}
				}
				return_ptr = &cursor->header + 0x1;
				cursor->header.alloc = 1;
				cursor->header.requested_size = size;
				int oldSize = cursor->header.block_size;
				if (size % 16 != 0)
				{
					cursor->header.block_size = (size + 16 - size % 16) / 16 + 0x1;
				}else{
					cursor->header.block_size = (size) / 16 + 0x1;
				}
				void *ptr = &cursor->header + cursor->header.block_size * 2;
				sf_footer *foot = ptr - 8;
				foot->alloc = 1;
				foot->block_size = cursor->header.block_size;
				sf_free_header *new_node = (sf_free_header *) ptr;
				new_node->header.alloc = 0;
				new_node->header.block_size = oldSize -  cursor->header.block_size;
				void *ptr2 = &cursor->header + oldSize * 2;
				sf_footer *update_foot  =  ptr2 - 8;
				update_foot->block_size = new_node->header.block_size;
				if (LIFO2 && new_node->header.block_size != 0)
				{
					if (new_node->header.block_size*16 <= 16)
					{
						sf_footer *foot = ptr + 8;
						foot->alloc = 1;
						foot->block_size = cursor->header.block_size + 16;
						cursor->header.block_size += 16;
						freelist_head = cursor->next;
					} else
					{
						if (freelist_head == cursor)
						{
							freelist_head = ptr;
							freelist_head->next = cursor->next;
							freelist_head->prev = cursor->prev;
							if (cursor->next != NULL)
								{
								cursor->next->prev = freelist_head;
							}
							if (cursor->prev != NULL)
							{
								cursor->prev->next = freelist_head;
							}
						} else{
							freelist_head->prev = new_node;
							if (cursor->next != NULL)
							{
								cursor->next->prev = cursor->prev;
							}
							if (cursor->prev != NULL)
							{
								cursor->prev->next = cursor->next;
							}
							new_node->next = freelist_head;
							freelist_head = new_node;

						}
					}
				} else if(new_node->header.block_size != 0){
					if (!FIRST2)
					{
						new_node->prev = cursor->prev;
						if (cursor->next != NULL)
						{
							cursor->next->prev = new_node;
						}
						new_node->next = cursor->next;
						if (cursor->prev != NULL )
						{
							cursor->prev->next = new_node;
							
						}
						if (freelist_head == cursor)
						{
							freelist_head = new_node;
						}
					} else{
						new_node->prev = cursor->prev;
						if (cursor->next != NULL)
						{
							cursor->next->prev = new_node;
						}
						new_node->next = cursor->next;
						if (cursor->prev != NULL)
						{
							cursor->prev->next = new_node;
						}
						if (freelist_head == cursor)
						{
							freelist_head = new_node;
						}
					}
				} else{
					freelist_head = cursor->next;
					if (freelist_head == NULL)
					{
						errno = 0;
						void *ptr3 = sf_sbrk(PAGE_SIZE);
						if (errno == ENOMEM)
						{
							errno = ENOMEM;
							return NULL;
						}
						sf_footer *foot2 = ptr3;
						void *ptr4 = ptr3 - foot2->block_size * 16 + 8;
						sf_free_header *new_node = ptr4;
						new_node->header.block_size += PAGE_SIZE / 16;
						sf_footer *foot3 = ptr3 + PAGE_SIZE;
						foot3->alloc = 0;
						foot3->block_size = new_node->header.block_size;
						cursor = new_node;
						if (LIFO2)
						{		
							freelist_head = (sf_free_header *) ptr4;
						} else{
							freelist_head = (sf_free_header *) ptr4;
						}
					}
				}
				if (!FIRST2)
				{
					cursor = new_node;
				}
				break;
			}else if(cursor->next == NULL)
			{

				if (!first_look)
				{
					errno = 0;
					void *ptr3 = sf_sbrk(PAGE_SIZE);
					if (errno == ENOMEM)
					{
						errno = ENOMEM;
						return NULL;
					}
					sf_footer *foot2 = ptr3;
					void *ptr4 = ptr3 - foot2->block_size * 16 + 8;
					sf_free_header *new_node = ptr4;
					new_node->header.block_size += PAGE_SIZE / 16;
					sf_footer *foot3 = ptr3 + PAGE_SIZE;
					foot3->alloc = 0;
					foot3->block_size = new_node->header.block_size;
					cursor = new_node;
					if (LIFO2)
					{
						if (cursor != freelist_head)
						{
						 	if (freelist_head->next == cursor)
						 	{
						 		freelist_head->prev = cursor;
						 		freelist_head->next = cursor->next;
						 		if (cursor->next != NULL)
						 		{
						 			cursor->next->prev = freelist_head;
						 		}
						 		cursor->next = new_node;
						 		new_node->prev = NULL;
						 	}else{
						 		freelist_head->prev = cursor;
						 		if (cursor->prev != NULL)
						 		{
						 			cursor->prev->next = cursor->next;
						 		}
						 		if (cursor->next != NULL)
						 		{
						 			cursor->next->prev = cursor->prev;
						 		}
						 		cursor->next = freelist_head;
						 		new_node->prev = NULL;
						 	}
						} 
					}
				}else{
					first_look = false;
				}
			}else{
				cursor = cursor->next;
			}
		}
	} else if(size > 0){
		errno = EDOM;
	}
	
    return return_ptr;
}

void sf_free(void *ptr) {
	if (ptr == NULL)
	{
		errno = EFAULT;
		return;
	}
	sf_free_header *new_node = ptr - 8;
	if (new_node->header.alloc != 1)
	{
		errno = EFAULT;
		return;
	}
	sf_free_header *is_addr = NULL;
	if (!LIFO2)
	{
		is_addr = new_node;
	}
	sf_footer *prev_foot = ptr - 16;
	sf_footer *new_foot = ptr + new_node->header.block_size * 16 - 0x10;
	if (prev_foot->alloc == 0){
		sf_free_header *prev = ptr - prev_foot->block_size * 16 - 8;
		prev->header.block_size += new_node->header.block_size;
		new_foot->block_size = prev->header.block_size;
		new_node = prev;
		prev_foot->block_size = new_node->header.block_size;
		new_foot->block_size = new_node->header.block_size;
		new_foot->alloc = 0;
	}
	new_node->header.alloc = 0;
	sf_header *next_head = (void *)new_foot + 8;
	if (next_head->alloc == 0 && next_head->block_size != 0){
		if (prev_foot->alloc != 0)
		{
			sf_footer *next_foot = (void *)next_head + next_head->block_size * 16 - 8;
			next_foot->block_size += new_node->header.block_size;
			new_foot = next_foot;
			sf_free_header *next = (void *)next_head;
			new_node->header.block_size += next->header.block_size;
			if (LIFO2)
			{
				if (freelist_head == next)
				{
					new_node->next = freelist_head->next;
					if (new_node->next != NULL)
					{
						new_node->next->prev = new_node;
					}
					freelist_head->next = next->next;
				} else{
					new_node->next = freelist_head;
				}
				new_node->prev = NULL;
				freelist_head->prev = new_node;
				if (next->next != NULL)
				{
					next->next->prev = freelist_head;
				}
				if (next->prev != NULL)
				{
					next->prev->next = next->next;
				}
				freelist_head = new_node;
			} else{
				new_node->next = next->next;
				if (next->next != NULL)
				{
					next->next->prev = new_node;
				}
				if (next->prev != NULL)
				{
					next->prev->next = new_node;
				}
				new_node->prev = next->prev;
				if (next == freelist_head)
				{
					freelist_head = new_node;
				}
			}
			if (!FIRST2)
			{
				if (cursor == next)
				{
					cursor = new_node;
				}
			}
		} else if (next_head->block_size != 0){
			sf_footer *next_foot = (void *)next_head + next_head->block_size * 16 - 8;
			next_foot->block_size += new_node->header.block_size;
			new_foot = next_foot;
			new_node->header.block_size = next_foot->block_size;
			sf_free_header *next = (void *)next_head;
			if (LIFO2)
			{
				if (freelist_head != next && freelist_head != new_node)
				{
					freelist_head->prev = new_node;
					if (new_node->next == next)
					{
						if (freelist_head->next == new_node)
						{
							if (next->next != NULL)
							{
								next->next->prev = freelist_head;
							}
							new_node->next = freelist_head;
							freelist_head->next = next->next;
						} else{
							new_node->next = freelist_head;
							if (new_node->prev!=NULL)
							{
								new_node->prev->next = next->next;
							}
							if (next->next != NULL)
							{
								next->next->prev = new_node->prev;
							}
						}
					} else if(next->next == new_node){
						if (freelist_head->next == next)
						{
							freelist_head->next = new_node->next;
							if (new_node->next != NULL)
							{
								new_node->next->prev = freelist_head;
							}
							new_node->next = freelist_head;
						}else{
							if (new_node->next !=NULL)
							{
								new_node->next->prev = next->prev;
							}
							new_node->next = freelist_head;
							if (next->prev !=NULL)
							{
								next->prev->next = new_node->next;
							}
						}
					}else{
						if (freelist_head->next == new_node)
						{
							if (new_node->next != NULL)
							{
								new_node->next->next = next->next;
								new_node->next->prev = freelist_head;
							}
							if (next->next != NULL)
							{
								next->next->prev = new_node->next;
							}
							freelist_head->next = new_node->next;
							new_node->next = freelist_head;
						}else if(freelist_head->next == next){
							if (next->next != NULL)
							{
								next->next->next = new_node->next;
								next->next->prev = freelist_head;
							}
							if (new_node->next != NULL)
							{
								new_node->next->prev = next->next;
							}
							freelist_head->next = next->next;
							new_node->next = freelist_head;
						} else{
							sf_free_header *cur = freelist_head;
							while(true){
								if (cur->next == new_node)
								{
									if (new_node->prev != NULL)
									{
										new_node->prev->next = new_node->next;
									}
									if (new_node->next != NULL)
									{
										new_node->next->prev = new_node->prev;
									}
									if (next->prev != NULL)
									{
										next->prev->next = next->next;
									}
									if (next->next != NULL)
									{
										next->next->prev = new_node->next;
									}
									new_node->next = freelist_head;
									break;
								}else if (cur->next == next)
								{
									if (next->prev != NULL)
									{
										next->prev->next = next->next;
									}
									if (next->next != NULL)
									{
										next->next->prev = next->prev;
									}
									if (new_node->prev != NULL)
									{
										new_node->prev->next = new_node->next;
									}
									if (new_node->next != NULL)
									{
										new_node->next->prev = new_node->next;
									}
									new_node->next = freelist_head;
									break;
								}
								cur = cur->next;
							}
						}
					}
					new_node->prev = NULL;
				} else if (freelist_head == new_node)
				{
					if (freelist_head->next == next)
					{
						new_node->next = next->next;
						if (next->next != NULL)
						{
							next->next->prev = new_node;
						}
					} else {
						if (next->prev != NULL)
						{
							next->prev->next = next->next;
						}
						if (next->next != NULL)
						{
							next->next->prev = freelist_head->next;
						}
					}
				} else{
					if (freelist_head->next == new_node)
					{
					} else {
						if (new_node->next != NULL)
						{
							new_node->next->prev = new_node->prev;
						}
						new_node->prev->next = new_node->next;
						new_node->next = freelist_head->next;
						freelist_head->next->prev = new_node;
					}
					new_node->prev = NULL;
				}
				freelist_head = new_node;
			} else{
				new_node->next = next->next;
				if (next->next != NULL)
				{
					next->next->prev = new_node;
				}
			}
			if (!FIRST2)
			{
				if (cursor == next)
				{
					cursor = new_node;
				}
			}
		}
	} else if (next_head->alloc == 1 && next_head->block_size != 0){
		if (prev_foot->alloc != 0)
		{
			if (LIFO2)
			{
				new_node->next = freelist_head;
				new_node->prev = NULL;
				freelist_head->prev = new_node;
				freelist_head = new_node;
			} else{
				sf_free_header *cur = freelist_head;
				while(true){
					if (&new_node->header < &freelist_head->header)
					{
						new_node->next = freelist_head;
						freelist_head->prev = new_node;
						new_node->prev = NULL;
						freelist_head = new_node;
						break;
					}
					if (&cur->header < &new_node->header && &cur->next->header > &new_node->header)
					{
						new_node->next = cur->next;
						new_node->prev = cur;
						if (cur->next != NULL)
						{
							cur->next->prev = new_node;
						}
						cur->next = new_node;
						break;
					}else{
						cur = cur->next;
					}
				}
			}
		}
	}
	if (next_head->block_size != 0 )
	{
		new_foot->block_size = new_node->header.block_size;
		new_foot->alloc = 0;
	}
}

void* sf_realloc(void *ptr, size_t size) {
	if (ptr == NULL)
	{
		errno = EFAULT;
		return NULL;
	} else if (size == 0)
	{
		errno = EDOM;
		return NULL;
	}
	sf_header *thingy = ptr - 8;
	if (thingy->alloc != 1)
	{
		errno = EFAULT;
		return NULL;
	}
	if (size < thingy->block_size * 16 - 16)
	{
		if (thingy->block_size * 16 - 16 - size >= 32)
		{
			thingy->requested_size = size;
			int oldSize = thingy->block_size;
			if (size % 16 != 0)
			{
				thingy->block_size = (size + 16 - size % 16) / 16 + 0x1;
			}else{
				thingy->block_size = (size) / 16 + 0x1;
			}
			void *ptr = thingy + thingy->block_size * 2;
			sf_footer *foot = ptr - 8;
			foot->alloc = 1;
			foot->block_size = thingy->block_size;
			sf_free_header *new_node = ptr;
			new_node->header.alloc = 0;
			new_node->header.block_size = oldSize -  thingy->block_size;
			void *ptr2 = thingy + oldSize * 2;
			sf_footer *update_foot  =  ptr2 - 8;
			update_foot->block_size = new_node->header.block_size;
			update_foot->alloc = 0;
			update_foot->block_size = new_node->header.block_size;
			if (LIFO2)
			{
				new_node->next = freelist_head;
				new_node->prev = NULL;
				freelist_head->prev = new_node;
				freelist_head = new_node;
			} else{
				sf_free_header *cur = freelist_head;
				while(true){
					if (&new_node->header < &freelist_head->header)
					{
						new_node->next = freelist_head;
						freelist_head->prev = new_node;
						new_node->prev = NULL;
						freelist_head = new_node;
						break;
					}
					if (&cur->header < &new_node->header && &cur->next->header > &new_node->header)
					{
						new_node->next = cur->next;
						new_node->prev = cur;
						if (cur->next != NULL)
						{
							cur->next->prev = new_node;
						}
						cur->next = new_node;
						break;
					}else{
						cur = cur->next;
					}
				}
			}
			return ptr;
		} else{
			thingy->requested_size = size;
			return ptr;
		}
	}
	sf_footer *footy = ptr + thingy->block_size * 16 - 16;
	void *ptr1 = ptr;
	int i[((void*)footy-(void*)thingy)/4];
	int counter = 0;

	while(true){
		i[counter] = *((int *) ptr);
		ptr += 4;
		if (ptr == footy)
		{
			break;
		}
		counter++;
	}
	counter = 0;
	errno = 0;
	sf_free(ptr1);
	if (errno != 0)
	{
		return NULL;
	}
	int *rtrn = (int *)sf_malloc(size);
	while(counter < ((void*)footy-(void*)thingy)/4){
		rtrn[counter] = i[counter];
		counter++;
	}
	return rtrn;
}

void* sf_calloc(size_t nmemb, size_t size) {

	int real_size = nmemb * size;
	if (size == 0 || nmemb == 0)
	{
		errno = EDOM;
		return NULL;
	}
	void *return_ptr = NULL;
	return_ptr = sf_malloc(real_size);
	memset(return_ptr, 0, real_size);
    return return_ptr;
}