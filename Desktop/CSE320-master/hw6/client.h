#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <termios.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/file.h>
#include <sys/wait.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include "sfwrite.h"

#define MAXLINE 1024

#define _GNU_SOURCE

int helpMenu();
int helpEntered();
int logoutEntered();
int listuEntered();
int handleSTDIN(char buf[]);
int handleServer();
int handleChats(int fd);
int addToChatNodeList(int fileDescript, char usernameOfClient[],
                      char usernameOfRecipient[], pid_t child);
int listTraverse(char checkName[]);
int listTraverse2();
void signal_catch(int sigNum);
void signal_catch2(int sigNum);
int removeChatFD(int fd);
int addChatFD(int fd);
int setSelectFDs();
int checkProtocol(bool createClient, bool verboseFlag);
int writeToAudit(char* msg, bool login, bool chatMSG, bool logout,
                 bool command);
int writeToAudit2(char* msg, char* to, char* from, char* chatMSG);
int initLoginMethod();
int newPrint(char* msg);
