#include <pthread.h>
#include <stdarg.h>
#include <stdio.h>

void sfwrite(pthread_mutex_t *lock, FILE* stream, char *fmt, ...);