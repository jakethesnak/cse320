#include "server.h"

int verbose = 0, listenfd;
pthread_mutex_t termlock, wlock;
char *MOTD;
static active_users pool;
static accounts *acc_Head;
static Set set;
static sfbuffer buffer;
accounts *cur;
int fp;

void sigint_handler(int sig) {
  int i;
  for (i = 0; i < FD_SETSIZE; ++i) {
    if (pool.client[i].fd > 0) {
      char *buf = "BYE \r\n\r\n";
      int num = write(pool.client[i].fd, buf, strlen(buf));
      if (verbose) {
        sfwrite(&termlock, stderr, "\x1B[1;34mSent: BYE Byte Count: %d\n\x1B[0m", num);
      }
      pthread_mutex_destroy(&pool.client[i].lock);
      errno = 0;
      close(pool.client[i].fd);
      ERROR(2);
    }
  }
  while (cur != NULL) {
    accounts *temp;
    temp = acc_Head;
    while (temp != cur->prev) {
      temp = temp->next;
    }
    pthread_mutex_destroy(&cur->lock);
    free(cur);
    cur = temp;
  }
  pthread_mutex_destroy(&set.lock);
  pthread_mutex_destroy(&termlock);
  pthread_mutex_destroy(&wlock);
  pthread_mutex_destroy(&buffer.lock);
  pthread_mutex_destroy(&pool.lock);
  sem_destroy(&buffer.items);
  free(buffer.queue);
  close(listenfd);
  free(MOTD);
  close(fp);
  exit(0);
}

void sigpipe_handler(int sig) {}

int main(int argc, char **argv) {
  signal(SIGINT, sigint_handler);
  signal(SIGPIPE, sigpipe_handler);
  buffer.queue = (int *)calloc(MAXLINE, sizeof(int));
  pthread_mutex_init(&termlock, NULL);
  pthread_mutex_init(&set.lock, NULL);
  pthread_mutex_init(&wlock, NULL);
  pthread_mutex_init(&buffer.lock, NULL);
  pthread_mutex_init(&pool.lock, NULL);
  sem_init(&buffer.items, 0, 0);
  int threadVar = 0;
  long long threadCount = 2;
  buffer.front = buffer.rear =  0;
  int args = commandLineArgs(argc, argv);
  if (args == -1) {
    helpArgs();
    return 1;
  } else if (args == 0) {
    helpArgs();
    return 0;
  } else if (args == 1)
    verbose = 1;
  if (argc < 3) {
    helpArgs();
    return 1;
  }else if (strcmp(argv[1 + verbose], "-t") == 0)
  {
    threadVar = 2;
    errno = 0;
    threadCount = strtoll(argv[1 + verbose + 1], NULL, 10);
    if (threadCount < 0)
    {
      errno = ERANGE;
      threadCount = 2;
    }
    ERROR(6);
  }
  int portNum = 0;
  MOTD = (char *)calloc(MAXLINE, 1);
  for (int i = 1; i < argc; ++i) {
    if (i + verbose + threadVar == 1 + verbose + threadVar) {
      char *str = argv[i + verbose + threadVar];
      for (int j = 0; j < strlen(argv[i + verbose + threadVar]); j++) {
        if ((*str >= '0') && (*str <= '9')) {
          portNum = (portNum * 10) + ((*str) - '0');
          str++;
        } else {
          sfwrite(&termlock, stderr, "\x1B[1;34mError: No such port %s\n",
                  argv[i + verbose + threadVar]);
          helpArgs();
          free(MOTD);
          return 1;
        }
      }
    } else if (i + verbose + threadVar == 2 + verbose + threadVar)
      strcpy(MOTD, argv[i + verbose + threadVar]);
  }
  char *input_path = calloc(MAXLINE, 1);
  if (3 + verbose + threadVar < argc && (argc - 3 - verbose - threadVar) == 1) {
    // input_path = argv[3 + verbose];
    sprintf(input_path, "%s", argv[3 + verbose + threadVar]);
  }
  args = validate_args(input_path);
  // change to sqlite3
  if (args == 1) {
    struct stat sb;
    stat(input_path, &sb);
    FILE *fake = fopen(input_path, "w");
    fclose(fake);
    if ((fp = open(input_path, O_WRONLY)) <= 0) {
      sfwrite(&termlock, stderr, "\t\x1B[1;31mFailed to open the file %s\x1B[0m\n",
              input_path);
      return 1;
    }
    acc_Head = (accounts *)malloc(sizeof(accounts));
    cur = acc_Head;
    pthread_mutex_init(&acc_Head->lock, NULL);
    acc_Head->next = NULL;
    acc_Head->prev = NULL;
  } else if (args == 0) {
    struct stat sb;
    stat(input_path, &sb);
    if ((fp = open(input_path, O_RDONLY)) <= 0) {
      sfwrite(&termlock, stderr, "\t\x1B[1;31mFailed to open the file %s\x1B[0m\n",
              input_path);
      return 1;
    }
    FILE *fake = fdopen(fp, "r");
    unsigned char read_value;
    char *str;
    accounts *new = (accounts *)malloc(sizeof(accounts));
    str = new->c.name;
    int isName = 0, set = 0;
    while ((read_value = (char)fgetc(fake)) != 255) {
      if (set) {
        new = (accounts *)malloc(sizeof(accounts));
        set = 0;
        str = new->c.name;
      }
      if (read_value == '\n' && isName == 0) {
        isName = 1;
        str = new->c.hash;
      } else if (read_value == '\n' && isName == 1) {
        isName = 2;
        str = new->c.salt;
      } else if (read_value == '\n' && isName == 2) {
        isName = 0;
        if (acc_Head == NULL) {
          acc_Head = new;
          cur = acc_Head;
          pthread_mutex_init(&acc_Head->lock, NULL);
          set = 1;
        } else {
          new->prev = cur;
          cur->next = new;
          new->next = NULL;
          cur = new;
          pthread_mutex_init(&cur->lock, NULL);
          set = 1;
        }
      } else {
        *str = read_value;
        str++;
      }
    }
    fclose(fake);
    if (acc_Head == NULL) {
      acc_Head = (accounts *)malloc(sizeof(accounts));
      cur = acc_Head;
      pthread_mutex_init(&acc_Head->lock, NULL);
      acc_Head->next = NULL;
      acc_Head->prev = NULL;
    }
    close(fp);
    if ((fp = open(input_path, O_WRONLY)) <= 0) {
      perror("Open");
      sfwrite(&termlock, stderr, "\t\x1B[1;31mFailed to open the file %s\x1B[0m\n",
              input_path);
      return 1;
    }
    lseek(fp, 0, SEEK_END);
  } else {
    free(input_path);
    input_path = calloc(MAXLINE, 1);
    sprintf(input_path, "accounts.txt");
    struct stat sb;
    stat(input_path, &sb);
    FILE *fake = fopen(input_path, "w");
    fclose(fake);
    if ((fp = open(input_path, O_WRONLY)) <= 0) {
      sfwrite(&termlock, stderr, "\t\x1B[1;31mFailed to open the file %s\x1B[0m\n",
              input_path);
      return 1;
    }
    acc_Head = (accounts *)malloc(sizeof(accounts));
    cur = acc_Head;
    pthread_mutex_init(&acc_Head->lock, NULL);
    acc_Head->next = NULL;
    acc_Head->prev = NULL;
  }
  free(input_path);
  int connfd, optval = 1;
  sfwrite(&termlock, stderr, "\x1B[0mCurrently listening on port %d.\n", portNum);
  struct sockaddr_in serveraddr;
  if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Failed to create socket.\x1B[0m\n");
    return 1;
  }
  if (setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval,
                 sizeof(int)) < 0) 
  {
    ERROR(3);
    return 1;
  }
  memset((char *)&serveraddr, 0, sizeof(serveraddr));
  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short)portNum);
  if (bind(listenfd, (SA *)&serveraddr, sizeof(serveraddr)) < 0) {
    ERROR(4);
    return 1;
  }
  if (listen(listenfd, 1024) < 0) {
    ERROR(5);
    return 1;
  }
  initilizeUsers(listenfd, &pool);
  struct sockaddr_storage clientaddr;
  char hostname[MAXLINE], port[MAXLINE];
  socklen_t clientlen;
  for (int i = 0; i < threadCount; ++i)
  {
    thread *t = malloc(sizeof(thread));
    t->p = &pool;
    pthread_t tid1;
    pthread_create(&tid1, NULL, loginThread, t);
  }
  struct timeval timeout;
  timeout.tv_sec = 5;
  timeout.tv_usec = 0;
  while (1) {
    pthread_mutex_lock(&set.lock);
    set.ready_set = set.read_set;
    pthread_mutex_unlock(&set.lock);
    errno = 0;
    set.nready = select(set.maxfd + 1, &set.ready_set, NULL, NULL, &timeout);
    //sfwrite(&termlock, stderr,"has lock\n");
    if (ERROR(0) > 0) {
      internalError(&pool);
    }
    /*This processes server terminal commands.*/
    if (FD_ISSET(STDIN_FILENO, &set.ready_set)) {
      int isExit = command(&pool);
      if (isExit) {
        pthread_mutex_destroy(&set.lock);
        pthread_mutex_destroy(&termlock);
        pthread_mutex_destroy(&wlock);
        pthread_mutex_destroy(&pool.lock);
        free(MOTD);
        close(listenfd);
        close(fp);
        pthread_mutex_destroy(&buffer.lock);
        sem_destroy(&buffer.items);
        free(buffer.queue);
        return 0;
      }
    }
    /*This is the Accept Thread for clients*/
    if (FD_ISSET(listenfd, &set.ready_set)) {
      clientlen = sizeof(struct sockaddr_storage);
      errno = 0;
      connfd = accept(listenfd, (SA *)&clientaddr, &clientlen);
      if (ERROR(1) > 0) {
        internalError(&pool);
      }
      errno = getnameinfo((SA *)&clientaddr, clientlen, hostname, MAXLINE, port,
                          MAXLINE, 0);
      if (errno != 0) {
        internalError(&pool);
      }
      errno = 0;
      addClient(connfd, &pool, hostname);
    }
    /*This is the login thread and comm thread*/
    checkClients(&pool);
  }
  close(listenfd);
  close(fp);
  return 0;
}

/*Displays the help menu for args*/
void helpArgs() {
  sfwrite(&termlock, stderr, "\x1B[0mUsage:\n./server [-hv] [-t THREAD_COUNT] PORT_NUMBER MOTD [ACCOUNTS_FILE]\n");
  sfwrite(&termlock, stderr, "-h\t\t  Displays this help menu & returns EXIT_SUCCESS.\n");
  sfwrite(&termlock, stderr, "-v\t\t  Verbose print all incoming and outgoing protocol verbs &\n\t\t  content.\n");
  sfwrite(&termlock, stderr, "-t THREAD_COUNT\t  The number of threads used for the login queue.\n");
  sfwrite(&termlock, stderr, "PORT_NUMBER\t  Port number to listen on.\n");
  sfwrite(&termlock, stderr, "MOTD\t\t  Message to display to the client when they connect.\n");
  sfwrite(&termlock, stderr, "ACCOUNTS_FILE\t  File containing username and password data to be loaded upon\n\t\t  execution.\n");
}

/*Validates file 1 for No such file, 2 for NULL path, 0 if file exists*/
int validate_args(const char *input_path) {
  if (input_path != NULL && input_path[0] != '\0') {
    struct stat sb;
    memset(&sb, 0, sizeof(sb) + 1);
    if (stat(input_path, &sb) == -1) {
      if (errno == ENOENT)
        return 1;
    }
    stat(input_path, &sb);
  } else {
    return 2;
  }
  return 0;
}

/*Returns -1 on failure 0 if -h, 1 if -v, 2 if anything else*/
int commandLineArgs(int argc, char **argv) {
  if (argv[1] == NULL)
    return -1;
  else if (strcmp(argv[1], "-h") == 0)
    return 0;
  else if (strcmp(argv[1], "-v") == 0)
    return 1;
  else
    return 2;
}

/*Initilizes the pool of clients*/
void initilizeUsers(int listenfd, active_users *p) {
  for (int i = 0; i < FD_SETSIZE; ++i) {
    p->client[i].fd = -2;
    p->client[i].sec = -1;
    p->client[i].tid = -1;
    pthread_mutex_init(&p->client[i].lock, NULL);
  }
  set.maxfd = listenfd;
  FD_ZERO(&set.read_set);
  FD_ZERO(&set.login_set);
  FD_ZERO(&set.ready_set);
  FD_ZERO(&set.notlogin_set);
  FD_SET(listenfd, &set.read_set);
  FD_SET(STDIN_FILENO, &set.read_set);
}

/*Adds a client to the pool*/
void addClient(int connfd, active_users *p, char *hostname) {
  set.nready--;
  int i = 0;
  for (i = 0; i < FD_SETSIZE; ++i) {
    pthread_mutex_lock(&set.lock);
    pthread_mutex_lock(&p->client[i].lock);
    if (p->client[i].fd < 0) {
      p->client[i].fd = connfd;
      strcpy(p->client[i].ip, hostname);
      p->client[i].rio.bufptr = p->client[i].rio.buf;
      FD_SET(connfd, &set.notlogin_set);
      if (connfd > set.maxfd)
        set.maxfd = connfd;
      pthread_mutex_unlock(&set.lock);
      pthread_mutex_unlock(&p->client[i].lock);
      pthread_mutex_lock(&buffer.lock);
      buffer.queue[buffer.rear % MAXLINE] = connfd;
      buffer.rear++;
      pthread_mutex_unlock(&buffer.lock);
      sem_post(&buffer.items);
      break;
    }
    pthread_mutex_unlock(&set.lock);
    pthread_mutex_unlock(&p->client[i].lock);
  }
  if (i == FD_SETSIZE) {
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Full.\x1B[0m\n");
  }
}

/*Login and comm thread creation*/
void checkClients(active_users *p) {
  int i, connfd;
  pthread_mutex_lock(&set.lock);
  for (i = 0; (i <= set.maxfd + 1) && (set.nready > 0); ++i) {
    connfd = p->client[i].fd;
    /* Login thread */
    pthread_mutex_lock(&p->client[i].lock);
    if (connfd > 0 && FD_ISSET(connfd, &set.login_set) &&
               FD_ISSET(connfd, &set.ready_set) &&
               !FD_ISSET(connfd, &set.notlogin_set)) 
    {
      set.nready--;
      thread *t = malloc(sizeof(thread));
      t->c = &p->client[i];
      t->p = p;
      FD_CLR(p->client[i].fd, &set.read_set);
      pthread_mutex_unlock(&p->client[i].lock);
      pthread_t tid;
      pthread_create(&tid, NULL, commThread, t);
    }
    pthread_mutex_unlock(&p->client[i].lock);
  }
  pthread_mutex_unlock(&set.lock);
}

/*
* Prints out what errno error caused the failure and what method caused it
* whatError = 0 when select is called
* whatError = 1 when accept is called
* whatError = 2 when close is called
* whatError = 3 when setsockopt is called
* whatError = 4 when bind is called
* whatError = 5 when listen is called
* whatError = 6 when strtoll is called
*/
int ERROR(int whatError) {
  if (errno == 0)
    return 0;
  else if (errno == EBADF)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Bad file descriptor.\n");
  else if (errno == EINTR)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: A signal was caught.\n");
  else if (errno == EINVAL && whatError == 0)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: The file descriptor is negative.\n");
  else if (errno == ENOMEM)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Unable to allocate enough memory.\n");
  else if (errno == EAGAIN)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Socket is nonnlocking and no "
                    "connections are present.\n");
  else if (errno == ECONNABORTED)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Connection has been aborted.\n");
  else if (errno == EFAULT)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: The address argument is not in a "
                    "writeable part of the user address space.\n");
  else if (errno == EINVAL && whatError == 1)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Socket is not listening for "
                    "connections or the length is invalid.\n");
  else if (errno == EMFILE)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: The per process limit of open file "
                    "descriptors has been reached.\n");
  else if (errno == ENFILE)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: The system limit of open files has "
                    "been reached.\n");
  else if (errno == ENOTSOCK)
    sfwrite(&termlock, 
        stderr,
        "\t\x1B[1;31mError: The descriptor references a file, not a socket.\n");
  else if (errno == EOPNOTSUPP)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: The referenced socket is not of type "
                    "SOCK_STREAM.\n");
  else if (errno == EPROTO)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Protocol.\n");
  else if (errno == EIO)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: I/O.\n");
  else if (errno == EINVAL && whatError == 3)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Optlen is invalid.\n");
  else if (errno == EINVAL && whatError == 6)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Conversion not performed.\n");
  else if (errno == ENOPROTOOPT)
    sfwrite(&termlock, 
        stderr,
        "\t\x1B[1;31mError: The option is unknown at the level indicated.\n");
  else if (errno == EADDRINUSE)
    sfwrite(&termlock, stderr,
            "\t\x1B[1;31mError: The specified address is already in use.\n");
  else if (errno == EADDRNOTAVAIL)
    sfwrite(&termlock, stderr,
            "\t\x1B[1;31mError: The specified address is not available.\n");
  else if (errno == EAFNOSUPPORT)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: The specified address is not a valid "
                    "address for the address family of the specified "
                    "socket.\n");
  else if (errno == EINVAL && whatError == 4)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: The socket is already bound to an "
                    "address or the socket has been shutdown.\n");
  else if (errno == EINVAL && whatError == 5)
    sfwrite(&termlock, 
        stderr,
        "\t\x1B[1;31mError: The socket is already connected or shutdown.\n");
  else if (errno == EDESTADDRREQ)
    sfwrite(&termlock, stderr,
            "\t\x1B[1;31mError: The socket is not bound to a local address.\n");
  else if (errno == EACCES)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: The calling proccess does not have the "
                    "appropriate privileges.\n");
  else if (errno == ENOBUFS)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Insufficient resources are available "
                    "in the system to complete the call.\x1B[0m\n");
  else if (errno == ERANGE)
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Given string out of range.\n");
  if (whatError == 0 && errno != 0) {
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Failed to select.\x1B[0m\n");
    return 1;
  } else if (whatError == 1 && errno != 0) {
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Failed to accept.\x1B[0m\n");
    return 1;
  } else if (whatError == 2 && errno != 0) {
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Failed to close.\x1B[0m\n");
    return 1;
  } else if (whatError == 3 && errno != 0) {
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Failed to set socket.\x1B[0m\n");
    return 1;
  } else if (whatError == 4 && errno != 0) {
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Failed to bind.\x1B[0m\n");
    return 1;
  } else if (whatError == 5 && errno != 0) {
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Failed to listen.\x1B[0m\n");
    return 1;
  } else if (whatError == 6 && errno != 0) {
    sfwrite(&termlock, stderr, "\t\x1B[1;31mError: Failed to strtoll.\x1B[0m\n");
    return 1;
  }
  return 0;
}

/*Interprets server terminal commands*/
int command(active_users *p) {
  char buf[MAXLINE];
  if (!fgets(buf, MAXLINE, stdin)) {
  }
  if (strcmp(buf, "/users\n") == 0) {
    sfwrite(&termlock, stderr, "\x1B[0mCurrent Users:\n");
    sfwrite(&termlock, stderr, "______________________________________________________"
                    "__________________________\n");
    for (int i = 0; i < FD_SETSIZE; ++i) {
      pthread_mutex_lock(&p->client[i].lock);
      if (p->client[i].fd > 0) {
        sfwrite(&termlock, stderr, "NAME = %s\n", p->client[i].name);
        sfwrite(&termlock, stderr, "SALT = %s\n", p->client[i].salt);
        sfwrite(&termlock, stderr, "HASH = %s\n", p->client[i].hash);
        sfwrite(&termlock, stderr, "LOGIN TIME = %d\n", p->client[i].sec);
        sfwrite(&termlock, stderr, "IP = %s\n", p->client[i].ip);
        sfwrite(&termlock, stderr, "______________________________________________________"
                        "__________________________\n");
      }
      pthread_mutex_unlock(&p->client[i].lock);
    }
    return 0;
  } else if (strcmp(buf, "/help\n") == 0) {
    helpServer();
    return 0;
  } else if (strstr(buf, "/shutdown\n") != NULL) {
    shutDown(p);
    return 1;
  } else if (strstr(buf, "/accts\n") != NULL) {
    accounts *temp = acc_Head;
    sfwrite(&termlock, stderr, "\x1B[0mCurrent Accounts:\n");
    sfwrite(&termlock, stderr, "______________________________________________________"
                    "__________________________\n");
    while (temp != NULL) {
      pthread_mutex_lock(&temp->lock);
      if (temp->c.name[0] != '\0')
      {
        sfwrite(&termlock, stderr, "NAME = %s\n", temp->c.name);
        sfwrite(&termlock, stderr, "SALT = %s\n", temp->c.salt);
        sfwrite(&termlock, stderr, "HASH = %s\n", temp->c.hash);
        sfwrite(&termlock, stderr, "________________________________________________________"
                      "______________\n");
      }
      pthread_mutex_unlock(&temp->lock);
      temp = temp->next;
    }
    return 0;
  } else {
    return 0;
  }
}

/*Help menu for server*/
void helpServer() {
  sfwrite(&termlock, stderr, "\x1B[0m\t/help\t\tDisplays this help menu.\n");
  sfwrite(&termlock, stderr,
          "\t/shutdown\tDisconnects all users and shuts down the server.\n");
  sfwrite(&termlock, stderr, "\t/users\t\tDisplays a list of the users and their "
                  "information that\n\t\t\tare currently logged in.\n");
  sfwrite(&termlock, stderr, "\t/accts\t\tDisplays a list of all users accounts.\n");
}

/*Shutdown command*/
void shutDown(active_users *p) {
  int i;
  for (i = 0; i < FD_SETSIZE; ++i) {
    pthread_mutex_lock(&p->client[i].lock);
    if (p->client[i].fd > 0) {
      char *buf = "BYE \r\n\r\n";
      int num = write(p->client[i].fd, buf, strlen(buf));
      if (verbose) {
        sfwrite(&termlock, stderr, "\x1B[1;34mSent: BYE Byte Count: %d\n\x1B[0m", num);
      }
      pthread_mutex_destroy(&p->client[i].lock);
      errno = 0;
      close(p->client[i].fd);
      ERROR(2);
    }
    pthread_mutex_unlock(&p->client[i].lock);
  }
  while (cur != NULL) {
    accounts *temp;
    temp = acc_Head;
    while (temp != cur->prev) {
      temp = temp->next;
    }
    pthread_mutex_destroy(&cur->lock);
    free(cur);
    cur = temp;
  }
}

/* Actual thread for logging in*/
void *loginThread(void *args) {
  thread *t = (thread *)args;
  active_users *p = t->p;
  pthread_detach(pthread_self());
  int numBytes;
  while(1) 
  {
    sem_wait(&buffer.items);
    pthread_mutex_lock(&buffer.lock);
    int connfd = buffer.queue[buffer.front % MAXLINE];
    buffer.front++;
    pthread_mutex_unlock(&buffer.lock);
    client *c;
    for (int i = 0; i < FD_SETSIZE; ++i)
    {
      pthread_mutex_lock(&p->client[i].lock);
      if (p->client[i].fd == connfd)
      {
        c = &p->client[i];
        c->tid = pthread_self();
      }
      pthread_mutex_unlock(&p->client[i].lock);
    }
    while(1)
    {
      int error = 0;
      numBytes = read(c->fd, c->rio.buf, MAXLINE);
      if (numBytes != 0) {
        pthread_mutex_lock(&c->lock);
        if (strstr(c->rio.buf, "WOLFIE \r\n\r\n") != NULL) {
          if (verbose) {
            sfwrite(&termlock, stderr, "\x1B[1;34mRecieved: WOLFIE Byte Count: %d\x1B[0m\n",
                    numBytes);
          }
          char *buf = "EIFLOW \r\n\r\n";
          if (c->fd > 0) {
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              sfwrite(&termlock, stderr, "\x1B[1;34mSent: EIFLOW Byte Count: %d\x1B[0m\n",
                      num);
            }
          }
        } else if (strstr(c->rio.buf, "BYE \r\n\r\n") != NULL) {
          if (verbose) {
            sfwrite(&termlock, stderr, "\x1B[1;34mRecieved: BYE Byte Count: %d\x1B[0m\n",
                    numBytes);
          }
          if (ERROR(2) > 0) {
          }
          pthread_mutex_lock(&set.lock);
          FD_CLR(c->fd, &set.login_set);
          FD_CLR(c->fd, &set.notlogin_set);
          pthread_mutex_unlock(&set.lock);
          memset(c->name, 0, MAXLINE);
          memset(c->pass, 0, MAXLINE);
          memset(c->ip, 0, MAXLINE);
          memset(c->hash, 0, MAXLINE);
          memset(c->salt, 0, MAXLINE);
          errno = 0;
          if (c->fd > 0)
          {
            close(c->fd);
            ERROR(2);
          }
          c->fd = -2;
        } else if (strstr(c->rio.buf, "IAMNEW ") != NULL) {
          char *str;
          str = strstr(c->rio.buf, " ");
          str++;
          char *str2 = c->name;
          while (*str != '\r') {
            *str2 = *str;
            str2++;
            str++;
          }
          *str2 = '\0';
          if (verbose) {
            sfwrite(&termlock, stderr,
                    "\x1B[1;34mRecieved: IAMNEW %s Byte Count: %d\x1B[0m\n",
                    c->name, numBytes);
          }
          accounts *temp = acc_Head;  
          while (temp != NULL) {
            pthread_mutex_lock(&temp->lock);
            if (strcmp(temp->c.name, c->name) == 0 &&
                strlen(c->name) == strlen(temp->c.name)) 
            {
              char *buf = "ERR 00 USER NAME TAKEN \r\n\r\n";
              int num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                sfwrite(&termlock, stderr, "\x1B[1;34mSent: ERR 00 USER NAME TAKEN Byte "
                                "Count: %d\x1B[0m\n",
                        num);
              }
              memset(c->name, 0, MAXLINE);
              memset(c->ip, 0, MAXLINE);
              memset(c->hash, 0, MAXLINE);
              memset(c->salt, 0, MAXLINE);
              buf = "BYE \r\n\r\n";
              num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                sfwrite(&termlock, stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                          num);
              }
              error = 1;
              pthread_mutex_lock(&set.lock);
              FD_CLR(c->fd, &set.login_set);
              FD_CLR(c->fd, &set.notlogin_set);
              pthread_mutex_unlock(&set.lock);
              if (c->fd > 0)
              {
                errno = 0;
                close(c->fd);
                ERROR(2);
              }
              c->fd = -2;
              pthread_mutex_unlock(&temp->lock);
              c->rio.bufptr = c->rio.buf;
              memset(c->rio.buf, 0, MAXLINE);
              pthread_mutex_unlock(&c->lock);
              break;
            }
            pthread_mutex_unlock(&temp->lock);
            temp = temp->next;
          }
          pthread_mutex_unlock(&c->lock);
            int i = 0;
            for (i = 0; i < FD_SETSIZE; ++i) {
              pthread_mutex_lock(&p->client[i].lock);
              if (p->client[i].fd > 0) {
                if (strcmp(p->client[i].name, c->name) == 0 &&
                    strlen(c->name) == strlen(p->client[i].name) &&
                    c->fd != p->client[i].fd) 
                {
                  char *buf = "ERR 00 USER NAME TAKEN \r\n\r\n";
                  int num = write(c->fd, buf, strlen(buf));
                  if (verbose) {
                    sfwrite(&termlock, stderr, "\x1B[1;34mSent: ERR 00 USER NAME TAKEN Byte "
                                    "Count: %d\x1B[0m\n",
                            num);
                  }
                  memset(c->name, 0, MAXLINE);
                  memset(c->ip, 0, MAXLINE);
                  memset(c->hash, 0, MAXLINE);
                  memset(c->salt, 0, MAXLINE);
                  buf = "BYE \r\n\r\n";
                  num = write(c->fd, buf, strlen(buf));
                  if (verbose) {
                    sfwrite(&termlock, stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                            num);
                  }
                  error = 2;
                  pthread_mutex_lock(&set.lock);
                  FD_CLR(c->fd, &set.read_set);
                  FD_CLR(c->fd, &set.login_set);
                  FD_CLR(c->fd, &set.notlogin_set);
                  pthread_mutex_unlock(&set.lock);
                  c->fd = -2;
                  errno = 0;
                  if (c->fd > 0)
                  {
                    close(c->fd);
                    ERROR(2);
                  }
                  pthread_mutex_unlock(&p->client[i].lock);
                  break;
                }
              }
              pthread_mutex_unlock(&p->client[i].lock);
            }
            if (error == 2)
            {
              break;
            }
          if (error != 1) {
            char *buf = (char *)calloc(MAXLINE, 1);
            sprintf(buf, "HINEW %s \r\n\r\n", c->name);
            if (c->fd > 0) {
              int num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                sfwrite(&termlock, stderr,
                        "\x1B[1;34mSent: HINEW %s Byte Count: %d\x1B[0m\n",
                        c->name, num);
              }
            }
            free(buf);
          } else {
            break;
          }
        } else if (strstr(c->rio.buf, "NEWPASS ") !=
                   NULL) // storing passwords as strings need salt
        {
          char *str;
          str = strstr(c->rio.buf, " ");      str++;
          char *str2 = c->pass;
          while (*str != '\r') {
            *str2 = *str;
            str2++;
            str++;
          }
          *str2 = '\0';
          if (verbose) {
            sfwrite(&termlock, stderr,
                    "\x1B[1;34mRecieved: NEWPASS %s Byte Count: %d\x1B[0m\n",
                    c->pass, numBytes);
          }
          int bad = checkPassword(c->pass);
          if (bad) {
            char *buf = "ERR 02 BAD PASSWORD \r\n\r\n";
            if (c->fd > 0) {
              int num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                sfwrite(&termlock, 
                    stderr,
                    "\x1B[1;34mSent: ERR 02 BAD PASSWORD Byte Count: %d\x1B[0m\n",
                    num);
              }
            }
            memset(c->name, 0, MAXLINE);
            memset(c->pass, 0, MAXLINE);
            memset(c->hash, 0, MAXLINE);
            memset(c->salt, 0, MAXLINE);
            memset(c->ip, 0, MAXLINE);
            buf = "BYE \r\n\r\n";
            if (c->fd > 0) {
              int num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                sfwrite(&termlock, stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                        num);
              }
              error = 1;
            }
            errno = 0;
            if (c->fd > 0)
            {
              close(c->fd);
              ERROR(2);
            }
            pthread_mutex_lock(&set.lock);
            FD_CLR(c->fd, &set.login_set);
            FD_CLR(c->fd, &set.notlogin_set);
            pthread_mutex_unlock(&set.lock);
            c->fd = -2;
            c->rio.bufptr = c->rio.buf;
            memset(c->rio.buf, 0, MAXLINE);
            pthread_mutex_unlock(&c->lock);
            break;
          } else {
            pthread_mutex_unlock(&c->lock);
            int i = 0;
            for (i = 0; i < FD_SETSIZE; ++i) {
              pthread_mutex_lock(&p->client[i].lock);
              if (p->client[i].fd > 0) {
                if (strcmp(p->client[i].name, c->name) == 0 &&
                    strlen(c->name) == strlen(p->client[i].name) &&
                    c->fd != p->client[i].fd) 
                {
                  char *buf = "ERR 00 USER NAME TAKEN \r\n\r\n";
                  int num = write(c->fd, buf, strlen(buf));
                  if (verbose) {
                    sfwrite(&termlock, stderr, "\x1B[1;34mSent: ERR 00 USER NAME TAKEN Byte "
                                    "Count: %d\x1B[0m\n",
                            num);
                  }
                  memset(c->name, 0, MAXLINE);
                  memset(c->ip, 0, MAXLINE);
                  memset(c->hash, 0, MAXLINE);
                  memset(c->salt, 0, MAXLINE);
                  buf = "BYE \r\n\r\n";
                  num = write(c->fd, buf, strlen(buf));
                  if (verbose) {
                    sfwrite(&termlock, stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                            num);
                  }
                  error = 2;
                  pthread_mutex_lock(&set.lock);
                  FD_CLR(c->fd, &set.read_set);
                  FD_CLR(c->fd, &set.login_set);
                  FD_CLR(c->fd, &set.notlogin_set);
                  pthread_mutex_unlock(&set.lock);
                  errno = 0;
                  if (c->fd > 0)
                  {
                    close(c->fd);
                    ERROR(2);
                  }
                  c->fd = -2;
                  pthread_mutex_unlock(&p->client[i].lock);
                  break;
                }
              }
              pthread_mutex_unlock(&p->client[i].lock);
            }
            if (error == 2)
            {
              break;
            }
            time_t rawtime;
            time(&rawtime);
            c->login_t = localtime(&rawtime);
            char *buf = (char *)calloc(MAXLINE, 1);
            sprintf(buf, "SSAPWEN \r\n\r\n");
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              sfwrite(&termlock, stderr, "\x1B[1;34mSent: SSAPWEN Byte Count: %d\x1B[0m\n",
                      num);
            }
            unsigned char *salt = (unsigned char *)calloc(RAND_BYTE_LENGTH, 1);
            RAND_bytes(salt, RAND_BYTE_LENGTH / 2);
            char *hexSalt = (char *)calloc(RAND_BYTE_LENGTH + 1, 1);
            char *hexSaltPtr = hexSalt;
            for (int i = 0; i < RAND_BYTE_LENGTH / 2; i++) {
              hexSaltPtr += sprintf(hexSaltPtr, "%02x", salt[i]);
            }
            hexSaltPtr = hexSalt;
            char *str3 = c->salt;
            while (*hexSaltPtr != '\0') {
              *str3 = *hexSaltPtr;
              str3++;
              hexSaltPtr++;
            }
            *str3 = '\0';
            unsigned char hash[SHA256_DIGEST_LENGTH];
            SHA256_CTX sha256;
            SHA256_Init(&sha256);
            char *pass =
                (char *)calloc(RAND_BYTE_LENGTH + strlen(c->pass) + 1, 1);
            sprintf(pass, "%s%s", c->pass, hexSalt);        SHA256_Update(&sha256, pass, RAND_BYTE_LENGTH + strlen(c->pass) + 1);
            SHA256_Final(hash, &sha256);
            char *hexHash = (char *)calloc(SHA256_DIGEST_LENGTH * 2 + 1, 1);
            char *hexHashPtr = hexHash;
            for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
              hexHashPtr += sprintf(hexHashPtr, "%02x", hash[i]);
            }
            str3 = c->hash;
            hexHashPtr = hexHash;
            while (*hexHashPtr != '\0') {
              *str3 = *hexHashPtr;
              str3++;
              hexHashPtr++;
            }
            *str3 = '\0';
            free(buf);
            free(pass);
            free(hexHash);
            free(salt);
            free(hexSalt);
            memset(c->pass, 0, MAXLINE);
            buf = (char *)calloc(MAXLINE, 1);
            c->sec = c->login_t->tm_mday * 24 * 60 * 60;
            c->sec += c->login_t->tm_hour * 60 * 60;
            c->sec += c->login_t->tm_min * 60;
            c->sec += c->login_t->tm_sec;
            sprintf(buf, "HI %s \r\n\r\n", c->name);
            num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              sfwrite(&termlock, stderr, "\x1B[1;34mSent: HI %s Byte Count: %d\x1B[0m\n",
                        c->name, num);
            }
            free(buf);
            pthread_mutex_lock(&cur->lock);
            strcpy(cur->c.name, c->name); // adds to end of list
            strcpy(cur->c.salt, c->salt);
            strcpy(cur->c.hash, c->hash);
            accounts *temp = cur;
            cur = cur->next;
            cur = (accounts *)malloc(sizeof(accounts));
            cur->prev = temp;
            temp->next = cur;
            cur->next = NULL;
            cur->prev = NULL;
            pthread_mutex_unlock(&temp->lock);
            pthread_mutex_lock(&wlock);
            buf = (char *)calloc(MAXLINE, 1);
            sprintf(buf, "%s\n", c->name);
            write(fp, buf, strlen(buf));
            free(buf);
            buf = (char *)calloc(MAXLINE, 1);
            sprintf(buf, "%s\n", c->hash);
            write(fp, buf, strlen(buf));
            free(buf);
            buf = (char *)calloc(MAXLINE, 1);
            sprintf(buf, "%s\n", c->salt);
            write(fp, buf, strlen(buf));
            pthread_mutex_unlock(&wlock);
            free(buf);
            buf = (char *)calloc(MAXLINE, 1);
            sprintf(buf, "MOTD %s \r\n\r\n", MOTD);
            num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              sfwrite(&termlock, stderr, "\x1B[1;34mSent: MOTD %s Byte Count: %d\x1B[0m\n",
                      MOTD, num);
            }
            fflush(stderr);
            free(buf);
            pthread_mutex_lock(&set.lock);
            FD_SET(c->fd, &set.read_set);
            FD_SET(c->fd, &set.login_set);
            FD_CLR(c->fd, &set.notlogin_set);
            pthread_mutex_unlock(&set.lock);
            c->rio.bufptr = c->rio.buf;
            memset(c->rio.buf, 0, MAXLINE);
            pthread_mutex_unlock(&c->lock);
            break;
          }
        } else if (strstr(c->rio.buf, "IAM") != NULL) // IAM part 3
        {
          char *str;
          str = strstr(c->rio.buf, " ");
          str++;
          char *str2 = c->name;
          while (*str != '\r') {
            *str2 = *str;
            str2++;
            str++;
          }
          *str2 = '\0';
          if (verbose) {
            sfwrite(&termlock, stderr, "\x1B[1;34mRecieved: IAM %s Byte Count: %d\x1B[0m\n",
                    c->name, numBytes);
          }
          int i = 0;
          pthread_mutex_unlock(&c->lock);
          for (i = 0; i < FD_SETSIZE; ++i) {
            pthread_mutex_lock(&p->client[i].lock);
            if (p->client[i].fd > 0) {
              if (strcmp(p->client[i].name, c->name) == 0 &&
                  strlen(c->name) == strlen(p->client[i].name) &&
                  c->fd != p->client[i].fd) 
              {
                char *buf = "ERR 00 USER NAME TAKEN \r\n\r\n";
                int num = write(c->fd, buf, strlen(buf));
                if (verbose) {
                  sfwrite(&termlock, stderr, "\x1B[1;34mSent: ERR 00 USER NAME TAKEN Byte "
                                  "Count: %d\x1B[0m\n",
                          num);
                }
                memset(c->name, 0, MAXLINE);
                memset(c->ip, 0, MAXLINE);
                memset(c->hash, 0, MAXLINE);
                memset(c->salt, 0, MAXLINE);
                buf = "BYE \r\n\r\n";
                num = write(c->fd, buf, strlen(buf));
                if (verbose) {
                  sfwrite(&termlock, stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                          num);
                }
                error = 1;
                pthread_mutex_lock(&set.lock);
                FD_CLR(c->fd, &set.read_set);
                FD_CLR(c->fd, &set.login_set);
                FD_CLR(c->fd, &set.notlogin_set);
                pthread_mutex_unlock(&set.lock);
                errno =0;
                if (c->fd > 0)
                {
                  close(c->fd);
                  ERROR(2);
                }
                c->fd = -2;
                pthread_mutex_unlock(&p->client[i].lock);
                break;
              }
            }
            pthread_mutex_unlock(&p->client[i].lock);
          }
          pthread_mutex_lock(&c->lock);
          if (!error) {
            accounts *temp = acc_Head;
            while (temp != NULL) {
              pthread_mutex_lock(&temp->lock);
              if (strcmp(temp->c.name, c->name) == 0 &&
                  strlen(c->name) == strlen(temp->c.name)) 
              {
                char *buf = (char *)calloc(MAXLINE, 1);
                sprintf(buf, "AUTH %s \r\n\r\n", c->name);
                if (c->fd > 0) {
                  int num = write(c->fd, buf, strlen(buf));
                  if (verbose) {
                    sfwrite(&termlock, stderr,
                            "\x1B[1;34mSent: AUTH %s Byte Count: %d\x1B[0m\n",
                            c->name, num);
                  }
                  error = 1;
                }
                pthread_mutex_unlock(&temp->lock);
                break;
              }
              pthread_mutex_unlock(&temp->lock);
              temp = temp->next;
            }
            if (!error) {
              char *buf = "ERR 01 USER NOT AVAIBLE \r\n\r\n";
              int num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                sfwrite(&termlock, stderr, "\x1B[1;34mSent: ERR 01 USER NOT AVAIBLE Byte "
                                "Count: %d\x1B[0m\n",
                        num);
              }
              memset(c->name, 0, MAXLINE);
              memset(c->ip, 0, MAXLINE);
              memset(c->hash, 0, MAXLINE);
              memset(c->salt, 0, MAXLINE);
              buf = "BYE \r\n\r\n";
              num = write(c->fd, buf, strlen(buf));
              if (verbose) {
                sfwrite(&termlock, stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                        num);
              }
              error = 0;
              pthread_mutex_lock(&set.lock);
              FD_CLR(c->fd, &set.login_set);
              FD_CLR(c->fd, &set.notlogin_set);
              pthread_mutex_unlock(&set.lock);
              errno = 0;
              if (c->fd > 0)
              {
                close(c->fd);
                ERROR(2);
              }
              c->fd = -2;
              c->rio.bufptr = c->rio.buf;
              memset(c->rio.buf, 0, MAXLINE);
              pthread_mutex_unlock(&c->lock);
              break;
            }
          } else {
            c->rio.bufptr = c->rio.buf;
            memset(c->rio.buf, 0, MAXLINE);
            pthread_mutex_unlock(&c->lock);
            break;
          }
        } else if (strstr(c->rio.buf, "PASS") != NULL) {
          char *str;
          str = strstr(c->rio.buf, " ");
          str++;
          char *str2 = c->pass;
          while (*str != '\r') {
            *str2 = *str;
            str2++;
            str++;
          }
          *str2 = '\0';
          if (verbose) {
            sfwrite(&termlock, stderr, "\x1B[1;34mRecieved: PASS %s Byte Count: %d\x1B[0m\n",
                    c->pass, numBytes);
          }
          accounts *temp = acc_Head;
          while (temp != NULL) {
            pthread_mutex_lock(&temp->lock);
            if (strcmp(temp->c.name, c->name) == 0 &&
                strlen(c->name) == strlen(temp->c.name)) 
            {
              unsigned char hash[SHA256_DIGEST_LENGTH];
              SHA256_CTX sha256;
              SHA256_Init(&sha256);
              char *pass =
                  (char *)calloc(RAND_BYTE_LENGTH + strlen(c->pass) + 1, 1);
              sprintf(pass, "%s%s", c->pass, temp->c.salt);
              SHA256_Update(&sha256, pass,
                            RAND_BYTE_LENGTH + strlen(c->pass) + 1);
              SHA256_Final(hash, &sha256);
              char *hexHash = (char *)calloc(SHA256_DIGEST_LENGTH * 2 + 1, 1);
              char *hexHashPtr = hexHash;
              for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
                hexHashPtr += sprintf(hexHashPtr, "%02x", hash[i]);
              }
              char *str3 = c->hash;
              hexHashPtr = hexHash;
              while (*hexHashPtr != '\0') {
                *str3 = *hexHashPtr;
                str3++;
                hexHashPtr++;
              }
              *str3 = '\0';
              free(pass);
              free(hexHash);
              if (!strcmp(temp->c.hash, c->hash) == 0 ||
                  strlen(c->hash) != strlen(temp->c.hash)) 
              {
                char *buf = "ERR 02 BAD PASSWORD \r\n\r\n";
                int num = write(c->fd, buf, strlen(buf));
                if (verbose) {
                  sfwrite(&termlock, stderr, "\x1B[1;34mSent: ERR 02 BAD PASSWORD Byte "
                            "Count: %d\x1B[0m\n",
                            num);
                }
                memset(c->name, 0, MAXLINE);
                memset(c->ip, 0, MAXLINE);
                memset(c->pass, 0, MAXLINE);
                memset(c->hash, 0, MAXLINE);
                memset(c->salt, 0, MAXLINE);
                buf = "BYE \r\n\r\n";
                num = write(c->fd, buf, strlen(buf));
                if (verbose) {
                  sfwrite(&termlock, stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                          num);
                }
                error = 1;
                pthread_mutex_lock(&set.lock);
                FD_CLR(c->fd, &set.login_set);
                FD_CLR(c->fd, &set.notlogin_set);
                pthread_mutex_unlock(&set.lock);
                errno = 0;
                if (c->fd > 0)
                {
                  close(c->fd);
                  ERROR(2);
                }
                c->fd = -2;
                pthread_mutex_unlock(&temp->lock);
                c->rio.bufptr = c->rio.buf;
                memset(c->rio.buf, 0, MAXLINE);
                pthread_mutex_unlock(&c->lock);
                break;
              }
            }
            pthread_mutex_unlock(&temp->lock);
            temp = temp->next;
          }
          if (!error) {
            pthread_mutex_unlock(&c->lock);
            int i = 0;
            for (i = 0; i < FD_SETSIZE; ++i) {
              pthread_mutex_lock(&p->client[i].lock);
              if (p->client[i].fd > 0) {
                if (strcmp(p->client[i].name, c->name) == 0 &&
                    strlen(c->name) == strlen(p->client[i].name) &&
                    c->fd != p->client[i].fd) 
                {
                  char *buf = "ERR 00 USER NAME TAKEN \r\n\r\n";
                  int num = write(c->fd, buf, strlen(buf));
                  if (verbose) {
                    sfwrite(&termlock, stderr, "\x1B[1;34mSent: ERR 00 USER NAME TAKEN Byte "
                                    "Count: %d\x1B[0m\n",
                            num);
                  }
                  memset(c->name, 0, MAXLINE);
                  memset(c->ip, 0, MAXLINE);
                  memset(c->hash, 0, MAXLINE);
                  memset(c->salt, 0, MAXLINE);
                  buf = "BYE \r\n\r\n";
                  num = write(c->fd, buf, strlen(buf));
                  if (verbose) {
                    sfwrite(&termlock, stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                            num);
                  }
                  error = 2;
                  pthread_mutex_lock(&set.lock);
                  FD_CLR(c->fd, &set.read_set);
                  FD_CLR(c->fd, &set.login_set);
                  FD_CLR(c->fd, &set.notlogin_set);
                  pthread_mutex_unlock(&set.lock);
                  errno = 0;
                  if (c->fd > 0)
                  {
                    close(c->fd);
                    ERROR(2);
                  }
                  c->fd = -2;
                  pthread_mutex_unlock(&p->client[i].lock);
                  break;
                }
              }
              pthread_mutex_unlock(&p->client[i].lock);
            }
            if (error == 2)
            {
              break;
            }
            time_t rawtime;
            time(&rawtime);
            c->login_t = localtime(&rawtime);
            char *buf = (char *)calloc(MAXLINE, 1);
            sprintf(buf, "SSAP \r\n\r\n");
            int num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              sfwrite(&termlock, stderr, "\x1B[1;34mSent: SSAP Byte Count: %d\x1B[0m\n",
                      num);
            }
            free(buf);
            buf = (char *)calloc(MAXLINE, 1);
            c->sec = c->login_t->tm_mday * 24 * 60 * 60;
            c->sec += c->login_t->tm_hour * 60 * 60;
            c->sec += c->login_t->tm_min * 60;
            c->sec += c->login_t->tm_sec;
            sprintf(buf, "HI %s \r\n\r\n", c->name);
            num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              sfwrite(&termlock, stderr, "\x1B[1;34mSent: HI %s Byte Count: %d\x1B[0m\n",
                      c->name, num);
            }
            free(buf);
            buf = (char *)calloc(MAXLINE, 1);
            sprintf(buf, "MOTD %s \r\n\r\n", MOTD);
            num = write(c->fd, buf, strlen(buf));
            if (verbose) {
              sfwrite(&termlock, stderr,
                      "\x1B[1;34mSent: MOTD %s Byte Count: %d\x1B[0m\n", MOTD,
                       num);
            }
            fflush(stderr);
            free(buf);
            pthread_mutex_lock(&set.lock);
            FD_SET(c->fd, &set.read_set);
            FD_SET(c->fd, &set.login_set);
            FD_CLR(c->fd, &set.notlogin_set);
            pthread_mutex_unlock(&set.lock);
            c->rio.bufptr = c->rio.buf;
            memset(c->rio.buf, 0, MAXLINE);
            pthread_mutex_unlock(&c->lock);
            break;
          } else {
            break;
          }
        } else {
          if (c->fd > 0)
          {
            pthread_mutex_lock(&set.lock);
            FD_CLR(c->fd, &set.login_set);
            FD_CLR(c->fd, &set.notlogin_set);
            pthread_mutex_unlock(&set.lock);
            memset(c->name, 0, MAXLINE);
            memset(c->pass, 0, MAXLINE);
            memset(c->ip, 0, MAXLINE);
            memset(c->hash, 0, MAXLINE);
            memset(c->salt, 0, MAXLINE);
            errno = 0;
            if (c->fd > 0)
            {
              close(c->fd);
              ERROR(2);
            }
            c->fd = -2;
          }
          break;
        }
        c->rio.bufptr = c->rio.buf;
        memset(c->rio.buf, 0, MAXLINE);
        pthread_mutex_unlock(&c->lock);
      } else {
        if (c->fd > 0)
        {
          FD_CLR(c->fd, &set.read_set);
          FD_CLR(c->fd, &set.login_set);
          FD_CLR(c->fd, &set.notlogin_set);
          memset(c->name, 0, MAXLINE);
          memset(c->pass, 0, MAXLINE);
          memset(c->ip, 0, MAXLINE);
          memset(c->hash, 0, MAXLINE);
          memset(c->salt, 0, MAXLINE);
          errno = 0;
          if (c->fd > 0)
          {
            close(c->fd);
            ERROR(2);
          }
          c->fd = -2;
        }
        break;
      }
    }
  }
  free(t);
  return NULL;
}

/* Actual thread for communication*/
void *commThread(void *args) {
  thread *t = (thread *)args;
  client *c = t->c;
  active_users *p = t->p;
  pthread_detach(pthread_self());
  while(1)
  {
    int error = 0;
    int numBytes = read(c->fd, c->rio.buf, MAXLINE);
    if (numBytes != 0) {
      pthread_mutex_lock(&c->lock);
      if (strcmp(c->rio.buf, "LISTU \r\n\r\n") == 0) {
        if (verbose) {
          sfwrite(&termlock, stderr, "\x1B[1;34mRecieved: LISTU Byte Count: %d\x1B[0m\n",
                  numBytes);
        }
        char *buf;
        int i = 0;
        int active = 0;
        pthread_mutex_unlock(&c->lock);
        for (i = 0; i < FD_SETSIZE; ++i) {
          pthread_mutex_lock(&p->client[i].lock);
          if (p->client[i].fd > 0) {
            active++;
          }
          pthread_mutex_unlock(&p->client[i].lock);
        }
        buf = (char *)calloc(MAXLINE * active + 5 * active + 8, 1);
        sprintf(buf, "UTSIL");
        for (i = 0; i < FD_SETSIZE; ++i) {
          pthread_mutex_lock(&p->client[i].lock);
          if (p->client[i].fd > 0) {
            sprintf(buf, "%s %s \r\n", buf, p->client[i].name);
          }
          pthread_mutex_unlock(&p->client[i].lock);
        }
        sprintf(buf, "%s\r\n", buf);
        int num = write(c->fd, buf, strlen(buf));
        free(buf);
        buf = (char *)calloc(MAXLINE * active + 5 * active + 8, 1);
        if (verbose) {
          sfwrite(&termlock, stderr, "\x1B[1;34mSent: UTSIL");
          for (i = 0; i < FD_SETSIZE; ++i) {
            pthread_mutex_lock(&p->client[i].lock);
            if (p->client[i].fd > 0) {
              sprintf(buf, "%s%s ", buf, p->client[i].name);
            }
            pthread_mutex_unlock(&p->client[i].lock);
          }
          sfwrite(&termlock, stderr, "%sByte Count: %d\n\x1B[0m", buf, num);
        }
        free(buf);
        pthread_mutex_lock(&c->lock);
      } else if (strcmp(c->rio.buf, "BYE \r\n\r\n") == 0) {
        if (verbose) {
          sfwrite(&termlock, stderr, "\x1B[1;34mRecieved: BYE Byte Count: %d\x1B[0m\n",
                  numBytes);
        }
        if (error == 0) {
          char *buf = "BYE \r\n\r\n";
          int num = write(c->fd, buf, strlen(buf));
          if (verbose) {
            sfwrite(&termlock, stderr, "\x1B[1;34mSent: BYE Byte Count: %d\x1B[0m\n",
                      num);
          }
        }
        pthread_mutex_lock(&set.lock);
        FD_CLR(c->fd, &set.login_set);
        pthread_mutex_unlock(&set.lock);
        errno = 0;
        if (c->fd > 0)
        {
          close(c->fd);
          ERROR(2);
        }
        c->fd = -2;
        uoff(c, p);
        memset(c->name, 0, MAXLINE);
        memset(c->pass, 0, MAXLINE);
        memset(c->ip, 0, MAXLINE);
        memset(c->hash, 0, MAXLINE);
        memset(c->salt, 0, MAXLINE);
        c->rio.bufptr = c->rio.buf;
        memset(c->rio.buf, 0, MAXLINE);
        pthread_mutex_unlock(&c->lock);
        break;
      } else if (strcmp(c->rio.buf, "TIME \r\n\r\n") == 0) {
        if (verbose) {
          sfwrite(&termlock, stderr, "\x1B[1;34mRecieved: TIME Byte Count: %d\x1B[0m\n",
                  numBytes);
        }
        char *buf = (char *)calloc(MAXLINE, 1);
        time_t rawtime;
        struct tm *timeinfo;
        time(&rawtime);
        timeinfo = localtime(&rawtime);
        int sec = timeinfo->tm_mday * 24 * 60 * 60;
        sec += timeinfo->tm_hour * 60 * 60;
        sec += timeinfo->tm_min * 60;
        sec += timeinfo->tm_sec;
        sprintf(buf, "EMIT %d \r\n\r\n", sec - c->sec);
        if (c->fd > 0) {
          int num = write(c->fd, buf, strlen(buf));
          if (verbose) {
            sfwrite(&termlock, stderr, "\x1B[1;34mSent: EMIT %d Byte Count: %d\x1B[0m\n",
                    sec - c->sec, num);
          }
        }
        free(buf);
      } else if (strstr(c->rio.buf, "MSG") != NULL) {
        char *str = strstr(c->rio.buf, " ");
        str++;
        client *c1 = (client *)malloc(sizeof(client));
        client *c2 = (client *)malloc(sizeof(client));
        char *str2 = c1->name;
        while (*str != ' ') {
          *str2 = *str;
          str2++;
          str++;
        }
        *str2 = '\0';
        str++;
        str2 = c2->name;
        while (*str != ' ') {
          *str2 = *str;
          str2++;
          str++;
        }
        *str2 = '\0';
        char *msg = (char *)calloc(MAXLINE, 1);
        str++;
        str2 = msg;
        while (*str != '\r') {
          *str2 = *str;
          str2++;
          str++;
        }
        str2--;
        *str2 = '\0';
        char *buf = (char *)calloc(MAXLINE, 1);
        if (verbose) {
          sfwrite(&termlock, stderr,
                  "\x1B[1;34mRecieved: MSG %s %s %s Byte Count: %d\x1B[0m\n",
                  c1->name, c2->name, msg, numBytes);
        }
        int i, loggedin = -1;
        pthread_mutex_unlock(&c->lock);
        for (i = 0; i < FD_SETSIZE; ++i) {
          pthread_mutex_lock(&p->client[i].lock);
          if (p->client[i].fd > 0) {
            if (strcmp(p->client[i].name, c1->name) == 0 &&
                strlen(c1->name) == strlen(p->client[i].name)) {
              if (c->fd > 0) {
                int num = write(c->fd, c->rio.buf, strlen(c->rio.buf));
                int num2 =
                    write(p->client[i].fd, c->rio.buf, strlen(c->rio.buf));
                if (verbose) {
                  sfwrite(&termlock, 
                      stderr,
                      "\x1B[1;34mSent: MSG %s %s %s Byte Count: %d\x1B[0m\n",
                      c1->name, c2->name, msg, num);
                  sfwrite(&termlock, 
                      stderr,
                      "\x1B[1;34mSent: MSG %s %s %s Byte Count: %d\x1B[0m\n",
                      c1->name, c2->name, msg, num2);
                }
                loggedin = 0;
                pthread_mutex_unlock(&p->client[i].lock);
                break;
              }
            }
          }
          pthread_mutex_unlock(&p->client[i].lock);
        }
        pthread_mutex_lock(&c->lock);
        if (loggedin == -1) {
          char *buf = "ERR 01 USER NOT AVAIBLE \r\n\r\n";
          int num = write(c->fd, buf, strlen(buf));
          if (verbose) {
            sfwrite(&termlock, stderr, "\x1B[1;34mSent: ERR 01 USER NOT AVAIBLE Byte "
                              "Count: %d\x1B[0m\n",
                      num);
          }
        }
        free(c1);
        free(c2);
        free(msg);
        free(buf);
      }
      c->rio.bufptr = c->rio.buf;
      memset(c->rio.buf, 0, MAXLINE);
      pthread_mutex_unlock(&c->lock);
    } else {
      if (c->fd > 0)
      {
        pthread_mutex_lock(&set.lock);
        FD_CLR(c->fd, &set.login_set);
        pthread_mutex_unlock(&set.lock);
        c->fd = -2;
        uoff(c, p);
        memset(c->name, 0, MAXLINE);
        memset(c->pass, 0, MAXLINE);
        memset(c->ip, 0, MAXLINE);
        memset(c->hash, 0, MAXLINE);
        memset(c->salt, 0, MAXLINE);
      }
      c->rio.bufptr = c->rio.buf;
      memset(c->rio.buf, 0, MAXLINE);
      pthread_mutex_unlock(&c->lock);
      break;
    }
  }
  free(t);
  return NULL;
}

/* Broadcasts UOFF*/
void uoff(client *c, active_users *p) {
  int i;
  for (i = 0; i < FD_SETSIZE; ++i) {
    if (p->client[i].fd > 0 && FD_ISSET(p->client[i].fd, &set.read_set)) {
      char *buf = (char *)calloc(MAXLINE, 1);
      sprintf(buf, "UOFF %s \r\n\r\n", c->name);
      int num = write(p->client[i].fd, buf, strlen(buf));
      if (verbose) {
        sfwrite(&termlock, stderr, "\x1B[1;34mSent: UOFF %s Byte Count: %d\x1B[0m\n",
                c->name, num);
      }
      free(buf);
    }
  }
}

/* Checks new passwords */
int checkPassword(char password[]) {
  int i = 0;
  bool hasLength = false;
  bool hasUpper = false;
  bool hasSymbol = false;
  bool hasNumber = false;
  if (strlen(password) >= 5) {
    hasLength = true;
  }
  for (i = 0; i < strlen(password); i++) {
    if (password[i] >= 65 && password[i] <= 90) {
      hasUpper = true;
    }
    if (password[i] >= 33 && password[i] <= 47) {
      hasSymbol = true;
    }
    if (password[i] >= 48 && password[i] <= 57) {
      hasNumber = true;
    }
  }
  if ((hasLength && hasUpper) && (hasSymbol && hasNumber))
    return 0;
  else
    return 1;
}

/* Broadcasts ERR 100 INTERNAL SERVER ERROR*/
void internalError(active_users *p) {
  int i;
  for (i = 0; i < FD_SETSIZE; ++i) {
    if (p->client[i].fd > 0 && FD_ISSET(p->client[i].fd, &set.read_set)) {
      char *buf = (char *)calloc(MAXLINE, 1);
      sprintf(buf, "ERR 100 INTERNAL SERVER ERROR \r\n\r\n");
      int num = write(p->client[i].fd, buf, strlen(buf));
      if (verbose) {
        sfwrite(&termlock, stderr, "\x1B[1;34mSent: ERR 100 INTERNAL SERVER ERROR Byte "
                        "Count: %d\x1B[0m\n",
                num);
      }
      free(buf);
    }
  }
}
