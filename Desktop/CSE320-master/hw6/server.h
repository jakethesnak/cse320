#include "sfwrite.h"
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <math.h>
#include <netdb.h>
#include <netinet/in.h>
#include <openssl/rand.h>
#include <openssl/sha.h>
#include <semaphore.h>
#include <signal.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <sqlite3.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#define _GNU_SOURCE

#define MAXLINE 4096
#define RAND_BYTE_LENGTH 256

typedef struct sockaddr SA;

typedef struct {
  char *bufptr;
  char buf[MAXLINE];
} rio_t;

typedef struct {
  pthread_mutex_t lock;
  int maxfd;
  fd_set read_set;
  fd_set ready_set;
  fd_set login_set;
  fd_set notlogin_set;
  int nready;
} Set;

typedef struct {
  pthread_mutex_t lock;
  sem_t items;
  int *queue;
  int front;
  int rear;
} sfbuffer;

typedef struct {
  pthread_mutex_t lock;
  struct tm* login_t;
  int sec;
  int fd;
  pthread_t tid;
  char name[MAXLINE];
  char ip[MAXLINE];
  char pass[MAXLINE];
  char salt[RAND_BYTE_LENGTH * 2];
  char hash[SHA256_DIGEST_LENGTH * 2];
  rio_t rio;
} client;

typedef struct {
  pthread_mutex_t lock;
  client client[FD_SETSIZE];
} active_users;

typedef struct {
  client *c;
  active_users *p;
} thread;

struct __attribute__((__packed__)) accounts {
    client c;
    pthread_mutex_t lock;
    struct accounts *next;
    struct accounts *prev;
};

typedef struct accounts accounts;

void addClient(int connfd, active_users *p, char *hostname);
void *chatThread(void *args);
void checkClients(active_users *p);
int checkPassword(char password[]);
int command(active_users *p);
int commandLineArgs(int argc, char **argv);
void *commThread(void *args);
int ERROR(int whatError);
void helpArgs();
void helpServer();
void initilizeUsers(int listenfd, active_users *p);
void internalError(active_users *p);
void *loginThread(void *args);
void shutDown(active_users *p);
void uoff(client *c, active_users *p);
int validate_args(const char *input_path);