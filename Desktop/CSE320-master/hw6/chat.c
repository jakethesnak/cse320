#include "chat.h"
fd_set read_set, ready_set;
char* username = NULL;
int main(int argc, char* argv[]) {
  int clientFD = atoi(argv[1]);
  // fprintf(stderr, "%s\n", argv[2]);
  username = argv[3];
  FD_ZERO(&read_set);
  FD_SET(STDIN_FILENO, &read_set);
  FD_SET(clientFD, &read_set);
  while (true) {
    ready_set = read_set;
    select(clientFD + 1, &ready_set, NULL, NULL, NULL);
    // user types in chat
    if (FD_ISSET(STDIN_FILENO, &ready_set)) {
      char* buf = (char*)malloc(1024);
      memset(buf, '\0', 1024);
      if (fgets(buf, 1024, stdin) != NULL) {
        //  fprintf(stderr, "\033[1A%s: %s\n", username, buf);
        if (strstr(buf, "/close") != NULL) {
          exit(EXIT_SUCCESS);
        }
        // its an actual msg to the other person in the chat
        else {
          write(clientFD, buf, strlen(buf));
        }
      }
      free(buf);
    }
    // client wrote to me.
    if (FD_ISSET(clientFD, &ready_set)) {
      char newBuf[1024];
      memset(newBuf, '\0', 1024);
      read(clientFD, newBuf, 1024);
      fprintf(stderr, "\033[1A%s\n", newBuf);
    }
  }

  return 0;
}
